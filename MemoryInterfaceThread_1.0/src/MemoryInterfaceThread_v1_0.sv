`timescale 1ns / 1ps

interface sb;
    logic [7:0] address;
    logic [7:0] data;
    logic dataValid;
    logic start, read;   
    modport master  (output start, output read, output address, inout dataValid, inout data ); 
    modport slave  (input start, input read, input address, inout dataValid, inout data );
endinterface

module MemoryInterfaceThread(
    input logic clock,
    sb.slave memBus,
    input logic resetN
    );

logic [7:0] Mem [15: 0], MemData;
logic ld_AddrUp, ld_AddrLo, memDataAvail = 0, en_Data, ld_Data, dv;
logic [7:0] DataReg;
logic [15:0] AddrReg;

enum {SA, SB, SC, SD, SE} State, NextState;

initial begin
for (int i = 0; i <= 16; i++)
    Mem[i] = 0;
end

assign memBus.data = (en_Data) ? MemData : 'bz;
assign memBus.dataValid = (State == SC) ? dv : 1'bz;

always @(AddrReg, ld_Data)
    MemData = Mem[AddrReg] ;

//always_ff @(posedge clock)
//    if (ld_AddrUp) AddrReg[15:8] <= memBus.address;

//always_ff @(posedge clock)
//    if (ld_AddrLo) AddrReg[7:0] <= memBus.address;

always @(posedge clock) begin
    if (ld_Data) begin
        DataReg <= memBus.data;
        Mem [AddrReg] <= memBus.data;
    end
end

//always_ff @(posedge clock, negedge resetN)
//    if (~resetN) State <= SA;
//else State <= NextState;

always_ff @ ( posedge clock, negedge resetN) begin
    if (~resetN) begin 
        ld_AddrUp <= 0;
        ld_AddrLo <= 0;
        dv <= 0;
        en_Data <= 0;
        ld_Data <= 0;
    end 
    else begin
        case (State)
            SA: begin
                State <= (memBus.start) ? SB : SA;
                AddrReg[15:8] <= memBus.address;
//                ld_AddrUp <= (memBus.start) ? 1: 0;
            end
            SB: begin
                State <= (memBus.read) ? SC : SD;
                AddrReg[7:0] <= memBus.address;
                ld_AddrUp <= 0;
                ld_AddrLo <= 1;
            end
            SC: begin // read
                State <= (memDataAvail) ? SA : SC;
                ld_AddrLo <= 0;
                dv <= (memDataAvail) ? 1: 0;
                en_Data <= (memDataAvail) ? 1 : 0;
            end       
            SD: begin  //write
                State <= (memBus.dataValid) ? SE : SD;
                ld_AddrLo <= 0;
                ld_Data <= (memBus.dataValid) ? 1: 0;
            end
            SE: begin  //write
                State <= SA ;
                ld_AddrLo <= 0;
                ld_Data <= 0;
            end
        endcase
    end
end
//always_comb begin
//    ld_AddrUp = 0;
//    ld_AddrLo = 0;
//    dv = 0;
//    en_Data = 0;
//    ld_Data = 0;
//    case (State)
//        SA: begin
//            NextState = (memBus.start) ? SB : SA;
//            ld_AddrUp = (memBus.start) ? 1: 0;
//        end
//        SB: begin
//            NextState = (memBus.read) ? SC : SD;
//            ld_AddrLo = 1;
//        end
//        SC: begin
//            NextState = (memDataAvail) ? SA : SC;
//            dv = (memDataAvail) ? 1: 0;
//            en_Data = (memDataAvail) ? 1 : 0;
//        end
//        SD: begin
//            NextState = (memBus.dataValid) ? SA : SD;
//            ld_Data = (memBus.dataValid) ? 1: 0;
//        end
//    endcase

//end
endmodule
