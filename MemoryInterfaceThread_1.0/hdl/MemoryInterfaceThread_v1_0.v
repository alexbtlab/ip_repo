
`timescale 1 ns / 1 ps

	
module MemIntThread(
    input logic resetN, clock,
    input logic start, read,
    inout logic dataValid,
    input logic [7:0] address,
    inout logic [7:0] data);


logic
logic

logic
logic

[7:0] Mem [16°hFFFF: 0], MemData;

\d_AddrUp, Ud_AddrLo,

menDataAvail = 0, en_Data, ld_Data, dv;
[7:0] DataReg;
[15:0] AddrReg;

86 enum {SA, SB, SC, SD} State, NextState;


iumttal begin
for (int i = 0; i <= 16°hFFFF; i++)
Men[i] = i[7:0];
end

assign data = (en_Data) ? MenData : ‘bz;
assign dataValid = (State == SC) ? dv : 1’bz;
always @(AddrReg, 1d_Data)

MemData = Mem[AddrReg] ;

always_ff @(posedge clock)
Uf (ld_AddrUp) AddrReg[15:8] <= address;



always_ff @(posedge clock)
if (1d_AddrLo) AddrReg[7:0] <= address;

always @(posedge clock) begin
if (ld_Data) begin
DataReg <= data;
Mem [AddrReg] <= data;
end
end

always_ff @(posedge clock, negedge resetN)
if (~resetN) State <= SA;
else State <= NextState;


always_comb begin
d_AddrUp = 0;
Id_AddrLo = 0;

 

 

dv = 0;
en_Data
1d_Data
case (State)
SA: begin
NextState = (start) ? SB : SA;
Id_AddrUp = (start) 2.1: 0;
end
SB: begin
NextState = (read) ? SC : SD;
Id_AddrLo = 1;
end
SC: begin
NextState = (memDataAvail) ? SA : SC
dv = (memDataAvail) ? 1: 0;
en_Data = (memDataAvail) ? 1
end
SD: begin
NextState = (dataValid) ? SA : SD;
ld_Data = (dataValid) ? 1: 0;
end
endcase

end
