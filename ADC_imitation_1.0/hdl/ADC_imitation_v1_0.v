`timescale 1 ns / 1 ps

`define STATE_WRITE 1
`define STATE_READ 0

module ADC_imitation(
    input wire  clk_5MHz,
    input wire  clk_100MHz,
    input wire  s00_axi_aresetn,
    input wire  last_word_transmit,
    
    input wire [31:0] adr_read_ram,
    input wire [15:0] FrameSize,
    
    output wire clk_5MHzWR_100MhzRD,
    output wire clk_100Mhz_AXI_send,
    output wire [31:0] adr_bram,
    output wire [31:0] data,
    output wire write
);
	
reg FLAG_state = `STATE_WRITE;
reg [31:0] count_clk5 = 0;
reg [31:0] count_clk100 = 0;
reg [31:0] count_clk = 0;

reg [31:0] data_adc_deserial = 0;
assign data = count_clk;//(write) ? data_adc_deserial : 1'b0;

reg [31:0] adr_write_ram = 0;

wire last_word_transmit_w;
assign last_word_transmit_w = last_word_transmit;

reg write_reg;
assign write = write_reg;

wire [31:0] adr_read_ram_reg = 0;
assign adr_read_ram_reg = adr_read_ram;

assign clk_5MHzWR_100MhzRD = (write_reg) ? clk_5MHz      : clk_100MHz;
assign clk_100Mhz_AXI_send = (write_reg) ? 1'b1          : ~clk_100MHz;
assign adr_bram            = count_clk;//(write_reg) ? adr_write_ram : count_clk100;

//////////////////////////////////////////////////////////////////
always @ (negedge clk_5MHz & s00_axi_aresetn) begin
    data_adc_deserial <= count_clk5;
    adr_write_ram <= count_clk5;
end
//////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
always @ (negedge clk_5MHzWR_100MhzRD & s00_axi_aresetn) begin
      
        if(count_clk == FrameSize) begin
            count_clk <= 0;
            if(FLAG_state == `STATE_WRITE) begin
                FLAG_state = `STATE_READ;
                write_reg <= 0;
            end 
            else if(FLAG_state == `STATE_READ) begin
                FLAG_state = `STATE_WRITE;
                write_reg <= 1;
            end 
        end 
        else begin
            count_clk <= count_clk + 1;
        end
end
//////////////////////////////////////////////////////////////////////////////////////////
endmodule
