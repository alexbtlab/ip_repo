
`timescale 1 ns / 1 ps

	module sampleGenerator_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Users to add ports here
		input [7:0] FrameSize,
		input AXI_En,
		input En,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);
		wire  m00_axis_tvalidW;
		wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdataW;
		wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrbW;
		wire  m00_axis_tlastW;
		wire  s00_axis_treadyW;
		wire  m00_axis_treadyW;
// Instantiation of Axi Bus Interface S00_AXIS

        
	sampleGenerator_v1_0_S00_AXIS # ( 
		.C_S_AXIS_TDATA_WIDTH(C_S00_AXIS_TDATA_WIDTH)
	) sampleGenerator_v1_0_S00_AXIS_inst (
	    .En(En),
		.S_AXIS_ACLK(s00_axis_aclk),
		.S_AXIS_ARESETN(s00_axis_aresetn),
		.S_AXIS_TREADY(s00_axis_treadyW),
		.S_AXIS_TDATA(s00_axis_tdata),
		.S_AXIS_TSTRB(s00_axis_tstrb),
		.S_AXIS_TLAST(s00_axis_tlast),
		.S_AXIS_TVALID(s00_axis_tvalid)
	);
    

// Instantiation of Axi Bus Interface M00_AXIS
	sampleGeneraror_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) samplGeneraror_v1_0_M00_AXIS_inst (
		.FrameSize(FrameSize),
		.En(En),
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalidW),
		.M_AXIS_TDATA(m00_axis_tdataW),
		.M_AXIS_TSTRB(m00_axis_tstrbW),
		.M_AXIS_TLAST(m00_axis_tlastW),
		.M_AXIS_TREADY(s00_axis_tready)
	);

	// Add user logic here
	assign m00_axis_tdata = (AXI_En) ? s00_axis_tdata : m00_axis_tdataW;
	assign m00_axis_tstrb = (AXI_En) ? s00_axis_tstrb : m00_axis_tstrbW;
	assign m00_axis_tlast = (AXI_En) ? s00_axis_tlast : m00_axis_tlastW;
	assign m00_axis_tvalid = (AXI_En) ? s00_axis_tvalid : m00_axis_tvalidW;
	assign s00_axis_tready = m00_axis_tready;
	// User logic ends

	endmodule
