`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.04.2020 09:54:36
// Design Name: 
// Module Name: tb_sampleGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_sampleGenerator();

reg         AXI_En;
reg         En;
reg  [7:0]  FrameSize;
wire [31:0]  M00_AXIS_tdata;
wire         M00_AXIS_tlast;
reg          M00_AXIS_tready;
wire [3:0]   M00_AXIS_tstrb;
wire         M00_AXIS_tvalid;
reg [31:0]   S00_AXIS_tdata;
reg          S00_AXIS_tlast;
wire         S00_AXIS_tready; 
reg [3:0]   S00_AXIS_tstrb;
reg         S00_AXIS_tvalid;
reg         Clk;
reg         ResetN;

initial begin
    AXI_En = 0;
    FrameSize =  4;
    S00_AXIS_tdata = 0;
    S00_AXIS_tlast =  0;
    S00_AXIS_tstrb =  0;
    S00_AXIS_tvalid = 0;
end
initial begin
    Clk = 0;
    forever #5 Clk = ~Clk;
end
initial begin
    ResetN = 0;
    #100 ResetN = 1;
end
initial begin
    En = 1;
    #1000 En = 0;
    #100 En = 1;
end

initial begin
    M00_AXIS_tready = 0;
    #200 M00_AXIS_tready = 1;
    #2000 M00_AXIS_tready = 0;
    #200 M00_AXIS_tready = 1;
end

design_1_wrapper DUT
   (.AXI_En(AXI_En),
    .En(En),
    .FrameSize(FrameSize),
    .M00_AXIS_tdata(M00_AXIS_tdata),
    .M00_AXIS_tlast(M00_AXIS_tlast),
    .M00_AXIS_tready(M00_AXIS_tready),
    .M00_AXIS_tstrb(M00_AXIS_tstrb),
    .M00_AXIS_tvalid(M00_AXIS_tvalid),
    .S00_AXIS_tdata(S00_AXIS_tdata),
    .S00_AXIS_tlast(S00_AXIS_tlast),
    .S00_AXIS_tready(S00_AXIS_tready),
    .S00_AXIS_tstrb(S00_AXIS_tstrb),
    .S00_AXIS_tvalid(S00_AXIS_tvalid),
    .m00_axis_aclk(Clk),
    .m00_axis_aresetn(ResetN)
    );

endmodule
