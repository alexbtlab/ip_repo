library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity dna_reader_fsm is
	port(
		--����� �����
		iclk			: in std_logic;
		istart			: in std_logic; --������ ������� ������ DNA
		--����� ���������� dna_port
		odna_port_read	: out std_logic;
		odna_port_shift : out std_logic;
		idna_port_dout	: in  std_logic;
		--DNA
		odna_ready		: out std_logic;	--������ ��������� ������ dna
		odna			: out std_logic_vector(63 downto 0)
	);
end dna_reader_fsm;

architecture rtl of dna_reader_fsm is
	
	--��������� ���������� ��������
	type state_type is (s0, s1, s2, s3, s4, s5);
	signal state : state_type := s0;
	
	signal write_srl : std_logic := '0';	--������ ������ � ��������� ������� ������ ������������� �������� DNA
	signal k : natural range 0 to 63 := 0;	--������� ��� ������������ ���������� ����������� ��� �� DNA_PORT
	
	signal dna_port_read, dna_port_shift : std_logic := '0';		--������� ���������� DNA_PORT
	signal dna : std_logic_vector(odna'range) := (others => '0');	--��������� ������ ��� ������ ������������� ������ DNA
	signal dna_ready: std_logic := '0';								--������ ��������� ������ dna			
	
begin
	
	FSM: process(iclk)
	begin
		if rising_edge(iclk) then
			dna_port_read 	<= '0';
			dna_port_shift	<= '0';
			write_srl	<= '0';
			case state is
				when s0 =>  if istart = '1' then
								dna_port_read 	<= '1'; --��������� DNA �� ��������� ������� ��� ������������ ������
								state 			<= s1;
							end if;
							
				when s1 => state <= s2;	--���� ���� ���� ��������� ������ �� ������ DOUT DNA_PORT
					
				when s2	=>	write_srl 	<= '1'; --���������� �������� � �������� �������
				            k <= k + 1;			--����������� ���������� ����������� ���
							state 		<= s3;
							
				when s3	=>	dna_port_shift 	<= '1';	--��������� ����� ��� ������ ���������� ���� ������
							state 		<= s4;
							
				when s4	=>	if k = 56 then		--��������� ������� ������ ���� �������
								state 	<= s5;	--����� �� ���� ���� ���� ��� ��������� �������� �� ������ DOUT DNA_PORT
							else	
								state 	<= s2;
							end if;
							
							
				when s5	=> 	dna_ready 	<= '1';	--������������� ������ ��������� ������ DNA
							state 		<= s5;	
			end case;
		end if;
	end process FSM;
	
	
	--��������� ������� ��� �������� DNA
	SRL_DNA: process(iclk)
	begin
		if rising_edge(iclk) then
			if write_srl = '1' then
				for i in 1 to 63 loop
					dna(i) <= dna(i-1);
				end loop;
			end if;
		end if;
		dna(0) <= idna_port_dout;
	end process SRL_DNA;
	
	odna <= dna;
	odna_ready	<= dna_ready;
	
	odna_port_read  <= dna_port_read;
	odna_port_shift <= dna_port_shift;	
end rtl;
