library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity dna_port_wrapper is
	port(
		odout 	: out std_logic;
		iclk	: in  std_logic;
		idin	: in std_logic;
		iread	: in std_logic;
		ishift	: in std_logic
	);
end dna_port_wrapper;

architecture structural of dna_port_wrapper is

begin

DNA_PORT_inst : DNA_PORT
   generic map (
      SIM_DNA_VALUE => X"1D01234567890a2"  -- Specifies a sample 57-bit DNA value for simulation
   )
   port map (
      DOUT => odout,   -- 1-bit output: DNA output data.
      CLK => iclk,     -- 1-bit input: Clock input.
      DIN => idin,     -- 1-bit input: User data input pin.
      READ => iread,   -- 1-bit input: Active high load DNA, active low read input.
      SHIFT => ishift  -- 1-bit input: Active high shift enable input.
   );
end structural;
