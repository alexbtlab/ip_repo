----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 07.11.2017 14:03:24
-- Design Name: 
-- Module Name: top - structural
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( iclk : in STD_LOGIC;
           istart : in STD_LOGIC);
end top;

architecture structural of top is
	
  component dna_reader_top_wrapper is
  port (
    clk_in1 : in STD_LOGIC;
    istart : in STD_LOGIC;
    odna : out STD_LOGIC_VECTOR ( 63 downto 0 );
    odna_ready : out STD_LOGIC
  );
end component dna_reader_top_wrapper;
	
begin
	
  inst_dna_reader: dna_reader_top_wrapper
  port map(
    clk_in1    => iclk,
    istart     => istart,
    odna       => open,
    odna_ready => open
  );
end structural;
