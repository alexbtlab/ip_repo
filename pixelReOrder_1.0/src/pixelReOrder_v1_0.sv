`timescale 1 ns / 1 ps

`define out_enable_16_null 5'h10
`define NUM_CELL_MEM 16

	module pixelReOrder_v1_0 #
	(
		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,

		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32
	)
	(
		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,

		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [47 : 0] s00_axis_tuser,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid
	);
     
	/* --- Commutation in & out mem --- */
    reg  [31:0] DI1 [15:0];
	reg  [31:0] DI2 [15:0];
    reg  [31:0] DOUT1,   DOUT2 [15 : 0];
	// wire [31:0] DI1_w,   _DI2   [15 : 0];
	wire [31:0] DI1_w [15:0];
	wire [31:0] _DI2 [15:0];
	wire [31:0] _DOUT_1 [15:0];
	wire [31:0] _DOUT_2 [15:0];

//	genvar ind_mem;        
//	generate        
//	for (ind_mem = 0; ind_mem < 16 ; ind_mem = ind_mem + 1) begin        
//			assign DI1_w[ind_mem] = DI1[ind_mem];  
//			assign _DI2[ind_mem] = DI2[ind_mem];     
//		end
//	endgenerate 
    assign DI1_w[0] = DI1[0]; 
    assign DI1_w[1] = DI1[1]; 
    assign DI1_w[2] = DI1[2]; 
    assign DI1_w[3] = DI1[3]; 
    assign DI1_w[4] = DI1[4]; 
    
    assign DI1_w[5] = DI1[5]; 
    assign DI1_w[6] = DI1[6]; 
    assign DI1_w[7] = DI1[7]; 
    assign DI1_w[8] = DI1[8]; 
    assign DI1_w[9] = DI1[9]; 
    assign DI1_w[10] = DI1[10]; 
    assign DI1_w[11] = DI1[11]; 
    assign DI1_w[12] = DI1[12]; 
    assign DI1_w[13] = DI1[13]; 
    assign DI1_w[14] = DI1[14]; 
    assign DI1_w[15] = DI1[15];
    
	/* --- Commutation ctrl & data mem --- */
	wire CLK, WE_1, WE_2, RST;
    wire [15:0] EN_mem1, EN_mem2;
	wire [15:0] ADDR;

	/*-----    MEM   ------------------------------------------------------------------------------------------*/
	genvar j;        
		generate        
		for (j = 0; j < `NUM_CELL_MEM ; j = j + 1)   begin      
			rams_sp_rf_rst rams_sp_rf_rst_inst0( .clk(CLK), .en(EN_mem1[j]), .we(WE_1), .rst(RST), .addr(ADDR),   .di(s00_axis_tdata),   .dout(_DOUT_1[j]) );     
			rams_sp_rf_rst rams_sp_rf_rst_inst1( .clk(CLK), .en(EN_mem2[j]), .we(WE_2), .rst(RST), .addr(ADDR),   .di(s00_axis_tdata),  .dout(_DOUT_2[j]) );     
		end
	endgenerate 
	/*-----   OUT  ------------------------------------------------------------------------------*/


	multiplexer # ( 
		.N(C_M00_AXIS_TDATA_WIDTH)
	)
    multiplexer_inst
    (       .d0(_DOUT_1[0]),     .d1(_DOUT_1[1]),   .d2(_DOUT_1[2]),        .d3(_DOUT_1[3]),
            .d4(_DOUT_1[4]),     .d5(_DOUT_1[5]),   .d6(_DOUT_1[6]),        .d7(_DOUT_1[7]),
			.d8(_DOUT_1[8]),     .d9(_DOUT_1[9]),   .d10(_DOUT_1[10]),      .d11(_DOUT_1[11]),
            .d12(_DOUT_1[12]),   .d13(_DOUT_1[13]), .d14(_DOUT_1[14]),      .d15(_DOUT_1[15]),
			
			.d16(32'h00000000),   .ctrl(ctrl),    .y(out_mux_1)                  
    );
	multiplexer # ( 
		.N(C_M00_AXIS_TDATA_WIDTH)
	)
    multiplexer2_inst
    (       .d0(_DOUT_2[0]),     .d1(_DOUT_2[1]),   .d2(_DOUT_2[2]),        .d3(_DOUT_2[3]),
            .d4(_DOUT_2[4]),     .d5(_DOUT_2[5]),   .d6(_DOUT_2[6]),        .d7(_DOUT_2[7]),
			.d8(_DOUT_2[8]),     .d9(_DOUT_2[9]),   .d10(_DOUT_2[10]),      .d11(_DOUT_2[11]),
            .d12(_DOUT_2[12]),   .d13(_DOUT_2[13]), .d14(_DOUT_2[14]),      .d15(_DOUT_2[15]),
			
			.d16(32'h00000000),   .ctrl(ctrl),    .y(out_mux)                  
    );

 	reg [15:0] EN1, EN2;
	assign EN_mem1 = EN1;
	assign EN_mem2 = EN2;

	assign m00_axis_tstrb = 4'b1111;
 	//reg [15:0] cnt_strok_async = 0;
 	reg m00_axis_tlast_r = 0;
 	assign m00_axis_tlast = m00_axis_tlast_r;

    assign RST = 1'b0;
	
	reg [15:0] i;
	wire [15:0] shift_val;
	assign shift_val[7:4] = s00_axis_tuser[23:20];
	assign shift_val[3:0] = 0;
	assign shift_val[15:8] = 0;
	reg [15:0] tmpbus_r;
	wire [15:0] tmpbus2;
	assign tmpbus2[3:0] = 0;
	reg [9:0] adr_read;
	wire [15:0] adr_wr;
	assign s00_axis_tready = 1'b1;
	assign CLK = m00_axis_aclk;
    reg state_mem1_wr;
    reg [15:0] adr_read_reorder_data_mem1;
	reg [15:0] active_mem1_in_read_state = 0;
	reg [15:0] active_mem2_in_read_state = 0;
	reg [15:0] cnt_valid_r = 0;
	reg [15:0] cnt_strok_sync;
    assign adr_wr = s00_axis_tuser[15:0];
	
	assign adr_wr = adr_wr_r + cnt_valid_r;
	reg write_state_mem1;
    assign ADDR = ( Y_up[0] == 0 ) ? adr_wr  : adr_read_reorder_data_mem1;

	wire [15 : 0] X;
	wire [15 : 0] Y;
	wire [15 : 0] Y_up;
	assign X = s00_axis_tuser[15:0];
	assign Y = s00_axis_tuser[31:16];
	assign Y_up = s00_axis_tuser[23:20];

	reg [15:0] adr_wr_r;
	assign adr_wr = X;

	reg [31:0] m00_axis_tdata_r;
    reg square_r;
    reg WE_1_r;
	reg [15:0] num_string_image_sync;
	assign WE_1 = (num_string_image_sync[0] == 0) ? s00_axis_tvalid : 0;
	assign WE_1 = (square_r) ? WE_1_r : 0;
	wire [31:0] out_mux;
	wire [31:0] out_mux_1;
	assign m00_axis_tdata = out_mux_1;
	wire [4:0] ctrl;
	reg [4:0] ctrl_mux_out_1;
	assign ctrl = ctrl_mux_out_1;
	reg [15:0] double_cycle;
	
	wire [3 : 0] Y_low;
	assign Y_low = s00_axis_tuser[19:16];


	reg [15:0] cnt_kvadratik_in_stroke = 0;
 	reg [15:0]test;   
	reg [15:0] cnt_inner_valid;
	
	reg m00_axis_tvalid_r;
	reg cnt_enable;
	assign m00_axis_tvalid = m00_axis_tvalid_r;
	wire EN1_w;
	assign EN1_w = s00_axis_tuser[31:16];
	reg num_string_image_sync_r;
	assign num_string_image_sync = Y_up;

	always @ (posedge m00_axis_aclk) begin

		if(s00_axis_aresetn) 	        cnt_strok_sync <= cnt_strok_async;
		if(!s00_axis_aresetn) 	        adr_read_reorder_data_mem1 <= 16'hFFFF;
		if(!s00_axis_aresetn) 	        num_string_image_sync <= 0;				
		if(num_string_image_sync[0] == 0 )		ctrl_mux_out_1  <= `out_enable_16_null;  // 2,4,6,8,... string is active
		

		if(s00_axis_tvalid & s00_axis_aresetn) begin
			

			if(Y_up == 1919) begin
				if(cnt_enable) begin
					num_string_image_sync <= Y_up; 
					cnt_enable <= 0;
				end	
			end
			else begin
				num_string_image_sync <= num_string_image_sync;
			end

				
					if(cnt_enable == 1) begin 
						cnt_enable <= 0;
						// cnt_valid_r <= cnt_valid_r + 1;	
						// 	if(cnt_valid_r == 120) begin
						// 		cnt_valid_r <= 16'h0;
						// 		num_string_image_sync <= num_string_image_sync + 1;				
						// 	end	
						
					end
					else begin
						cnt_valid_r <= cnt_valid_r;
					end
		end
		else begin
			cnt_enable <= 1;
			cnt_valid_r <= 16'h0;
		end

		if(s00_axis_aresetn) begin
			/*--------------------------------------------------------------*/
			if(num_string_image_sync[0] == 1) begin   // 2,4,6,8,...
				if(cnt_inner_valid >= 0 & cnt_inner_valid < 16'hF)			 	EN1[0] <= 1;
				else															EN1[0] <= 0;
				if(cnt_inner_valid >= 0 & cnt_inner_valid < 16'hF)			 	EN1[1] <= 1;
				else															EN1[1] <= 0;
				for (i = 0; i < `NUM_CELL_MEM; i = i + 1) begin
					if(active_mem1_in_read_state == i) EN1[i] <= 1;
                    else                               EN1[i] <= 0;
				end
				for (i = 0; i < `NUM_CELL_MEM; i = i + 1) begin
					EN2[i] <= 0;
				end
			end	
			else begin 							// 1,3,5,7,...
				for (i = 0; i < `NUM_CELL_MEM; i = i + 1) begin
					if(active_mem2_in_read_state == i) EN2[i] <= 1;
                    else                               EN2[i] <= 0;
				end
				for (i = 0; i < `NUM_CELL_MEM; i = i + 1) begin
					EN1[i] <= 0;
				end
			end
			/*--------------------------------------------------------------*/
			if(num_string_image_sync[0] == 0) begin	// odd or even
				
				if(cnt_inner_valid >= 0 & cnt_inner_valid < 16'hF)			 	EN1[0] <= 1;
				else															EN1[0] <= 0;
				if(cnt_inner_valid >= 16'hF & cnt_inner_valid < 16'h1F)		    EN1[1] <= 1;
				else															EN1[1] <= 0;
				if(cnt_inner_valid >= 16'h1F & cnt_inner_valid < 16'h2F)		EN1[2] <= 1;
				else															EN1[2] <= 0;
				if(cnt_inner_valid >= 16'h2F & cnt_inner_valid < 16'h3F)		EN1[3] <= 1;
				else															EN1[3] <= 0;
				if(cnt_inner_valid >= 16'h3F & cnt_inner_valid < 16'h4F)		EN1[4] <= 1;
				else															EN1[4] <= 0;
				if(cnt_inner_valid >= 16'h4F & cnt_inner_valid < 16'h5F)		EN1[5] <= 1;
				else															EN1[5] <= 0;
				if(cnt_inner_valid >= 16'h5F & cnt_inner_valid < 16'h6F)		EN1[6] <= 1;
				else															EN1[6] <= 0;
				if(cnt_inner_valid >= 16'h6F & cnt_inner_valid < 16'h7F)		EN1[7] <= 1;
				else															EN1[7] <= 0;
				if(cnt_inner_valid >= 16'h7F & cnt_inner_valid < 16'h8F)		EN1[8] <= 1;
				else															EN1[8] <= 0;
				if(cnt_inner_valid >= 16'h8F & cnt_inner_valid < 16'h9F)		EN1[9] <= 1;
				else															EN1[9] <= 0;
				if(cnt_inner_valid >= 16'h9F & cnt_inner_valid < 16'hAF)		EN1[10] <= 1;
				else															EN1[10] <= 0;
				if(cnt_inner_valid >= 16'hAF & cnt_inner_valid < 16'hBF)		EN1[11] <= 1;
				else															EN1[11] <= 0;
				if(cnt_inner_valid >= 16'hBF & cnt_inner_valid < 16'hCF)		EN1[12] <= 1;
				else															EN1[12] <= 0;
				if(cnt_inner_valid >= 16'hCF & cnt_inner_valid < 16'hDF)		EN1[13] <= 1;
				else															EN1[13] <= 0;
				if(cnt_inner_valid >= 16'hDF & cnt_inner_valid < 16'hEF)		EN1[14] <= 1;
				else															EN1[14] <= 0;
				if(cnt_inner_valid >= 16'hEF & cnt_inner_valid < 16'hFF)		EN1[15] <= 1;
				else															EN1[15] <= 0;

				if(cnt_inner_valid < 16 & cnt_inner_valid < 31)	
				 	EN1[2] <= 1;
				else
					EN1[2] <= 0;
					

			end


		end
			
	    if( s00_axis_aresetn) begin
				if(num_string_image_sync[0] == 0 )	begin
						// write_state_mem1 <= 1;
						m00_axis_tlast_r <= 0;
						adr_read_reorder_data_mem1 <= 16'hFFFF;
						
 
				
							 for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
							 	if(Y_low == i)   DI1[i] <= s00_axis_tdata;
							
										 
				end
				if(   ((s00_axis_tuser[31:16] ) >= double_cycle + 16'h10) & ((s00_axis_tuser[31:16] ) < (double_cycle + 16'h20))	) begin	 /* --------- */
				if(num_string_image_sync[0] == 1 )	begin

							for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
							 	if(Y_low == i)   DI1[i] <= 0;

							// write_state_mem1 <= 0; 
							ctrl_mux_out_1 <= active_mem1_in_read_state;
							adr_read_reorder_data_mem1 <= adr_read_reorder_data_mem1 + 1;
							//adr_wr_r <= 0;

									if(adr_read_reorder_data_mem1 == 1920) begin 
										adr_read_reorder_data_mem1 <= 16'hFFFF;
										if(active_mem1_in_read_state != 16'hF)		active_mem1_in_read_state <= active_mem1_in_read_state + 1;
										else										active_mem1_in_read_state <= 0;				            
									end					
                end  
		end
			
		if(s00_axis_tvalid & s00_axis_aresetn) begin
					
			if(cnt_inner_valid > 0 & cnt_inner_valid <=  16)			m00_axis_tvalid_r <= 1;
			else                                         				m00_axis_tvalid_r <= 0;
			if(cnt_inner_valid == 1920)							cnt_inner_valid <= 0;
			else               							        cnt_inner_valid <= cnt_inner_valid + 1;
			
			adr_wr <= s00_axis_tuser[15:0] ;
			WE_1_r <= 1'b1;
		        
			if( 	(s00_axis_tuser[31:16] >> 4) & 1'b1 )			square_r <= 1'b0;
			else													square_r <= 1'b1;
			/*-------- EN Enable path ------------------------------------------------------------------------------------*/
			
			
				
				
				
				
/*-------------------------1,2 STRING-------------------------------------------------------------------------------------*/				
			
		for (double_cycle = 0; double_cycle < 16'h400; double_cycle = double_cycle + 16'h20)begin		
				

				if(  ((s00_axis_tuser[31:16] ) >= double_cycle) & ((s00_axis_tuser[31:16] ) < double_cycle + 16'h10)	) begin
						write_state_mem1 <= 1;
						m00_axis_tlast_r <= 0;
						adr_read_reorder_data_mem1 <= 16'hFFFF;
						adr_wr_r <= s00_axis_tuser[15:0];
 
							 for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
							 	if(s00_axis_tuser[31:16] == i)   DI1[i] <= s00_axis_tdata;
							
										
				end
				if(   ((s00_axis_tuser[31:16] ) >= double_cycle + 16'h10) & ((s00_axis_tuser[31:16] ) < (double_cycle + 16'h20))	) begin	 /* --------- */

							for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
								if(s00_axis_tuser[31:16] == i)   DI1[i] <= 0;
							write_state_mem1 <= 0; 
							ctrl_mux_out_1 <= active_mem1_in_read_state;
							adr_read_reorder_data_mem1 <= adr_read_reorder_data_mem1 + 1;
							adr_wr_r <= 0;

									if(adr_read_reorder_data_mem1 == 1920) begin 
										adr_read_reorder_data_mem1 <= 16'hFFFF;
										if(active_mem1_in_read_state != 16'hF)		active_mem1_in_read_state <= active_mem1_in_read_state + 1;
										else										active_mem1_in_read_state <= 0;				            
									end
										
            end  
		end
		
		if(  ((s00_axis_tuser[31:16] ) >= double_cycle) & ((s00_axis_tuser[31:16] ) < double_cycle + 16'h10)	) begin
	
		end
		else begin
		    ctrl_mux_out_1 <= 4'b0000;
			cnt_inner_valid <= 16'h0;
			WE_1_r <= 1'b0;

			for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
				if(s00_axis_tuser[31:16] == i)   DI1[i] <= 0;
			adr_wr <= 16'hffff;
			adr_wr <= 16'h0000;
		end
	end
	
	
    
	reg [15:0] cnt_valid_r2 = 0;

	always @ (posedge s00_axis_tvalid ) begin
		

		
		if(cnt_valid_r2  == 120) begin
				cnt_valid_r2 <= 0;
				if(num_string_image_sync != 50)
				num_string_image_sync <= num_string_image_sync + 1;
			else
				num_string_image_sync <= 0;
		end

		else  begin
		  

		  cnt_valid_r2 <= cnt_valid_r2 + 1;
		end
	end  
	
	always @ (posedge s00_axis_tlast ) begin
		cnt_kvadratik_in_stroke <= cnt_kvadratik_in_stroke + 1;
		if(s00_axis_tuser[15:0]  == 16'h0000)
		  cnt_kvadratik_in_stroke <= 0;
	end  
	endmodule
	
	

module multiplexer
#( parameter	 N = 32)
(
	input		[N-1:0]	d16,d15,d14,d13,d12,d11,d10,d9,d8,d7,d6,d5,d4,d3,d2,d1,d0, 	// data	
	input		[4:0]		ctrl,													// control
	output	reg	[N-1:0] y
);
 
	always @*
	case ({ctrl})
		5'b00000: y = d0;
		5'b00001: y = d1;
		5'b00010: y = d2;
		5'b00011: y = d3;
		5'b00100: y = d4;
		5'b00101: y = d5;
		5'b00110: y = d6;
		5'b00111: y = d7;
		5'b01000: y = d8;
		5'b01001: y = d9;
		5'b01010: y = d10;
		5'b01011: y = d11;
		5'b01100: y = d12;
		5'b01101: y = d13;
		5'b01110: y = d14;
		5'b01111: y = d15;
		5'b10000: y = d16;
	endcase
endmodule


















































//	always @ (posedge square_r, negedge square_r) begin
//		cnt_strok_async <= cnt_strok_async + 1;
//	end 

/*----------------------- 3,4 STRING ----------------------------------------------------------------------------------*/
				// if(   ((s00_axis_tuser[31:16] ) >= 16'h20) & ((s00_axis_tuser[31:16] ) < 16'h30)	) begin	
				// 	write_state_mem1 <= 1;
				// 	m00_axis_tlast_r <= 0;
				//     adr_read_reorder_data_mem1 <= 16'hFFFF;

				// 	for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
				// 		if(s00_axis_tuser[31:16] == i)   DI1[i] <= s00_axis_tdata;
				// end
				// if(   ((s00_axis_tuser[31:16] ) >= 16'h30) & ((s00_axis_tuser[31:16] ) < 16'h40)	) begin
							
				// 			write_state_mem1 <= 0; 
				// 			//ctrl_mux_out_1 <= active_mem1_in_read_state;
				// 			adr_read_reorder_data_mem1 <= adr_read_reorder_data_mem1 + 1;
									
				// 					if(adr_read_reorder_data_mem1 == 1920) begin 
				// 						adr_read_reorder_data_mem1 <= 16'hFFFF;
				// 						active_mem1_in_read_state <= active_mem1_in_read_state + 1;				            
				// 					end			
				// end
				
				/*------5,6 STRING------------------------------------------------------------------------------------*/
				// if(   ((s00_axis_tuser[31:16] ) >= 16'h40) & ((s00_axis_tuser[31:16] ) < 16'h50)	) begin
				// 	write_state_mem1 <= 1;
				// 	m00_axis_tlast_r <= 0;
				//     adr_read_reorder_data_mem1 <= 16'hFFFF;

				// 	for(i = 0; i < `NUM_CELL_MEM; i = i + 1)
				// 		if(s00_axis_tuser[31:16] == i)   DI1[i] <= s00_axis_tdata;
				// end
				// if(   ((s00_axis_tuser[31:16] ) >= 16'h50) & ((s00_axis_tuser[31:16] ) < 16'h60)	) begin
				      		
				// 			write_state_mem1 <= 0; 
				// 			//ctrl_mux_out_1 <= active_mem1_in_read_state;
				// 			adr_read_reorder_data_mem1 <= adr_read_reorder_data_mem1 + 1;
									
				// 					if(adr_read_reorder_data_mem1 == 1920) begin 
				// 						adr_read_reorder_data_mem1 <= 16'hFFFF;
				// 						active_mem1_in_read_state <= active_mem1_in_read_state + 1;				            
				// 					end	  
				    
				            
				// end

		
//				/*------------------------------------------------------------------------------------------------*/
//				if(   ((s00_axis_tuser[31:16] ) >= 16'h60) & ((s00_axis_tuser[31:16] ) < 16'h70)	) begin
//					DI3 <= s00_axis_tdata; 
//				end
//				if(   ((s00_axis_tuser[31:16] ) >= 16'h70) & ((s00_axis_tuser[31:16] ) < 16'h80)	) begin
//					                if(s00_axis_tuser[31:16]  == 16'h70)		ctrl_mux_out_1 <= 4'b0000;		if(s00_axis_tuser[31:16]  == 16'h71)		ctrl_mux_out_1 <= 4'b0001;
//								 if(s00_axis_tuser[31:16]  == 16'h72)		ctrl_mux_out_1 <= 4'b0010;		if(s00_axis_tuser[31:16]  == 16'h73)		ctrl_mux_out_1 <= 4'b0011;
//								 if(s00_axis_tuser[31:16]  == 16'h74)		ctrl_mux_out_1 <= 4'b0100;		if(s00_axis_tuser[31:16]  == 16'h75)		ctrl_mux_out_1 <= 4'b0101;
//								 if(s00_axis_tuser[31:16]  == 16'h76)		ctrl_mux_out_1 <= 4'b0110;		if(s00_axis_tuser[31:16]  == 16'h77)		ctrl_mux_out_1 <= 4'b0111;
//								 if(s00_axis_tuser[31:16]  == 16'h78)		ctrl_mux_out_1 <= 4'b1000;		if(s00_axis_tuser[31:16]  == 16'h79)		ctrl_mux_out_1 <= 4'b1001;
//								 if(s00_axis_tuser[31:16]  == 16'h7A)		ctrl_mux_out_1 <= 4'b1010;		if(s00_axis_tuser[31:16]  == 16'h7B)		ctrl_mux_out_1 <= 4'b1011;
//								 if(s00_axis_tuser[31:16]  == 16'h7C)		ctrl_mux_out_1 <= 4'b1100;		if(s00_axis_tuser[31:16]  == 16'h7D)		ctrl_mux_out_1 <= 4'b1101;
//								 if(s00_axis_tuser[31:16]  == 16'h7E)		ctrl_mux_out_1 <= 4'b1110;		if(s00_axis_tuser[31:16]  == 16'h7F)		ctrl_mux_out_1 <= 4'b1111;
    //1111111111111111111111111111111111 for (i = 0; i < 16; i = i + 1)
                            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      if(s00_axis_tuser[31:16]  == 16'h50 + i )		ctrl_mux_out_1 <= i; 
					//				end