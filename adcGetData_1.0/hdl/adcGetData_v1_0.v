`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BTLabs
// Engineer: Nechaev
// 
// Create Date: 23.07.2019 12:42:55
// Design Name: MRLS
// Module Name: mrls_top
// Project Name: 
// Target Devices: XC7A100
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module adcGetData(
    input wire clk_100MHz,

    input wire  ch_num,

    input wire fpga_clk,

    output wire adc_if2_ch1_clk_p,
    output wire adc_if2_ch1_clk_n,
    input wire adc_if2_ch1_dco_p,
    input wire adc_if2_ch1_dco_n,
    input wire adc_if2_ch1_da_p,
    input wire adc_if2_ch1_da_n,
    input wire adc_if2_ch1_db_p,
    input wire adc_if2_ch1_db_n,

    output wire adc_if2_ch2_clk_p,
    output wire adc_if2_ch2_clk_n,
    input wire adc_if2_ch2_dco_p,
    input wire adc_if2_ch2_dco_n,
    input wire adc_if2_ch2_da_p,
    input wire adc_if2_ch2_da_n,
    input wire adc_if2_ch2_db_p,
    input wire adc_if2_ch2_db_n,

    output wire adc_if1_ch1_clk_p,
    output wire adc_if1_ch1_clk_n,
    input wire adc_if1_ch1_dco_p,
    input wire adc_if1_ch1_dco_n,
    input wire adc_if1_ch1_da_p,
    input wire adc_if1_ch1_da_n,
    input wire adc_if1_ch1_db_p,
    input wire adc_if1_ch1_db_n,

    output wire adc_if1_ch2_clk_p,
    output wire adc_if1_ch2_clk_n,
    input wire adc_if1_ch2_dco_p,
    input wire adc_if1_ch2_dco_n,
    input wire adc_if1_ch2_da_p,
    input wire adc_if1_ch2_da_n,
    input wire adc_if1_ch2_db_p,
    input wire adc_if1_ch2_db_n,

    output wire strobe_adc,
    output wire [15:0] adc_data_1, 
    output wire [15:0] adc_data_2
    );

    assign strobe_adc = clk_10MHz_inverted;
       
    wire clk_200MHz; 
   
    wire adc_if2_ch1_clk, adc_if2_ch1_dco, adc_if2_ch1_da, adc_if2_ch1_db;
    
    OBUFDS #( 
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard 
        .SLEW("FAST") // Specify the output slew rate 
    ) OBUFDS_adc_if2_ch1_clk ( 
        .O(adc_if2_ch1_clk_p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_if2_ch1_clk_n), // Diff_n output (connect directly to top-level port) 
        .I(adc_if2_ch1_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch1_dco ( 
        .O(adc_if2_ch1_dco), // Buffer output
        .I(adc_if2_ch1_dco_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_dco_n) // Diff_n buffer input (connect directly to top-level port)
    );
      
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da_if2_ch1 (
        .O(adc_if2_ch1_da), // Buffer output
        .I(adc_if2_ch1_da_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_da_n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch1_db (
        .O(adc_if2_ch1_db), // Buffer output
        .I(adc_if2_ch1_db_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_db_n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    wire adc_if1_ch1_clk, adc_if1_ch1_dco, adc_if1_ch1_da, adc_if1_ch1_db;
    
    OBUFDS #( 
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard 
        .SLEW("FAST") // Specify the output slew rate 
    ) OBUFDS_adc_if1_ch1_clk ( 
        .O(adc_if1_ch1_clk_p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_if1_ch1_clk_n), // Diff_n output (connect directly to top-level port) 
        .I(adc_if1_ch1_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if1_ch1_dco (
        .O(adc_if1_ch1_dco), // Buffer output
        .I(adc_if1_ch1_dco_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch1_dco_n) // Diff_n buffer input (connect directly to top-level port)
    );
      
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da_if1_ch1 (
        .O(adc_if1_ch1_da), // Buffer output
        .I(adc_if1_ch1_da_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch1_da_n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if1_ch1_db (
        .O(adc_if1_ch1_db), // Buffer output
        .I(adc_if1_ch1_db_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch1_db_n) // Diff_n buffer input (connect directly to top-level port)
    );
    
     wire adc_if2_ch2_clk, adc_if2_ch2_dco, adc_if2_ch2_da, adc_if2_ch2_db;
    
    OBUFDS #( 
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard 
        .SLEW("FAST") // Specify the output slew rate 
    ) OBUFDS_adc_if2_ch2_clk ( 
        .O(adc_if2_ch2_clk_p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_if2_ch2_clk_n), // Diff_n output (connect directly to top-level port) 
        .I(adc_if2_ch2_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch2_dco (
        .O(adc_if2_ch2_dco), // Buffer output
        .I(adc_if2_ch2_dco_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch2_dco_n) // Diff_n buffer input (connect directly to top-level port)
    );
      
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da_if2_ch2 (
        .O(adc_if2_ch2_da), // Buffer output
        .I(adc_if2_ch2_da_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch2_da_n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch2_db (
        .O(adc_if2_ch2_db), // Buffer output
        .I(adc_if2_ch2_db_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch2_db_n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    
   
    wire adc_if1_ch2_clk, adc_if1_ch2_dco, adc_if1_ch2_da, adc_if1_ch2_db;
    
    OBUFDS #( 
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard 
        .SLEW("FAST") // Specify the output slew rate 
    ) OBUFDS_adc_if1_ch2_clk ( 
        .O(adc_if1_ch2_clk_p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_if1_ch2_clk_n), // Diff_n output (connect directly to top-level port) 
        .I(adc_if1_ch2_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if1_ch2_dco (
        .O(adc_if1_ch2_dco), // Buffer output
        .I(adc_if1_ch2_dco_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch2_dco_n) // Diff_n buffer input (connect directly to top-level port)
    );
      
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da_if1_ch2 (
        .O(adc_if1_ch2_da), // Buffer output
        .I(adc_if1_ch2_da_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch2_da_n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if1_ch2_db (
        .O(adc_if1_ch2_db), // Buffer output
        .I(adc_if1_ch2_db_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if1_ch2_db_n) // Diff_n buffer input (connect directly to top-level port)
    );
   
   
    wire clk_8MHz;    
    wire sys_clk_locked;

    wire clk_uart;
    wire clk_pll_spi;
    wire clk_spi;
    wire clk_adc;
    wire clk_adc_delay;       
    wire adc_clk_uart;
      
    
    wire clk_10MHz_inverted;
    wire [15:0] adc_data_if1, adc_data_if2;
    
    assign adc_data_1 = adc_data_if1;
    assign adc_data_2 = adc_data_if2;
    
    wire adc_strobe_if1,adc_strobe_if2;    
    wire fpga_clk_buf_if1, fpga_clk_buf_if2;
    wire adc_clk_100MHz_if1, adc_clk_100MHz_if2;
     
    clk_wiz_2 clk_wizard_adc(
      .clk_out1(clk_10MHz_inverted),
      .clk_out2(fpga_clk_buf_if1),
      .clk_out3(fpga_clk_buf_if2),
      .clk_out4(adc_clk_100MHz_if1),
      .clk_out5(adc_clk_100MHz_if2),
      .clk_in1(fpga_clk)
      );
    
    wire ch_num_reg;
    //assign ch_num_reg = ch_num;

    assign adc_if2_ch2_clk = adc_if2_ch1_clk;
    
    adc_get_data adc_if2_get_data(
    .fpga_clk(fpga_clk_buf_if2),    
    .adc_dco((ch_num==0) ? adc_if2_ch1_dco : adc_if2_ch2_dco),
    .adc_da((ch_num==0) ? adc_if2_ch1_da : adc_if2_ch2_da),
    .adc_db((ch_num==0) ? adc_if2_ch1_db : adc_if2_ch2_db),
    .adc_clk(adc_if2_ch1_clk),
    .clk_100MHz(adc_clk_100MHz_if2), 
    .adc_data(adc_data_if2)
    );
    
    assign adc_if1_ch2_clk = adc_if1_ch1_clk;
    
    adc_get_data adc_if1_get_data(
    .fpga_clk(fpga_clk_buf_if1),    
    .adc_dco((ch_num==1) ? adc_if1_ch1_dco : adc_if1_ch2_dco),
    .adc_da((ch_num==1) ? adc_if1_ch1_da : adc_if1_ch2_da),
    .adc_db((ch_num==1) ? adc_if1_ch1_db : adc_if1_ch2_db),
    .adc_clk (adc_if1_ch1_clk),
    .clk_100MHz(adc_clk_100MHz_if1),
    .adc_data(adc_data_if1)
    );
          
endmodule

