`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2017 17:00:50
// Design Name: 
// Module Name: adc_get_data
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adc_get_data(   
    
    //input wire clk_100MHz_adc,    
    input wire tsweep,
    input wire fpga_clk,    
    input wire adc_dco,
    input wire adc_da,
    input wire adc_db,
    input wire uart_clk,    
    output wire [7:0] output_data,
    output wire [15:0] adc_data_out,    
    input wire is_sending,
    output wire send_uart_byte,
    output wire adc_clk
    
    /*output wire test_signal_1,
    output wire test_signal_2,
    output wire test_signal_3,
    output wire test_signal_4,
    output wire test_signal_5,
    output wire test_signal_6,
    output wire test_signal_7 */
   
    );
    
    wire clk_adc;
    
    reg[7:0] output_data_reg =0;
    reg send_uart_byte_reg = 0;
    reg [15:0] cnt_uart = 0;
    
    assign output_data = output_data_reg;
    assign send_uart_byte = send_uart_byte_reg;
    
    reg data_15 = 0;
    reg data_14 = 0;
    reg data_13 = 0;
    reg data_12 = 0;
    reg data_11 = 0;
    reg data_10 = 0;
    reg data_9 = 0;
    reg data_8 = 0;
    reg data_7 = 0;
    reg data_6 = 0;
    reg data_5 = 0;
    reg data_4 = 0;
    reg data_3 = 0;
    reg data_2 = 0;
    reg data_1 = 0;
    reg data_0 = 0;
    
    wire[15:0] adc_data;
    reg adc_reading_allowed_reg = 0;
    //assign adc_data = (data_15 << 15) | 
    //                  (data_14 << 14) | 
    //                  (data_13 << 13) | 
    //                  (data_12 << 12) | 
    //                  (data_11 << 11) | 
    //                  (data_10 << 10) | 
    //                  (data_9 << 9) | 
    //                  (data_8 << 8) | 
    //                  (data_7 << 7) | 
    //                  (data_6 << 6) | 
    //                  (data_5 << 5) | 
    //                  (data_4 << 4) | 
    //                  (data_3 << 3) | 
    //                  (data_2 << 2) | 
    //                  (data_1 << 1) | 
    //                  data_0;
    
    assign adc_data[15] = data_15;
    assign adc_data[14] = data_14;
    assign adc_data[13] = data_13;
    assign adc_data[12] = data_12;
    assign adc_data[11] = data_11;
    assign adc_data[10] = data_10;
    assign adc_data[9] = data_9;
    assign adc_data[8] = data_8;
    assign adc_data[7] = data_7;
    assign adc_data[6] = data_6;
    assign adc_data[5] = data_5;
    assign adc_data[4] = data_4;
    assign adc_data[3] = data_3;
    assign adc_data[2] = data_2;
    
    assign adc_data[1] = data_1;
    assign adc_data[0] = data_0;
    
    
    
    
     wire clk_bram;               
     wire clk_600MHz;
                      
    clk_wiz_1 clk_wizard_adc(
        
      .clk_out1(clk_adc),
      .clk_out2(clk_600MHz),
      .clk_out3(clk_bram),
      .clk_in1(fpga_clk)
      );
    
    localparam ADC_BUF_DEPTH=4096;
      
    //reg adc_bram_we=0;     
    //reg [15:0] adc_bram_addr_write = ADC_BUF_DEPTH;
    //reg [15:0] adc_bram_addr_read = 0;
    //reg [15:0] adc_bram_din = 0;
    //wire [15:0] adc_bram_dout;
     
          
    //bram bram_adc(
    //    .clk(clk_bram),
    //    .we(adc_bram_we),
    //    .addr_write(adc_bram_addr_write),
    //    .addr_read(adc_bram_addr_read),
    //    .din(adc_bram_din),
    //    .dout(adc_bram_dout)
    //);   
     

      
    assign adc_clk = clk_adc&(adc_reading_allowed_reg>0);
    
    reg [7:0] tconv_cnt_reg = 0;
    
    reg fpga_clk_prev = 0;
    
    reg tsweep_prev = 1;
    
    reg is_start_writing_bram_allowed = 0;
    
    always @(negedge clk_adc)
    begin  
        
          
        if (fpga_clk == 1)
        begin
            fpga_clk_prev <= 1;
            if (fpga_clk_prev==0) tconv_cnt_reg <= 0;
            else tconv_cnt_reg <= tconv_cnt_reg+1;
         end
         else
         begin
            fpga_clk_prev <= 0;
            tconv_cnt_reg <= tconv_cnt_reg+1;
         end
                
        if (tconv_cnt_reg==6) 
        begin
            adc_reading_allowed_reg <= 1;
            data_15 = adc_da;
            data_14 = adc_db;
        end    
        else if (tconv_cnt_reg==0) adc_reading_allowed_reg <=0;
    end
    
    reg [1:0] dco_pos_num = 0; //����� ��������� �������� �� 0 �� 3, �.� ���� ��� 3 ��������� 1, ����� ����� 0
    reg [1:0] dco_neg_num = 0; //����� ��������� �������� �� 0 �� 3, �.� ���� ��� 3 ��������� 1, ����� ����� 0

    
    
    
    
    reg [16:0] cnt_clk_600MHz=0;
    
    reg adc_dco_prev;
    
    always @(posedge clk_600MHz)
    begin
        /*adc_dco_prev <= adc_dco;
        if (adc_dco_prev < adc_dco) 
        begin
            dco_pos_num <= dco_pos_num+1;
            cnt_clk_600MHz <= 0;
        end
        else if (adc_dco_prev > adc_dco) 
         begin
            dco_neg_num <= dco_neg_num+1;
            cnt_clk_600MHz <= 0;
        end
        else if (cnt_clk_600MHz < 20) cnt_clk_600MHz <= cnt_clk_600MHz+1;
        else 
        begin
            dco_neg_num <= 0;
            dco_pos_num <= 0;
        end*/
          
        if (adc_dco > 0)
        begin
            if (adc_dco_prev == 0)
            begin
                adc_dco_prev <= 1;
                
                cnt_clk_600MHz <= 0;
            end
            else if (cnt_clk_600MHz < 20) cnt_clk_600MHz <= cnt_clk_600MHz+1;
            else 
            begin
                dco_neg_num <= 0;
                
            end
            
        end
        else // if (adc_dco == 0)
        begin
            if (adc_dco_prev == 1)
            begin
                adc_dco_prev <= 0;
                dco_neg_num <= dco_neg_num+1;
                cnt_clk_600MHz <= 0;
            end
            else if (cnt_clk_600MHz < 20) cnt_clk_600MHz <= cnt_clk_600MHz+1;
            else 
            begin
                dco_neg_num <= 0;
                
            end
        end
    end
    
    
    always @(posedge adc_dco)
    begin
        if (dco_neg_num == 0) dco_pos_num <= 1;
        else dco_pos_num <= dco_pos_num + 1;
        if (dco_pos_num == 0) 
        begin
            data_13 = adc_da;
            data_12 = adc_db;
        end 
        if (dco_pos_num == 1) 
        begin
            data_9 = adc_da;
            data_8 = adc_db;
        end
        if (dco_pos_num == 2) 
        begin
            data_5 = adc_da;
            data_4 = adc_db;
        end 
        if (dco_pos_num == 3) 
        begin
            data_1 = adc_da;
            data_0 = adc_db;
        end  
        
    end
    
   reg is_sending_prev = 0;
   reg [15:0] adc_data_reg= 0;
   assign adc_data_out = adc_data_reg;
  
    
    always @(negedge adc_dco)
    begin
        if (dco_neg_num == 0) 
        begin        
            data_11 = adc_da;
            data_10 = adc_db;
             
           
            tsweep_prev <= tsweep;
            
            
            //if (is_sending_prev < is_sending) 
            //begin
            //    is_start_writing_bram_allowed <= 1;
            //    is_sending_prev <= 1;
            //end
            //else if (is_sending_prev > is_sending) is_sending_prev <= 0;             
            //else if ((tsweep_prev>tsweep)&(is_start_writing_bram_allowed==1)) 
            //begin
            //    adc_bram_addr_write <= 0;
            //    is_start_writing_bram_allowed <= 0;
            //end 
            //else if (adc_bram_addr_write < ADC_BUF_DEPTH) 
            //begin
            //    adc_bram_addr_write <= adc_bram_addr_write + 1;
            //    adc_bram_we <= 1;
            //end
        end 
        else if (dco_neg_num == 1) 
        begin
            data_7 = adc_da;
            data_6 = adc_db;
        end
        else if (dco_neg_num == 2) 
        begin
            data_3 = adc_da;
            data_2 = adc_db;
            //adc_bram_we <= 0;
        end
        //��� dco_neg_num == 3 �� ������ ��� ������� ��������, ������� �� ����������         
        else if (dco_neg_num == 3) 
        begin
            //adc_bram_din <= adc_data;
            adc_data_reg <= adc_data;       // !!!!!!!!!!!!!!!!!
        end  
            
        
    end
    
    reg [15:0] adc_current_transmit_data_number=0;
    reg [7:0] adc_current_transmit_part_number=0;
    reg is_counting = 0;
    
    reg [15:0] adc_write_address_prev = 0;
    
    //always @(negedge uart_clk) adc_bram_addr_read <= adc_current_transmit_data_number;
    
    reg is_sending_prev_uart = 0;
    /*
    always @(posedge uart_clk)
    begin    
        adc_write_address_prev <= adc_bram_addr_write;   
        if ((adc_bram_addr_write==ADC_BUF_DEPTH)&(adc_write_address_prev != adc_bram_addr_write))
        begin
                adc_current_transmit_part_number <= 1; 
                adc_current_transmit_data_number <= 0;               
        end
        
        if ((adc_current_transmit_data_number < ADC_BUF_DEPTH)&(adc_bram_addr_write==ADC_BUF_DEPTH))
        begin
            if ((adc_current_transmit_part_number>0)&(is_counting==0))
            begin
            
                                                              //adc_data_reg <= adc_bram_dout[15:0];
                if (adc_current_transmit_part_number == 1) output_data_reg <= adc_bram_dout[15:8];
                else if (adc_current_transmit_part_number == 2) output_data_reg <= adc_bram_dout[7:0];
                
                //if (adc_current_transmit_part_number == 1) output_data_reg <= 0|adc_data[adc_current_transmit_data_number][17:12];
                //else if (adc_current_transmit_part_number == 2) output_data_reg <=0|adc_data[adc_current_transmit_data_number][11:6];
                //else if (adc_current_transmit_part_number == 3) output_data_reg <= 0|adc_data[adc_current_transmit_data_number][5:0]; 
                send_uart_byte_reg <= 1;
                is_counting <= 1;
                if (adc_current_transmit_part_number == 2) 
                begin
                    adc_current_transmit_data_number <= adc_current_transmit_data_number + 1;
                    if (adc_current_transmit_data_number < ADC_BUF_DEPTH-1)
                    begin                        
                        adc_current_transmit_part_number<=1;
                        
                    end
                    else
                    begin                        
                        adc_current_transmit_part_number <= 0; 
                        
                    end
                end
                else adc_current_transmit_part_number <= adc_current_transmit_part_number + 1;
            end
        end
        
        if (cnt_uart<1900)
        begin
            if (is_counting>0) cnt_uart <= cnt_uart+1;        
            if (cnt_uart == 10) send_uart_byte_reg <= 0;
        end    
        else             
        begin            
            cnt_uart <= 0;
            is_counting <= 0;            
        end
    end
    */
    /*ila_1 ila(
        .clk(test_clk),
        .probe0(fpga_clk),
        .probe1(adc_dco),        
        .probe2(adc_da),
        .probe3(adc_db),
        .probe4(adc_data),
        .probe5(dco_pos_num),
        .probe6(dco_neg_num),
        .probe7(tconv_cnt_reg),
        .probe8(adc_reading_allowed_reg)
        */
        /*.probe4(fpga_clk),
        .probe5(dco_pos_num),
        .probe6(dco_neg_num),
        .probe7(adc_data)*/
    //);
   
    
    /* assign test_signal_1 = fpga_clk;
    assign test_signal_1 = clk_adc;
    assign test_signal_1 = adc_dco;
    assign test_signal_1 = dco_pos_num>>1;
    assign test_signal_1 = dco_pos_num;
    assign test_signal_1 = dco_neg_num>>1;
    assign test_signal_1 = dco_neg_num;*/
    
endmodule
