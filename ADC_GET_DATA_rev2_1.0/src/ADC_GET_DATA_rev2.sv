`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BTLabs
// Engineer: Nechaev
// 
// Create Date: 23.07.2019 12:42:55
// Design Name: MRLS
// Module Name: mrls_top
// Project Name: 
// Target Devices: XC7A100
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module ADC_GET_DATA_rev2(
    input wire clk_100MHz,
    input wire fpga_clk,    
    output wire adc_if2_ch1_clk_p,
    output wire adc_if2_ch1_clk_n,
    input wire adc_if2_ch1_dco_p,
    input wire adc_if2_ch1_dco_n,
    input wire adc_if2_ch1_da_p,
    input wire adc_if2_ch1_da_n,
    input wire adc_if2_ch1_db_p,
    input wire adc_if2_ch1_db_n,
    output wire adc_if2_ch1_PDn,
    output wire adc_testpat,
    output wire adc_twolanes,
    
    //output wire adc_if1_ch1_PDn,
    //output wire adc_if2_ch2_PDn,
    //output wire adc_if1_ch2_PDn,
    //output wire clk_cs,
    //output wire clk_mosi,
    //input wire clk_miso,
    //output wire clk_sclk,
    //output wire clk_sync,
    //output wire clk_reset,
    
    output wire pll_trig,
    
    input wire send_adc_data,
    output wire data_ready,
    output wire [7:0] data_out,
    output wire [15:0] adc_data_out
    
    //output wire [6:0] if_ch2_vga_gc,
    //output wire if_ch2_vga_sclk,
    //output wire if_ch2_vga_miso,
    //output wire if_ch2_vga_mosi,
    //output wire if_ch2_vga_sen,
    //output wire if2_ch1_amp1_stu_en,
    //output wire if2_ch1_amp2_stu_en,
    //output wire if2_ch1_amp3_stu_en,

    //output wire [1:0] sw_if2,
    //output wire [1:0] sw_if1,
    //output wire sw1_if2_ch1,
    //output wire sw2_if2_ch1,
    //output wire [1:0] sw3_if2_ch1,
    //output wire sw_if2_ch2,
    //output wire sw_if1_ch2,
    //output wire sw1_if1_ch1,
    //output wire sw2_if1_ch1,
    //output wire [1:0] sw3_if1_ch1
    );
    
    
    //assign adc_if1_ch1_PDn = 0;   
    //assign adc_if2_ch2_PDn = 0;
    //assign adc_if1_ch2_PDn = 0;
    
    assign adc_if2_ch1_PDn = 1;
    assign adc_testpat = 0;
    assign adc_twolanes = 1;
    
    reg is_clk_initialized_reg = 0;
    reg [31:0] clk_sync_counter_reg = 0;
    
    wire adc_if2_ch1_clk,adc_if2_ch1_dco_undelayed, adc_if2_ch1_dco, adc_if2_ch1_da, adc_if2_ch1_db;
    
    wire clk_200MHz; 
   
    OBUFDS #( 
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard 
        .SLEW("SLOW") // Specify the output slew rate 
    ) OBUFDS_adc_if2_ch1_clk ( 
        .O(adc_if2_ch1_clk_p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_if2_ch1_clk_n), // Diff_n output (connect directly to top-level port) 
        .I(adc_if2_ch1_clk) // Buffer input 
    );

    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch1_dco_undelayed (
        .O(adc_if2_ch1_dco_undelayed), // Buffer output
        .I(adc_if2_ch1_dco_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_dco_n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    reg idelayctrl_rst = 0;
    wire idelayctrl_rdy;
       
    IDELAYCTRL IDELAYCTRL_inst (
    .RDY(idelayctrl_rdy), // 1-bit output: Ready output    
    .REFCLK(clk_200MHz), // 1-bit input: Reference clock input
    .RST(idelayctrl_rst) // 1-bit input: Active high reset input
    );
    
    IDELAYE2 #(
        .CINVCTRL_SEL("FALSE"), // Enable dynamic clock inversion (FALSE, TRUE)
        .DELAY_SRC("IDATAIN"), // Delay input (IDATAIN, DATAIN)
        .HIGH_PERFORMANCE_MODE("TRUE"),// Reduced jitter ("TRUE"), Reduced power ("FALSE")
        .IDELAY_TYPE ("FIXED"), // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
        .IDELAY_VALUE(31),//Input delay tap setting (0-31). Delay = 600ps + 78 ps*31=  ns
        .PIPE_SEL("FALSE"),// Select pipelined mode, FALSE, TRUE
        .REFCLK_FREQUENCY(200.0),// IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
        .SIGNAL_PATTERN ("DATA")// DATA, CLOCK input signal
    ) IDELAYE2_adc_if2_ch1_dco (
        .C(clk_200MHz),
        .DATAOUT(adc_if2_ch1_dco), // Buffer output
        .IDATAIN(adc_if2_ch1_dco_undelayed) 
    );
        
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da_if2_ch1 (
        .O(adc_if2_ch1_da), // Buffer output
        .I(adc_if2_ch1_da_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_da_n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_if2_ch1_db (
        .O(adc_if2_ch1_db), // Buffer output
        .I(adc_if2_ch1_db_p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_if2_ch1_db_n) // Diff_n buffer input (connect directly to top-level port)
    );
   
    wire clk_8MHz;    
    wire sys_clk_locked;

       
    wire uart_byte_received;
    reg uart_byte_received_prev = 0;
    wire[7:0] uart_rx_data;
    
    reg [3:0] received_byte_num = 0; 
    
    reg[7:0] uart_tx_data;
    reg uart_start_sending = 0;
    wire uart_tx_busy;
             
    reg is_counting = 0; 
    reg[3:0] cnt_uart = 0;
       
    reg pll_trig_reg = 0;
    assign pll_trig = pll_trig_reg;
     
    reg pll_write = 0; reg pll_read = 0;
    wire pll_data_ready;
    reg pll_data_ready_prev;
    wire [23:0] pll_read_data;
    reg [23:0] pll_write_data = 0;
    reg [7:0] pll_address_reg = 0;
    reg [4:0] pll_write_address_reg = 0;
    
    reg [7:0] clk_address_reg = 0;
    reg [7:0] clk_write_data_reg = 0;
    
    reg is_uart_receiving_atten_value = 0;
    
    reg is_uart_receiving_pll_read_address = 0;
    reg is_uart_receiving_pll_data = 0;    
    reg is_uart_receiving_pll_write_address = 0;
    
    reg is_uart_receiving_clk_data = 0;    
    reg is_uart_receiving_clk_write_address = 0;
    
    reg is_uart_receiving_vga_regs = 0;
    
    wire clk_uart;
    wire clk_pll_spi;
    wire clk_spi;
    wire clk_adc;
    wire clk_adc_delay;       
    
    
    clk_wiz_0 clk_wizard_main(      
      .clk_out1(clk_uart),      
      .clk_out2(clk_8MHz),
      .clk_out3(clk_pll_spi),
      .clk_out4(clk_spi),           
      .clk_out5(clk_200MHz),
      .clk_in1(clk_100MHz),
      .locked(sys_clk_locked)                 
    );
      
        
 
    
    
    
    wire [7:0] uart_tx_adc_data;
    assign data_out = uart_tx_adc_data;
    
    reg is_adc_data_sending = 1;
   
    reg adc_clk_valid=0;
    reg [16:0] trig_tsweep_counter=0; 
    reg [16:0] trig_tguard_counter=0;
    
    
    
    
    
    wire uart_adc_start_sending;
    assign data_ready = uart_adc_start_sending; 
    
    adc_get_data adc_get_data_IF2CH1(
    .tsweep(trig_tsweep_counter>0),
    .fpga_clk(fpga_clk),    
    .adc_dco(adc_if2_ch1_dco),
    .adc_da (adc_if2_ch1_da),
    .adc_db (adc_if2_ch1_db),
    .uart_clk (clk_uart),    
    .output_data(uart_tx_adc_data), 
    .adc_data_out(adc_data_out),   
    .is_sending(send_adc_data), //.is_sending(is_adc_data_sending),  
    .adc_clk(adc_if2_ch1_clk),
    .send_uart_byte(uart_adc_start_sending)    
    /*.test_signal_1(user_io_1),
    .test_signal_2(user_io_2),
    .test_signal_3(user_io_3),
    .test_signal_4(user_io_4),
    .test_signal_5(user_io_5),
    .test_signal_6(user_io_6),
    .test_signal_7(user_io_7) */   
    );    
       
    
    reg clk_write = 0;
    
    
     

    
    //����� ������� �������� idelayctrl_rdy bp 0 � 1 ������������ ������ rst �� 1/8��� = 125 ��
    reg idelayctrl_rdy_prev = 0;
    always @(posedge clk_8MHz & sys_clk_locked)
     begin
        idelayctrl_rdy_prev<= idelayctrl_rdy;
        if ((idelayctrl_rdy_prev==0)&(idelayctrl_rdy==1)) idelayctrl_rst<=1;
        else if (idelayctrl_rst==1) idelayctrl_rst<=0;
     end 
    
     
    
     
     //triggering
    
     always @(posedge clk_8MHz & sys_clk_locked)
     begin
        if ((trig_tsweep_counter<8000)&(trig_tguard_counter==0))
        begin 
            trig_tsweep_counter <= trig_tsweep_counter + 1;
            pll_trig_reg <= 0;
        end
        else if ((trig_tsweep_counter==8000)&(trig_tguard_counter==0))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter<100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 0;
            trig_tguard_counter<=trig_tguard_counter+1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter==100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=0;                        
        end
     end
         
endmodule

