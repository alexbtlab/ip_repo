
`timescale 1 ns / 1 ps

	module bit_pack_to_reg_v1_0 
	(
	   input bit_0,
	   input bit_1,
	   input bit_2,
	   input bit_3,
	   input bit_4,
	   input bit_5,
	   input bit_6,
	   input bit_7,
	   input bit_8,
	   input bit_9,
	   input bit_10,
	   input bit_11,
	   input bit_12,
	   input bit_13,
	   input bit_14,
	   input bit_15,
	   input bit_16,
	   input bit_17,
	   
	   output [17:0] OUT_DATA
	   
	);
	
	
	assign OUT_DATA = (bit_17  << 17)  |
                      (bit_16  << 16)  | 
                      (bit_15  << 15)  |
                      (bit_14  << 14)  | 
                      (bit_13  << 13)  |
                      (bit_12  << 12)  |
                      (bit_11  << 11)  | 
                      (bit_10  << 10)  |
                      (bit_9  << 9)  | 
                      (bit_8  << 8)  |
                      (bit_7  << 7)  |
                      (bit_6  << 6)  | 
                      (bit_5  << 5)  |
                      (bit_4  << 4)  | 
                      (bit_3  << 3)  |
                      (bit_2  << 2)  | 
                      (bit_1  << 1)  |
                       bit_0;

	endmodule
