
`timescale 1 ns / 1 ps

	module resizeJPEG_v1_0 #
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Ports of Axi Slave Bus Interface S00_AXIS
		input wire  s00_axis_aclk,
		input wire  m00_axis_aclk,
		input wire  s00_axis_aresetn,
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,
		input wire[47:0] M_AXIS_TUSER,

		// Ports of Axi Master Bus Interface M00_AXIS
		
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready
	);

reg [7:0] RED;
reg [7:0] GREEN;
reg [7:0] BLUE;


assign s00_axis_tready = 1'b1;

wire [15:0] X_input;
assign X_input = M_AXIS_TUSER[15:0];
wire [15:0] Y_input;
assign Y_input = M_AXIS_TUSER[31:16];
reg [31:0] m00_axis_tdata_r;
assign m00_axis_tdata = m00_axis_tdata_r;

reg [31:0] RGB_RAM[15:0];

        always @ (posedge s00_axis_aclk) begin
            if(s00_axis_aclk & s00_axis_aresetn) begin
                    if( (Y_input & 1'b1) == 0) begin
                        // Если  четная строка то кладем ее в буфер 
                        RED      <= s00_axis_tdata[23:16];
                        GREEN    <= s00_axis_tdata[15:8];
                        BLUE     <= s00_axis_tdata[7:0];
                    end 
                    else begin
                        m00_axis_tdata_r <= (        (s00_axis_tdata[23:16] + RED)          |
                                                    ((s00_axis_tdata[15:8]  + GREEN)  << 8) |
                                                    ((s00_axis_tdata[7:0]   + BLUE)   << 16)
                                            );
                        RGB_RAM[X_input] <= (        (s00_axis_tdata[23:16] + RED)          |
                                                    ((s00_axis_tdata[15:8]  + GREEN)  << 8) |
                                                    ((s00_axis_tdata[7:0]   + BLUE)   << 16)
                                            );
                            //Если  не четная строка то складываем с предыдущей 
                    end         
            end
        end
endmodule
