
`timescale 1 ns / 1 ps

	module ADC_AD9650_v1_0 
	
 (
             
        output wire SPI_SCLK,
        output wire SPI_SDO,
        output wire SPI_CS,
        
        output wire ADC_PDwN,  // Power-Down Input in External Pin Mode.
        output wire SYNC,      // Digital Synchronization Pin
        
        input wire DCOA,       // Channel A Data Clock Output.
        input wire DCOB,       // Channel B Data Clock Output. 
        
        input wire ORA,        // Channel A Overrange Output 
        input wire ORB,        // Channel B Overrange Output 
        
        input wire [15:0] DATA_INA,
        input wire [15:0] DATA_INB
    );
    
       
   
 
   
    

endmodule
