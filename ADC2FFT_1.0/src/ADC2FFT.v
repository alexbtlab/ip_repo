
`timescale 1 ns / 1 ps

	module ADC2FFT # 
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  [15:0] data_1,
		input wire  [15:0] data_2,
		
		input wire  m00_axis_aclk,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		input wire  [15 : 0] FS,
		output wire [15 : 0] WC
	);
wire [15:0] cnt_clk_w = 0;		
reg [15:0] cnt_clk;	
assign cnt_clk_w = cnt_clk;
reg flag_work = 1;	
assign   m00_axis_tvalid = (cnt_clk > 0 & cnt_clk <= FS & flag_work) ? 1 : 0;	
assign   m00_axis_tlast  = (cnt_clk == FS & flag_work) ? 1 : 0;	
	
reg [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata_r;
assign m00_axis_tdata = m00_axis_tdata_r;

wire [31:0] data_to_tx; 
assign   data_to_tx = {data_2, data_1};


always @(posedge m00_axis_aclk) begin
    
    
    if(m00_axis_tready & m00_axis_aresetn & flag_work) begin
            if(cnt_clk != FS) begin
                m00_axis_tdata_r <= data_to_tx;
                cnt_clk <= cnt_clk + 1;
            end        
            else begin
                flag_work <= 0;
            end    
    end
    else begin
        if(!m00_axis_aresetn) begin
            flag_work <= 1;
            cnt_clk <= 0;
        end 
        else begin
            flag_work <= 0;      
        end    
    end
end
    
    
    
endmodule
