
`timescale 1 ns / 1 ps

	module syncAzimut_v1_0 
	(
		 input wire  syscl3k_100,

		input wire  sysclk_100,
		input wire  reset,
		input wire  async_azimut,
		output wire  sync_azimut
	);

    reg [7:0] cnt_recieved_azimut;
    reg sync_azimut_r;
    assign sync_azimut = sync_azimut_r;
    
    always @ ( posedge sysclk_100) begin
        if ( reset ) begin
            if ( async_azimut ) begin
                cnt_recieved_azimut <= cnt_recieved_azimut + 1;
                if ( cnt_recieved_azimut == 10 )
                    sync_azimut_r <= 1;
            end
            else begin
                sync_azimut_r <= 0;
                cnt_recieved_azimut <= 0;
            end
        end
        else begin
            cnt_recieved_azimut <= 0;
        end
    end


	endmodule
