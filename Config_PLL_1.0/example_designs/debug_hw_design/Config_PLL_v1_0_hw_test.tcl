# Runtime Tcl commands to interact with - Config_PLL_v1_0

# Sourcing design address info tcl
set bd_path [get_property DIRECTORY [current_project]]/[current_project].srcs/[current_fileset]/bd
source ${bd_path}/Config_PLL_v1_0_include.tcl

# jtag axi master interface hardware name, change as per your design.
set jtag_axi_master hw_axi_1
set ec 0

# hw test script
# Delete all previous axis transactions
if { [llength [get_hw_axi_txns -quiet]] } {
	delete_hw_axi_txn [get_hw_axi_txns -quiet]
}


# Test all lite slaves.
set wdata_1 abcd1234

# Test: HV_Slave
# Create a write transaction at hv_slave_addr address
create_hw_axi_txn w_hv_slave_addr [get_hw_axis $jtag_axi_master] -type write -address $hv_slave_addr -data $wdata_1
# Create a read transaction at hv_slave_addr address
create_hw_axi_txn r_hv_slave_addr [get_hw_axis $jtag_axi_master] -type read -address $hv_slave_addr
# Initiate transactions
run_hw_axi r_hv_slave_addr
run_hw_axi w_hv_slave_addr
run_hw_axi r_hv_slave_addr
set rdata_tmp [get_property DATA [get_hw_axi_txn r_hv_slave_addr]]
# Compare read data
if { $rdata_tmp == $wdata_1 } {
	puts "Data comparison test pass for - HV_Slave"
} else {
	puts "Data comparison test fail for - HV_Slave, expected-$wdata_1 actual-$rdata_tmp"
	inc ec
}

# Test: Data_Save
# Create a write transaction at data_save_addr address
create_hw_axi_txn w_data_save_addr [get_hw_axis $jtag_axi_master] -type write -address $data_save_addr -data $wdata_1
# Create a read transaction at data_save_addr address
create_hw_axi_txn r_data_save_addr [get_hw_axis $jtag_axi_master] -type read -address $data_save_addr
# Initiate transactions
run_hw_axi r_data_save_addr
run_hw_axi w_data_save_addr
run_hw_axi r_data_save_addr
set rdata_tmp [get_property DATA [get_hw_axi_txn r_data_save_addr]]
# Compare read data
if { $rdata_tmp == $wdata_1 } {
	puts "Data comparison test pass for - Data_Save"
} else {
	puts "Data comparison test fail for - Data_Save, expected-$wdata_1 actual-$rdata_tmp"
	inc ec
}

# Check error flag
if { $ec == 0 } {
	 puts "PTGEN_TEST: PASSED!" 
} else {
	 puts "PTGEN_TEST: FAILED!" 
}

