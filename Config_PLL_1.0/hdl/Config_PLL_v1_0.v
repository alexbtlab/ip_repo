
`timescale 1 ns / 1 ps

	module Config_PLL_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface HV_Slave
		parameter integer C_HV_Slave_DATA_WIDTH	= 32,
		parameter integer C_HV_Slave_ADDR_WIDTH	= 4,

		// Parameters of Axi Slave Bus Interface Data_Save
		parameter integer C_Data_Save_DATA_WIDTH	= 32,
		parameter integer C_Data_Save_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
		output wire LED_Test,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface HV_Slave
		input wire  hv_slave_aclk,
		input wire  hv_slave_aresetn,
		input wire [C_HV_Slave_ADDR_WIDTH-1 : 0] hv_slave_awaddr,
		input wire [2 : 0] hv_slave_awprot,
		input wire  hv_slave_awvalid,
		output wire  hv_slave_awready,
		input wire [C_HV_Slave_DATA_WIDTH-1 : 0] hv_slave_wdata,
		input wire [(C_HV_Slave_DATA_WIDTH/8)-1 : 0] hv_slave_wstrb,
		input wire  hv_slave_wvalid,
		output wire  hv_slave_wready,
		output wire [1 : 0] hv_slave_bresp,
		output wire  hv_slave_bvalid,
		input wire  hv_slave_bready,
		input wire [C_HV_Slave_ADDR_WIDTH-1 : 0] hv_slave_araddr,
		input wire [2 : 0] hv_slave_arprot,
		input wire  hv_slave_arvalid,
		output wire  hv_slave_arready,
		output wire [C_HV_Slave_DATA_WIDTH-1 : 0] hv_slave_rdata,
		output wire [1 : 0] hv_slave_rresp,
		output wire  hv_slave_rvalid,
		input wire  hv_slave_rready,

		// Ports of Axi Slave Bus Interface Data_Save
		input wire  data_save_aclk,
		input wire  data_save_aresetn,
		input wire [C_Data_Save_ADDR_WIDTH-1 : 0] data_save_awaddr,
		input wire [2 : 0] data_save_awprot,
		input wire  data_save_awvalid,
		output wire  data_save_awready,
		input wire [C_Data_Save_DATA_WIDTH-1 : 0] data_save_wdata,
		input wire [(C_Data_Save_DATA_WIDTH/8)-1 : 0] data_save_wstrb,
		input wire  data_save_wvalid,
		output wire  data_save_wready,
		output wire [1 : 0] data_save_bresp,
		output wire  data_save_bvalid,
		input wire  data_save_bready,
		input wire [C_Data_Save_ADDR_WIDTH-1 : 0] data_save_araddr,
		input wire [2 : 0] data_save_arprot,
		input wire  data_save_arvalid,
		output wire  data_save_arready,
		output wire [C_Data_Save_DATA_WIDTH-1 : 0] data_save_rdata,
		output wire [1 : 0] data_save_rresp,
		output wire  data_save_rvalid,
		input wire  data_save_rready
	);
// Instantiation of Axi Bus Interface HV_Slave
	Config_PLL_v1_0_HV_Slave # ( 
		.C_S_AXI_DATA_WIDTH(C_HV_Slave_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_HV_Slave_ADDR_WIDTH)
	) Config_PLL_v1_0_HV_Slave_inst (
		.S_AXI_ACLK(hv_slave_aclk),
		.S_AXI_ARESETN(hv_slave_aresetn),
		.S_AXI_AWADDR(hv_slave_awaddr),
		.S_AXI_AWPROT(hv_slave_awprot),
		.S_AXI_AWVALID(hv_slave_awvalid),
		.S_AXI_AWREADY(hv_slave_awready),
		.S_AXI_WDATA(hv_slave_wdata),
		.S_AXI_WSTRB(hv_slave_wstrb),
		.S_AXI_WVALID(hv_slave_wvalid),
		.S_AXI_WREADY(hv_slave_wready),
		.S_AXI_BRESP(hv_slave_bresp),
		.S_AXI_BVALID(hv_slave_bvalid),
		.S_AXI_BREADY(hv_slave_bready),
		.S_AXI_ARADDR(hv_slave_araddr),
		.S_AXI_ARPROT(hv_slave_arprot),
		.S_AXI_ARVALID(hv_slave_arvalid),
		.S_AXI_ARREADY(hv_slave_arready),
		.S_AXI_RDATA(hv_slave_rdata),
		.S_AXI_RRESP(hv_slave_rresp),
		.S_AXI_RVALID(hv_slave_rvalid),
		.S_AXI_RREADY(hv_slave_rready),
		//.slv_reg0(Guest_ID_32),
		.slv_reg0(ID_adr_0_i),
		.slv_reg1(ID_adr_1_i),
		.slv_reg2(ID_adr_2_i),
		.slv_reg3(ID_adr_3_i)
	);

// Instantiation of Axi Bus Interface Data_Save
	Config_PLL_v1_0_Data_Save # ( 
		.C_S_AXI_DATA_WIDTH(C_Data_Save_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_Data_Save_ADDR_WIDTH)
	) Config_PLL_v1_0_Data_Save_inst (
		.S_AXI_ACLK(data_save_aclk),
		.S_AXI_ARESETN(data_save_aresetn),
		.S_AXI_AWADDR(data_save_awaddr),
		.S_AXI_AWPROT(data_save_awprot),
		.S_AXI_AWVALID(data_save_awvalid),
		.S_AXI_AWREADY(data_save_awready),
		.S_AXI_WDATA(data_save_wdata),
		.S_AXI_WSTRB(data_save_wstrb),
		.S_AXI_WVALID(data_save_wvalid),
		.S_AXI_WREADY(data_save_wready),
		.S_AXI_BRESP(data_save_bresp),
		.S_AXI_BVALID(data_save_bvalid),
		.S_AXI_BREADY(data_save_bready),
		.S_AXI_ARADDR(data_save_araddr),
		.S_AXI_ARPROT(data_save_arprot),
		.S_AXI_ARVALID(data_save_arvalid),
		.S_AXI_ARREADY(data_save_arready),
		.S_AXI_RDATA(data_save_rdata),
		.S_AXI_RRESP(data_save_rresp),
		.S_AXI_RVALID(data_save_rvalid),
		.S_AXI_RREADY(data_save_rready),
		.slv_reg0(Data1),
		.slv_reg1(Data2)
	);

	// Add user logic here
	wire clk1;
	wire rst1;
	wire [1:0]Guest_ID_in;

	wire [C_HV_Slave_DATA_WIDTH-1:0] ID_adr_0_i;
	wire [C_HV_Slave_DATA_WIDTH-1:0] ID_adr_1_i;
	wire [C_HV_Slave_DATA_WIDTH-1:0] ID_adr_2_i;
	wire [C_HV_Slave_DATA_WIDTH-1:0] ID_adr_3_i;
	
	wire [C_HV_Slave_DATA_WIDTH-1:0] Guest_ID_32;
	
	wire [C_Data_Save_DATA_WIDTH-1:0] Data1;
    wire [C_Data_Save_DATA_WIDTH-1:0] Data2;

	wire [63:0] Data;

	assign Guest_ID_in = Guest_ID_32[1:0];
	assign Data = {Data2, Data1};

	assign clk1 = data_save_aclk;
	assign rst1 = data_save_aresetn;

	Prototipo_IPC Prototipo_IPC(

		.clk(clk1),
		.rst(rst1),
		
		.ID_adr_0_i(ID_adr_0_i),
		.ID_adr_1_i(ID_adr_1_i),
		.ID_adr_2_i(ID_adr_2_i),
		.ID_adr_3_i(ID_adr_3_i),
		
		.Data_i(Data),
		.LED_Test1(LED_Test)
	);

	// User logic ends

	endmodule
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
module Prototipo_IPC(
	input clk,
	input rst,

	input [31:0] ID_adr_0_i,
	input [31:0] ID_adr_1_i,
	input [31:0] ID_adr_2_i,
	input [31:0] ID_adr_3_i,
	
	input wire [31:0] Data_i,

	output LED_Test1

);

reg [31:0] ID_adr_0_r32;
reg [31:0] ID_adr_1_r32;
reg [31:0] ID_adr_2_r32;
reg [31:0] ID_adr_3_r32;
reg [31:0] Data;
reg  LED_Test1_wr;
wire  [31 : 0] Datatmp;
wire FlagLed;


assign LED_Test1 = LED_Test1_wr;
assign Datatmp = Data_i;


assign FlagLed = (Datatmp == 32'hacdcEEEE) ? 1 : 0;
    
    
always @(clk) begin
	LED_Test1_wr <= FlagLed;
end


/*
always @(Guest_ID_i) begin
	case(Guest_ID_i)
	0: begin
		LED_Test1 = ID_adr_1_r32[0];
		LED_Test2 = ID_adr_1_r32[1];
	end
	1: begin
		LED_Test1 = ID_adr_2_r32[0];
		LED_Test2 = ID_adr_2_r32[1];
	end
	2: begin
		LED_Test1 = ID_adr_3_r32[0];
		LED_Test2 = ID_adr_3_r32[1];
	end
	3: begin
		LED_Test1 = ID_adr_4_r32[0];
		LED_Test2 = ID_adr_4_r32[1];
	end
	endcase
end*/

endmodule