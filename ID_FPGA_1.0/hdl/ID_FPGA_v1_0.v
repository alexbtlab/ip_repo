
`timescale 1 ns / 1 ps

	module ID_FPGA_v1_0
	(
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		output wire ready,
		output  wire [63:0] outID
	);

    wire DOUT;
//    wire CLK;
    wire DIN; 
    wire READ;
    wire SHIFT;
     
    reg read_r;
    reg [63:0] out_r;
    reg ready_r;
    reg shift_r = 0;
    
    assign ready = ready_r;
    assign DIN = 64'h12345678;
    assign READ = read_r;
    assign SHIFT = shift_r;
    assign outID = out_r;
    
   DNA_PORT #(
      .SIM_DNA_VALUE(57'h123456789012345)  // Specifies a sample 57-bit DNA value for simulation
   )
   DNA_PORT_inst (
      .DOUT(DOUT),   // 1-bit output: DNA output data.
      .CLK(s00_axi_aclk),     // 1-bit input: Clock input.
      .DIN(DIN),     // 1-bit input: User data input pin.
      .READ(READ),   // 1-bit input: Active high load DNA, active low read input.
      .SHIFT(SHIFT)  // 1-bit input: Active high shift enable input.
   );
   
   reg [31:0] cnt_clk = 0;
   
//    always @ (posedge s00_axi_aclk) begin
//        if ( s00_axi_aresetn ) begin
        
//        shift_r <= ~shift_r;
        
//            if ( cnt_clk == 255 )    cnt_clk <= 0;
//            else                     cnt_clk <= cnt_clk + 1;
         
//            if ( cnt_clk > 10 && cnt_clk > 74)      read_r <= 1;
//            else                     read_r <= 0;
            
//            if ( cnt_clk > 73 && cnt_clk < 134 )
                out_r =  DOUT;
                
//            if ( cnt_clk == 74 )  ready_r <= 1;
//            else                  ready_r <= 0;
//        end   
//    end

 



reg [1:0] stateDNA_reg, stateDNA_next_reg; 
localparam [1:0] // 3 states are required for Moore
    s_zeroDNA  = 2'b00,
    s_loadDNA  = 2'b01,
    s_readDNA  = 2'b10, 
    s_readyDNA = 2'b11;
    
    

always @(posedge s00_axi_aclk, posedge s00_axi_aresetn)
begin
    if(s00_axi_aresetn) // go to state zero if rese
        begin
            if ( cnt_clk == 255 )    cnt_clk <= 0;
            else                     cnt_clk <= cnt_clk + 1;
            stateDNA_reg <= stateDNA_next_reg;
        end
    else // otherwise update the states
        begin
            stateDNA_reg <= s_zeroDNA;
        end
end 

always @(stateDNA_reg)
begin
   
  stateDNA_next_reg = stateDNA_reg;  
   
    case(stateDNA_reg)
        s_zeroDNA:       stateDNA_next_reg <= s_loadDNA;
                 
          
            
        s_loadDNA: /**********************************/
                    begin
                        if ( cnt_clk == 64 )    stateDNA_next_reg <= s_readyDNA;
                        else                    read_r <= 1;
                        
                    end
        s_readyDNA:   /**********************************/
                    begin
                        read_r <= 0;
                        stateDNA_next_reg <= s_zeroDNA;
                    end
    endcase
end



endmodule
