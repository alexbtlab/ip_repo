
`timescale 1 ns / 1 ps

	module averageFFT_v1_0 # 
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 64,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,
		
		parameter integer C_S_AXI_ADDR_WIDTH	= 5,
		parameter integer C_S_AXI_DATA_WIDTH	= 32
	)
	(
	    input wire  S_AXI_ACLK,
		// Global Reset Signal. This Signal is Active LOW
		input wire  S_AXI_ARESETN,
		// Write address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR,
		// Write channel Protection type. This signal indicates the
    		// privilege and security level of the transaction, and whether
    		// the transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_AWPROT,
		// Write address valid. This signal indicates that the master signaling
    		// valid write address and control information.
		input wire  S_AXI_AWVALID,
		// Write address ready. This signal indicates that the slave is ready
    		// to accept an address and associated control signals.
		output wire  S_AXI_AWREADY,
		// Write data (issued by master, acceped by Slave) 
		input wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA,
		// Write strobes. This signal indicates which byte lanes hold
    		// valid data. There is one write strobe bit for each eight
    		// bits of the write data bus.    
		input wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB,
		// Write valid. This signal indicates that valid write
    		// data and strobes are available.
		input wire  S_AXI_WVALID,
		// Write ready. This signal indicates that the slave
    		// can accept the write data.
		output wire  S_AXI_WREADY,
		// Write response. This signal indicates the status
    		// of the write transaction.
		output wire [1 : 0] S_AXI_BRESP,
		// Write response valid. This signal indicates that the channel
    		// is signaling a valid write response.
		output wire  S_AXI_BVALID,
		// Response ready. This signal indicates that the master
    		// can accept a write response.
		input wire  S_AXI_BREADY,
		// Read address (issued by master, acceped by Slave)
		input wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR,
		// Protection type. This signal indicates the privilege
    		// and security level of the transaction, and whether the
    		// transaction is a data access or an instruction access.
		input wire [2 : 0] S_AXI_ARPROT,
		// Read address valid. This signal indicates that the channel
    		// is signaling valid read address and control information.
		input wire  S_AXI_ARVALID,
		// Read address ready. This signal indicates that the slave is
    		// ready to accept an address and associated control signals.
		output wire  S_AXI_ARREADY,
		// Read data (issued by slave)
		output wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA,
		// Read response. This signal indicates the status of the
    		// read transfer.
		output wire [1 : 0] S_AXI_RRESP,
		// Read valid. This signal indicates that the channel is
    		// signaling the required read data.
		output wire  S_AXI_RVALID,
		// Read ready. This signal indicates that the master can
    		// accept the read data and response information.
		input wire  S_AXI_RREADY,
		 //////////////////////////////////////////////
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		input wire  allowed_clk,
        input wire  azimut_0,		
		input  wire  m00_axis_aclk,
		input  wire  clk_10MHz,
		input  wire  m00_axis_aresetn,
		output wire interrupt_frame,
		output wire [31:0] azimut8,
		input wire enable_intr
			
	);
	
	     
		
		 reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;
    
    wire [31:0] interrupt_on_the_frame;
    assign interrupt_on_the_frame = slv_reg0;
    
    averageFFT_v1_0_S00_AXI averageFFT_v1_0_S00_AXI (.*);
	
localparam averag_val = 8;     // ?????????? ??????????? ????????
localparam divide_val = 3;     // ?? ??????? ??? ???????? ?????? ?? ?????? ??? ??????? 1-????? ?? 2, 2-????? ?? 4,3-8 ? ??
localparam frame_size = 8192;     // ?????????? ?????? ?? ??????
localparam VAL_SET = 55000; 

wire [29:0] data_abs_1;     
wire [29:0] data_abs_2;     

reg interrupt_frame_r;
reg [3:0] azimut_r;
reg [3:0] fft_azimut_r;
reg [3:0] fft_azimut8_r;
reg [15:0] azimut8_r;
reg [63:0] RAM [0:8191];
reg [15:0] adr;
reg [31:0] m00_axis_tdata_r;
reg [31:0] cnt100;
reg [15:0] adr_read;
reg [15:0] cnt_low_allowed_clk;
reg reset_cnt_trig = 0;
reg [15:0] cnt_high_allowed_clk;
reg azimut_0_prev = 0;

assign interrupt_frame = ( interrupt_frame_r & enable_intr );
assign m00_axis_tdata = m00_axis_tdata_r;
assign m00_axis_tstrb = 4'hF;
assign m00_axis_tlast  = ( fft_azimut_r == averag_val  ) ? s00_axis_tlast  : 0;
assign m00_axis_tvalid = ( fft_azimut_r == averag_val  ) ? s00_axis_tvalid : 0;
assign s00_axis_tready = m00_axis_tready;
assign data_abs_1[29:0] = (s00_axis_tdata[29] == 1) ? -s00_axis_tdata[29:0]  : s00_axis_tdata[29:0];
assign data_abs_2[29:0] = (s00_axis_tdata[61] == 1) ? -s00_axis_tdata[61:32] : s00_axis_tdata[61:32];
assign azimut8 = fft_azimut8_r;	

always @ (posedge m00_axis_aclk) begin 
		   
     if( ( (cnt100 >= VAL_SET)       & (cnt100 <= VAL_SET + 100) ) & (fft_azimut_r == interrupt_on_the_frame  ))     interrupt_frame_r <= 1;
     if( ( (cnt100 >= VAL_SET + 101) & (cnt100 <= VAL_SET + 200) ) & (fft_azimut_r == interrupt_on_the_frame  ))     interrupt_frame_r <= 0;
     
     if ( !m00_axis_aresetn )  interrupt_frame_r <= 0;
     
     if(allowed_clk)                                     cnt100 <= cnt100 + 1;
     else                                                cnt100 <= 0;
	
     if(m00_axis_aresetn)  begin
   
                         if( s00_axis_tvalid) begin
                         
                                adr_read <= 0;
                         
                                   if(adr == frame_size - 1)
                                       adr <= 0;
                                   else
                                       adr <= adr + 1;     // ????? ?????? ?????? ????????? ??? ?????? ???????????? ?????????? ?????? 
                                   if(fft_azimut_r == 0) begin    // ???? ??????? ????? ?? ????????? ?????? ? ?????? 
                                       RAM[adr] <= data_abs_1 + data_abs_2;
                                       m00_axis_tdata_r <= 0;
                                   end
                                   else begin      
                                       if( fft_azimut_r == averag_val )
                                            if (adr == 1000) 
                                                m00_axis_tdata_r <= fft_azimut8_r + 15000000;   // ???? ?????????????? averag_val ???????? ?? ?????????? ?????? ?? ?????? ? ????????(???????) ?? 2,4,8 ? ??
                                            else
                                                m00_axis_tdata_r <= (RAM[adr] >> divide_val);   
                                       else begin
                                           RAM[adr] <= RAM[adr] + data_abs_1 + data_abs_2; // ???? ?? ??????? ????? ?? ????????? ???????? ?????? ? ?????????? (???????????? ?? ?????????? ??????) ???????
                                           m00_axis_tdata_r <= 0;
                                       end
                                   end
                        end
                        else begin  
                            
                            adr <= 0;
                                                   
                        end
                                                       
                            
	
	end
	else begin
	   adr_read <= 0;
	   adr <= 0;
	   m00_axis_tdata_r = 0;
	end
	
end


always @ (posedge clk_10MHz) begin
    
    if ( m00_axis_aresetn ) begin
                
                if ( !allowed_clk )                 cnt_low_allowed_clk <= cnt_low_allowed_clk + 1;
                else                                cnt_low_allowed_clk <= 0;
                
                if ( allowed_clk )                  cnt_high_allowed_clk <= cnt_high_allowed_clk + 1;
                else                                cnt_high_allowed_clk <= 0;
                
                
                if( cnt_low_allowed_clk == 10 )     begin
                    azimut_r <= azimut_r + 1;
                    if ( azimut_r == 8 ) begin
                        azimut8_r <= azimut8_r + 1;
                        azimut_r <= 0;
                    end
                end 

                if( cnt_high_allowed_clk == 1000 )     begin
                    fft_azimut_r <= fft_azimut_r + 1;
                    if ( fft_azimut_r == 8 ) begin
                        fft_azimut8_r <= fft_azimut8_r + 1;
                        fft_azimut_r <= 0;
                    end
                end
                    
    end
    else begin
        cnt_low_allowed_clk <= 0;
        cnt_high_allowed_clk <= 0;
        azimut_r <= 0;
        fft_azimut_r <= 0;
        fft_azimut8_r <= 0;
        azimut8_r <= 0;
    end
    
    if (azimut_0 & !azimut_0_prev) begin
        cnt_low_allowed_clk <= 0;
        cnt_high_allowed_clk <= 0;
        azimut_r <= 0;
        fft_azimut_r <= 0;
        fft_azimut8_r <= 0;
        azimut8_r <= 0;
        reset_cnt_trig <= 1;
        azimut_0_prev <= 1;
    end
    else begin
        reset_cnt_trig <= 0;
        
    end  

    if(!azimut_0)
        azimut_0_prev <= 0;
end
   
endmodule

