
`timescale 1 ns / 1 ps

	module averageFFT_v1_0 # 
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 64,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,
		
		parameter integer C_S_AXI_ADDR_WIDTH	= 5,
		parameter integer C_S_AXI_DATA_WIDTH	= 32
	)
	(
		output wire  s00_axis_tready,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
//		input wire  [31:0] VAL_SET,
		input wire  allowed_clk,
        input wire  azimut_0,		
		
		input  wire  m00_axis_aclk,
		input  wire  clk_10MHz,
		input  wire  m00_axis_aresetn,
		
//		output wire [15:0] azimut_ila,
//		output wire [15:0] low_azimut_ila,
		output wire interrupt_frame,
		
//		output wire reset_cnt_trig_ila,
//		output wire allowed_clk_prev_ila,
		
		output wire [31:0] azimut,
		output wire [31:0] azimut8,
		
		input wire enable_intr,
		input wire [31:0] intr_frame
		
//		input wire [31:0] max_num_azimut
		
		
	);
	
	     wire  S_AXI_ACLK;
		 wire  S_AXI_ARESETN;
		 wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_AWADDR;
		 wire [2 : 0] S_AXI_AWPROT;
		 wire  S_AXI_AWVALID;
		 wire  S_AXI_AWREADY;
		 wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_WDATA;
		 wire [(C_S_AXI_DATA_WIDTH/8)-1 : 0] S_AXI_WSTRB;
		 wire  S_AXI_WVALID;
		 wire  S_AXI_WREADY;
		 wire [1 : 0] S_AXI_BRESP;
		 wire  S_AXI_BVALID;
		 wire  S_AXI_BREADY;
		 wire [C_S_AXI_ADDR_WIDTH-1 : 0] S_AXI_ARADDR;
		 wire [2 : 0] S_AXI_ARPROT;
		 wire  S_AXI_ARVALID;
		 wire  S_AXI_ARREADY;
		 wire [C_S_AXI_DATA_WIDTH-1 : 0] S_AXI_RDATA;
		 wire [1 : 0] S_AXI_RRESP;
		 wire  S_AXI_RVALID;
		 wire  S_AXI_RREADY;
		
		 reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg0;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg1;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg2;
         reg [C_S_AXI_DATA_WIDTH-1:0]	slv_reg3;

         wire [C_S_AXI_DATA_WIDTH-1:0]	ip2mb_reg0;
         wire [C_S_AXI_DATA_WIDTH-1:0]	ip2mb_reg1;
         wire [C_S_AXI_DATA_WIDTH-1:0]	ip2mb_reg2;
         wire [C_S_AXI_DATA_WIDTH-1:0]	ip2mb_reg;
        
        averageFFT_AXIS averageFFT_AXIS
		(
		.*
//		.S_AXI_ACLK(S_AXI_ACLK),
//		.S_AXI_ARESETN(S_AXI_ARESETN),
//		.S_AXI_AWADDR(S_AXI_AWADDR),
//		.S_AXI_AWPROT(),
//		.S_AXI_AWVALID(),
//		.S_AXI_AWREADY(),
//		.S_AXI_WDATA(),
//		.S_AXI_WSTRB(),
//		.S_AXI_WVALID(),
//		.S_AXI_WREADY(),
//		.S_AXI_BRESP(),
//		.S_AXI_BVALID(),
//		.S_AXI_BREADY(),
//		.S_AXI_ARADDR(),
//		.S_AXI_ARPROT(),
//		.S_AXI_ARVALID(),
//		.S_AXI_ARREADY(),
//		.S_AXI_RDATA(),
//		.S_AXI_RRESP(),
//		.S_AXI_RVALID(),
//		.S_AXI_RREADY(),
		
//		.slv_reg0(),
//        .slv_reg1(),
//        .slv_reg2(),
//        .slv_reg3(),

//        .ip2mb_reg0(),
//        .ip2mb_reg1(),
//        .ip2mb_reg2(),
//        .ip2mb_reg3()

	);
	
localparam averag_val = 8;     // ?????????? ??????????? ????????
localparam divide_val = 3;     // ?? ??????? ??? ???????? ?????? ?? ?????? ??? ??????? 1-????? ?? 2, 2-????? ?? 4,3-8 ? ??
localparam frame_size = 8192;     // ?????????? ?????? ?? ??????
localparam VAL_SET = 55000; 

wire [29:0] data_abs_1;     
wire [29:0] data_abs_2;     

reg interrupt_frame_r;
//reg [15:0] frame_azimut;
//reg [16:0]  data_sum;
//reg [15:0] frame_1ms = 0;
reg [3:0] azimut_r;
reg [3:0] fft_azimut_r;
reg [3:0] fft_azimut8_r;
reg [15:0] azimut8_r;
reg [63:0] RAM [0:8191];
reg [15:0] adr;
reg [31:0] m00_axis_tdata_r;
reg [31:0] cnt100;
//reg m00_axis_tlast_r;
//reg m00_axis_tvalid_r;
reg [15:0] adr_read;
reg [15:0] cnt_low_allowed_clk;
//reg reset_frame;
reg reset_cnt_trig = 0;
//reg [15:0] adr_8fr;
reg [15:0] cnt_high_allowed_clk;
reg azimut_0_prev = 0;

assign interrupt_frame = ( interrupt_frame_r & enable_intr );
//assign azimut_ila = frame_azimut;
assign m00_axis_tdata = m00_axis_tdata_r;
assign m00_axis_tstrb = 4'hF;
assign m00_axis_tlast  = ( fft_azimut_r == averag_val  ) ? s00_axis_tlast  : 0;
assign m00_axis_tvalid = ( fft_azimut_r == averag_val  ) ? s00_axis_tvalid : 0;
assign s00_axis_tready = m00_axis_tready;
assign data_abs_1[29:0] = (s00_axis_tdata[29] == 1) ? -s00_axis_tdata[29:0]  : s00_axis_tdata[29:0];
assign data_abs_2[29:0] = (s00_axis_tdata[61] == 1) ? -s00_axis_tdata[61:32] : s00_axis_tdata[61:32];
//assign reset_cnt_trig_ila  = reset_cnt_trig;
assign azimut = { fft_azimut_r};
assign azimut8 = fft_azimut8_r;	

always @ (posedge m00_axis_aclk) begin 
	
//	 if ( reset_cnt_trig )     reset_frame <= 1;
//	 else                      reset_frame <= 0;
	   
     if( ( (cnt100 >= VAL_SET)       & (cnt100 <= VAL_SET + 100) ) & (fft_azimut_r == intr_frame  ))     interrupt_frame_r <= 1;
     if( ( (cnt100 >= VAL_SET + 101) & (cnt100 <= VAL_SET + 200) ) & (fft_azimut_r == intr_frame  ))     interrupt_frame_r <= 0;
     
     if ( !m00_axis_aresetn )  interrupt_frame_r <= 0;
     
     if(allowed_clk)                                     cnt100 <= cnt100 + 1;
     else                                                cnt100 <= 0;
	


    if(m00_axis_aresetn)  begin
    
   
                         if( s00_axis_tvalid) begin
                         
                                adr_read <= 0;
                         
                                   if(adr == frame_size - 1)
                                       adr <= 0;
                                   else
                                       adr <= adr + 1;     // ????? ?????? ?????? ????????? ??? ?????? ???????????? ?????????? ?????? 
                                   if(fft_azimut_r == 0) begin    // ???? ??????? ????? ?? ????????? ?????? ? ?????? 
                                       RAM[adr] <= data_abs_1 + data_abs_2;
                                       m00_axis_tdata_r <= 0;
                                   end
                                   else begin      
                                       if( fft_azimut_r == averag_val )
                                            if (adr == 1000) 
                                                m00_axis_tdata_r <= fft_azimut8_r + 15000000;   // ???? ?????????????? averag_val ???????? ?? ?????????? ?????? ?? ?????? ? ????????(???????) ?? 2,4,8 ? ??
                                            else
                                                m00_axis_tdata_r <= (RAM[adr] >> divide_val);   
                                       else begin
                                           RAM[adr] <= RAM[adr] + data_abs_1 + data_abs_2; // ???? ?? ??????? ????? ?? ????????? ???????? ?????? ? ?????????? (???????????? ?? ?????????? ??????) ???????
                                           m00_axis_tdata_r <= 0;
                                       end
                                   end
                        end
                        else begin  
                            
                            adr <= 0;
                                                   
                        end
                                                       
                            
	
	end
	else begin
//	   adr_8fr <= 0;
	   adr_read <= 0;
	   adr <= 0;
	   m00_axis_tdata_r = 0;
	end
	
end







always @ (posedge clk_10MHz) begin
    
    if ( m00_axis_aresetn ) begin
                
                if ( !allowed_clk )                 cnt_low_allowed_clk <= cnt_low_allowed_clk + 1;
                else                                cnt_low_allowed_clk <= 0;
                
                if ( allowed_clk )                  cnt_high_allowed_clk <= cnt_high_allowed_clk + 1;
                else                                cnt_high_allowed_clk <= 0;
                
                
                
               /////////////////////////////////////////////////////////////////////// 
                if( cnt_low_allowed_clk == 10 )     begin
                    azimut_r <= azimut_r + 1;
                    if ( azimut_r == 8 ) begin
                        azimut8_r <= azimut8_r + 1;
                        azimut_r <= 0;
                    end
                end 
                ///////////////////////////////////////////////////////////////////////
                if( cnt_high_allowed_clk == 1000 )     begin
                    fft_azimut_r <= fft_azimut_r + 1;
                    if ( fft_azimut_r == 8 ) begin
                        fft_azimut8_r <= fft_azimut8_r + 1;
                        fft_azimut_r <= 0;
                    end
                end
                
                
    end
    else begin
        cnt_low_allowed_clk <= 0;
        cnt_high_allowed_clk <= 0;
        azimut_r <= 0;
        fft_azimut_r <= 0;
        fft_azimut8_r <= 0;
        azimut8_r <= 0;
    end
    
    if (azimut_0 & !azimut_0_prev) begin
        cnt_low_allowed_clk <= 0;
        cnt_high_allowed_clk <= 0;
        azimut_r <= 0;
        fft_azimut_r <= 0;
        fft_azimut8_r <= 0;
        azimut8_r <= 0;
        reset_cnt_trig <= 1;
        azimut_0_prev <= 1;
    end
    else begin
        reset_cnt_trig <= 0;
        
    end  

    if(!azimut_0)
        azimut_0_prev <= 0;
end
   


endmodule

