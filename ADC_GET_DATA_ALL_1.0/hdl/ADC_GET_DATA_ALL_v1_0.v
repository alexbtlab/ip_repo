`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2017 17:00:50
// Design Name: 
// Module Name: adc_get_data
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adc_get_data(   
    
    //input wire clk_100MHz_adc,
    input wire test1,
    input wire test2,
    input wire test3,
    input wire test4,
    input wire tsweep,
    input wire fpga_clk,
    input wire adc_dco,
    input wire adc_da,
    input wire adc_db,
    input wire uart_clk,    
    output wire [7:0] output_data,
    output wire [17:0] output_data_my,    
    input wire is_sending,    
    output wire send_uart_byte,
    output wire adc_clk 
    );
    
    wire clk_adc, clk_adc_delay, clk_fft, clk_bram, clk_mult;
    
    wire clk_locked;
    
      clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_adc),
      .clk_out2(clk_adc_delay), 
                
      .clk_in1(fpga_clk),
      .locked(clk_locked)                      
    );
    
    reg[7:0] output_data_reg = 0;
    reg send_uart_byte_reg = 0;
    reg is_counting = 0;
    reg [18:0] cnt_uart = 0;
    
    assign output_data = output_data_reg;
    assign send_uart_byte = send_uart_byte_reg;
    
    reg adc_da_prev_reg=0;
    reg adc_db_prev_reg=0;
    
    reg [41:0] fft_input_data=0;
    reg fft_input_tvalid = 1;
    wire fft_input_tready;
    wire [41:0] fft_output_data;
    wire fft_output_tvalid;
    reg s_axis_data_tlast = 0;
    
    wire s_axis_config_tready, m_axis_data_tlast, event_frame_started, event_tlast_unexpected, event_tlast_missing;
    wire event_data_in_channel_halt, event_status_channel_halt, event_data_out_channel_halt;
    
   
    
    
     
    
    reg [7:0] adc_tfirstclk_reg = 1;
    reg is_tfirstclk_counting = 0; 
    reg adc_reading_allowed_reg = 0;
    reg adc_reading_allowed_neg_reg = 1;
    reg adc_bytes_read_num_reg = 0;
    reg [7:0] adc_current_clk_num_reg = 1;
    
    reg [17:0] adc_data_pos;
    reg [17:0] adc_data_neg;
    //reg [17:0] adc_data [2047:0];
    
    
   
    
    assign adc_clk = clk_adc&(adc_reading_allowed_reg>0)&(adc_reading_allowed_neg_reg>0);
    
    localparam ADC_TFIRSTCLK_MIN = 4; 
    
    reg is_sending_prev = 0;
    
    reg fpgaclk_prev = 0;
    
    reg[7:0] adc_current_strobe_num_reg = 1;
    
    reg adc_data_saving_needed = 0;
    
    reg [18:0] adc_data_last_saved_number = 0;
    reg [18:0] adc_current_transmit_data_number = 0;
    reg [2:0] adc_current_transmit_part_number = 0; 
    
    reg is_it_first_strobe = 0; 
    
    reg is_strobe_clk_active=0;
    reg is_strobe_clk_active_neg=1;
    
    wire clk_adc_strobe;
    
    assign clk_adc_strobe=clk_adc_delay&(is_strobe_clk_active>0)&(is_strobe_clk_active_neg>0);
    
    assign test1 = adc_current_strobe_num_reg[2];
    assign test2 = adc_current_strobe_num_reg[1];
    assign test3 = clk_adc_strobe;
    assign test4 = adc_current_strobe_num_reg[0];
    
       
    localparam ADC_BUF_DEPTH=8192;
    
    reg adc_bram_we=0;     
    reg [18:0] adc_bram_addr_write = 0;
    reg [18:0] adc_bram_addr_read = 0;
    reg [17:0] adc_bram_din = 0;
    wire [17:0] adc_bram_dout;
        
   assign output_data_my = adc_bram_din;
        
    wire fft_bram_we;
    reg fft_bram_we_en=0;
    assign fft_bram_we = clk_fft&fft_bram_we_en;
    reg [18:0] fft_bram_addr_write = 0;
    reg [18:0] fft_bram_addr_read = 0;
    reg [39:0] fft_bram_din = 0;
    wire [39:0] fft_bram_dout;
        
   
    

 

  

   

    
    always @(posedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_reg==1) is_strobe_clk_active<=1;
        else is_strobe_clk_active<=0;
        
    end
    
     always @(negedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_neg_reg==1) is_strobe_clk_active_neg<=1;
        else is_strobe_clk_active_neg<=0;
        
    end
    
    reg[3:0] fpga_clk_counter=0;
    reg tfirstclk_passed = 0;
    reg tfirstclk_passed_prev = 0;
    
    always @(posedge fpga_clk)
    begin
        if (fpga_clk_counter<9) fpga_clk_counter<=fpga_clk_counter+1;
            else fpga_clk_counter <= 0;
            if (fpga_clk_counter==7) tfirstclk_passed <= 1;
            if (fpga_clk_counter==9) tfirstclk_passed <= 0;
    end
    
     
     
    always @(posedge (clk_adc&clk_locked))
    begin
       if (tfirstclk_passed==0) tfirstclk_passed_prev<=0;
       else
       if (tfirstclk_passed_prev == 0)
       begin
            tfirstclk_passed_prev<=1;
            adc_reading_allowed_reg <= 1;
            is_it_first_strobe <= 1;
       end
                     
       if (adc_reading_allowed_reg == 1)
       begin
         
         if (adc_current_clk_num_reg < 5) 
         begin            
            adc_current_clk_num_reg<=adc_current_clk_num_reg+1;
            is_it_first_strobe <= 0;            
         end
         else if (adc_current_clk_num_reg == 5)
         begin
            adc_reading_allowed_reg <= 0;
            adc_current_clk_num_reg <= 1;            
            end
       end
    end
    
    always @(negedge (clk_adc&clk_locked))
    begin
        if (adc_current_clk_num_reg == 5) adc_reading_allowed_neg_reg <= 0;
        if (adc_current_clk_num_reg == 1) adc_reading_allowed_neg_reg <= 1;  
    end 
    
    always @(posedge (clk_adc_strobe&clk_locked))
    begin 
         if (is_it_first_strobe==1)
         begin          
            adc_data_pos[17] <= adc_da;
            adc_data_pos[16] <= adc_db;
            adc_current_strobe_num_reg<=1;
         end
         else if (adc_current_strobe_num_reg<5)         
         begin 
            adc_data_pos[17-((adc_current_strobe_num_reg)<<2)] <= adc_da;
            adc_data_pos[16-((adc_current_strobe_num_reg)<<2)] <= adc_db;
            
            adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;            
         end 
         else adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;      
    end
   
    reg is_sending_prev1=0;
    reg writing_adc_data_needed=1;
    reg is_data_ready = 0;
    reg is_new_sweep_started = 0;
    reg tsweep_prev = 1;
    reg [17:0] adc_data;
    reg adc_bram_we_backup = 0;
   
    always @(negedge (clk_adc_strobe&clk_locked))    
    begin
        if (is_sending == 0)
        begin
            if (is_sending_prev1==1) writing_adc_data_needed<=1;            
            is_sending_prev1 <= 0;
        end
        else is_sending_prev1 <= 1;
    
        
    
       if ((writing_adc_data_needed==1)&(is_new_sweep_started==1)&(adc_current_transmit_data_number == ADC_BUF_DEPTH)) adc_data_last_saved_number <= 0; 
               
       if (adc_current_strobe_num_reg==5)  
       begin
            
            if (tsweep>0)
            begin
                if (tsweep_prev==0) is_new_sweep_started<=1;
                else is_new_sweep_started <= 0;
                tsweep_prev <= 1;
            end
            else tsweep_prev <= 0;
            
            if ((adc_data_last_saved_number < ADC_BUF_DEPTH)&(is_data_ready==1))
            begin
                writing_adc_data_needed<=0;
                //adc_data_saving_needed <= 1;                
                
                //adc_data[adc_data_last_saved_number] <= adc_data_pos|adc_data_neg;
                
                adc_bram_we_backup <= 1'b1;
                                
                
                adc_bram_din <= adc_data_pos|adc_data_neg;
                adc_bram_addr_write <= adc_data_last_saved_number;
                
                is_data_ready <= 0;
                
                adc_data_last_saved_number <= adc_data_last_saved_number + 1;
            end
            
       end
       else if (adc_current_strobe_num_reg<5)
       begin
       if ((adc_current_strobe_num_reg==1)&(adc_bram_we_backup == 1'b1)) adc_bram_we <= 1'b1;
       if (adc_current_strobe_num_reg==4) begin adc_bram_we <= 1'b0; adc_bram_we_backup<=1'b0; end
       //adc_data_saving_needed <= 0;
       //bram_addr_write <= 0;
       
       adc_data_neg[15-((adc_current_strobe_num_reg-1)<<2)] <= adc_da;
       adc_data_neg[14-((adc_current_strobe_num_reg-1)<<2)] <= adc_db;
       if (adc_current_strobe_num_reg==4) is_data_ready <= 1;
       end
    end
    
    
    
endmodule
