
#ifndef AD9650_H
#define AD9650_H


/****************** Include Files ********************/
#include "xil_types.h"
#include "xstatus.h"
#include "xil_io.h"
#include "xparameters.h"

#define XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR XPAR_HIER_1_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR
#define XPAR_AD9650_0_S00_AXI_BASEADDR              XPAR_HIER_0_AD9650_0_S00_AXI_BASEADDR

#define READ  1
#define WRITE 0

#define AD9650_S00_AXI_SLV_REG0_OFFSET 0
#define AD9650_S00_AXI_SLV_REG1_OFFSET 4
#define AD9650_S00_AXI_SLV_REG2_OFFSET 8
#define AD9650_S00_AXI_SLV_REG3_OFFSET 12

#define AD9650_CHANNEL_INDEX_REG 0x5
#define AD9650_CLOCK_REG 0x9
#define AD9650_CLOCK_DIVIDE_REG  0xB	
#define AD9650_DEVICE_UPDATE_REG 0xFF
#define AD9650_SYNC_CONTROL_REG 0x100

#define AD9650_SW_TRANSFER_BITREG ( 1 << 0 )
#define AD9650_DUTY_CYCLE_STABILIZE_BITREG ( 1 << 0 )

void AD9650_ADC_SetSwitch_AMP(uint32_t numAmpEnable);
void AD9650_SetAdrData_andStartTransfer(u32 adr, u32 data, u8 stateRW);
void AD9650_DIVIDE_CLOCK_10();
/**************************** Type Definitions *****************************/
/**
 *
 * Write a value to a AD9650 register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the AD9650device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void AD9650_mWriteReg(u32 BaseAddress, unsigned RegOffset, u32 Data)
 *
 */
#define AD9650_mWriteReg(BaseAddress, RegOffset, Data) \
  	Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))

/**
 *
 * Read a value from a AD9650 register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the AD9650 device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	u32 AD9650_mReadReg(u32 BaseAddress, unsigned RegOffset)
 *
 */
#define AD9650_mReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))

/************************** Function Prototypes ****************************/
/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the AD9650 instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus AD9650_Reg_SelfTest(void * baseaddr_p);

#endif // AD9650_H
