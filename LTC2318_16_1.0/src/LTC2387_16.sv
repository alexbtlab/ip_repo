`timescale 1ns / 1ps

interface diff;
  logic p, n;      // Indicates if slave is ready to accept data
  modport slave  (input p, n);
  modport master (output p, n);
endinterface

module LTC2387_16(
    output wire [15:0] ADC_DATA,
    input wire fpga_clk_async, 
    output wire fpga_clk_sync, 
    
    input wire clk_100,
    input wire clk_200,  
    diff.slave adc_dax,
    diff.slave adc_dbx,
    diff.slave adc_dcox,
    diff.master adc_clkx
    
//    input wire [4 : 0] Delay,
//    input wire LD,
//    output wire [4 : 0] CNTVALUEOUT
);
    
   reg [15:0] ADC_DATA_r;
   assign ADC_DATA = ADC_DATA_r;
   wire [15:0] ADC_DATA_inner; 
   reg [2:0] cnt100 = 0;  
   wire adc_da;
   wire adc_db;
   wire adc_dco;
   wire adc_clk; 
   wire adc_clk_delayed;
   
    OBUFDS #( 
        .IOSTANDARD("LVDS_25"), // Specify the output I/O standard 
        .SLEW("FAST") // Specify the output slew rate 
    ) OBUFDS_adc_clk ( 
        .O(adc_clkx.p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_clkx.n), // Diff_n output (connect directly to top-level port) 
        .I(adc_clk_delayed) // Buffer input 
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da (
        .O(adc_da), // Buffer output
        .I(adc_dax.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dax.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_db (
        .O(adc_db), // Buffer output
        .I(adc_dbx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dbx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_dco (
        .O(adc_dco), // Buffer output
        .I(adc_dcox.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dcox.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    reg idelayctrl_rst = 0;
    wire idelayctrl_rdy;
    
    
    IDELAYCTRL IDELAYCTRL_inst (
    .RDY(idelayctrl_rdy), // 1-bit output: Ready output    
    .REFCLK(clk_200), // 1-bit input: Reference clock input
    .RST(idelayctrl_rst) // 1-bit input: Active high reset input
    );
    
//    wire [4 : 0] CNTVALUEOUT;
    wire adc_dco_delayed;
    //assign adc_dco_delayed_ILA = adc_dco_delayed;
//    IDELAYE2 #(
//        .CINVCTRL_SEL("FALSE"),             // Enable dynamic clock inversion (FALSE, TRUE)
//        .DELAY_SRC("DATAIN"),               // Delay input (IDATAIN, DATAIN)
//        .HIGH_PERFORMANCE_MODE("TRUE"),     // Reduced jitter ("TRUE"), Reduced power ("FALSE")
//        .IDELAY_TYPE ("VAR_LOAD"),             // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
//        .IDELAY_VALUE(11),                   //Input delay tap setting (0-31). Delay = 600ps + 78 ps*31=  ns
//        .PIPE_SEL("FALSE"),                 // Select pipelined mode, FALSE, TRUE
//        .REFCLK_FREQUENCY(200.0),           // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
//        .SIGNAL_PATTERN ("CLOCK")           // DATA, CLOCK input signal
//    ) IDELAYE2_adc_if2_ch1_dco (
//        .CNTVALUEOUT(CNTVALUEOUT),
//        .C(clk_200),
//        .DATAOUT(adc_dco_delayed), // Buffer output
//        .DATAIN(adc_dco),
//        .LD(LD),
//        .CNTVALUEIN(Delay)
//    );
    
    IDELAYE2 #(
        .CINVCTRL_SEL("FALSE"),             // Enable dynamic clock inversion (FALSE, TRUE)
        .DELAY_SRC("DATAIN"),               // Delay input (IDATAIN, DATAIN)
        .HIGH_PERFORMANCE_MODE("TRUE"),     // Reduced jitter ("TRUE"), Reduced power ("FALSE")
        .IDELAY_TYPE ("FIXED"),             // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
        .IDELAY_VALUE(31),                   //Input delay tap setting (0-31). Delay = 600ps + 78 ps*31=  ns
        .PIPE_SEL("FALSE"),                 // Select pipelined mode, FALSE, TRUE
        .REFCLK_FREQUENCY(200.0),           // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
        .SIGNAL_PATTERN ("CLOCK")           // DATA, CLOCK input signal
    ) IDELAYE2_adc_if2_ch1_dco2 (
//        .CNTVALUEOUT(CNTVALUEOUT),
        .C(clk_200),
        .DATAOUT(adc_clk_delayed), // Buffer output
        .DATAIN(adc_clk)
//        .LD(LD),
//        .CNTVALUEIN(Delay)
    );
    
    reg allowed = 0;
    assign adc_clk = (allowed) ? clk_100 : 0;
    reg [1:0] cnt_pos_dco = 0;
    
    reg bit_15 = 0;
    reg bit_14 = 0;
    reg bit_13 = 0;
    reg bit_12 = 0;
    reg bit_11 = 0;
    reg bit_10 = 0;
    reg bit_9 = 0;
    reg bit_8 = 0;
    reg bit_7 = 0;
    reg bit_6 = 0;
    reg bit_5 = 0;
    reg bit_4 = 0;
    reg bit_3 = 0;
    reg bit_2 = 0;
    reg bit_1 = 0;
    reg bit_0 = 0;

 	reg [15:0] ADC_DATA_w;
 	assign ADC_DATA_inner[15:0] = ADC_DATA_w[15:0];

 	assign     ADC_DATA_w[0] = bit_0;
 	assign     ADC_DATA_w[1] = bit_1;
 	assign     ADC_DATA_w[2] = bit_2;
 	assign     ADC_DATA_w[3] = bit_3;
 	assign     ADC_DATA_w[4] = bit_4;
 	assign     ADC_DATA_w[5] = bit_5;
 	assign     ADC_DATA_w[6] = bit_6;
 	assign     ADC_DATA_w[7] = bit_7;
 	assign     ADC_DATA_w[8] = bit_8;
 	assign     ADC_DATA_w[9] = bit_9;
 	assign     ADC_DATA_w[10] = bit_10;
 	assign     ADC_DATA_w[11] = bit_11;
 	assign     ADC_DATA_w[12] = bit_12;
 	assign     ADC_DATA_w[13] = bit_13;
 	assign     ADC_DATA_w[14] = bit_14;
 	assign     ADC_DATA_w[15] = bit_15;

 reg reset_cnt = 0;
 reg fpga_clk_sync_r;
 assign fpga_clk_sync = fpga_clk_sync_r;
 
always @ (negedge clk_100) begin 
      if(fpga_clk_async)
        fpga_clk_sync_r <= 1;
      else
        fpga_clk_sync_r <= 0;  
end
       
always @ (posedge fpga_clk_sync) begin 
      ADC_DATA_r <= ADC_DATA_inner;
end
/*----------------------------------------------------------------------------------------------*/
//always @ (negedge fpga_clk_sync) begin 
//      ADC_DATA_r <= ADC_DATA_inner;
//end
/*----------------------------------------------------------------------------------------------*/
always @ (negedge clk_100) begin 
    if(~fpga_clk_sync) begin
       
        //if(cnt100 == 10)
        //    cnt100 <= 0;
        //else
            cnt100 <= cnt100 + 1;
        if(cnt100 == 0) begin 
            reset_cnt <= 1;
        end 
        else begin
            reset_cnt <= 0;	
        end
    end 
    else begin
        reset_cnt <= 1;
        cnt100 <= 0;
    end       
end
/*----------------------------------------------------------------------------------------------*/                 
always @ (posedge clk_100) begin 
    if(cnt100 == 2) begin
        bit_15 <= adc_da;
        bit_14 <= adc_db;
    end
    
    allowed <= (cnt100 > 1) & (cnt100 <= 5); 
end
/*----------------------------------------------------------------------------------------------*/                 
reg [1:0] cnt_neg_dco = 0;

always @ (posedge adc_dco) begin   
        if(cnt100 == 3) begin
        	cnt_pos_dco <= 1;
            bit_13 <= adc_da;
            bit_12 <= adc_db;
        end
        else if(cnt_pos_dco == 1) begin
            cnt_pos_dco <= 2;
            bit_9 <= adc_da;
            bit_8 <= adc_db; 
        end
        else if(cnt_pos_dco == 2) begin
            cnt_pos_dco <= 3;
            bit_5 <= adc_da;
            bit_4 <= adc_db; 
        end
       else if(cnt_pos_dco == 3) begin
            cnt_pos_dco <= 0;
            bit_1 <= adc_da;
            bit_0 <= adc_db;
        end 
end    
 /*----------------------------------------------------------------------------------------------*/
always @ (negedge adc_dco) begin 
        if(cnt_pos_dco == 1) begin
            bit_11 <= adc_da;
            bit_10 <= adc_db;
        end
        else if(cnt_pos_dco == 2) begin
            bit_7 <= adc_da;
            bit_6 <= adc_db; 
        end
        else if(cnt_pos_dco == 3)  begin   
            bit_3 <= adc_da;
            bit_2 <= adc_db; 
        end
end    
endmodule   
