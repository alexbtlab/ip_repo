`timescale 1ns / 1ps


interface diff;
  logic p, n;      // Indicates if slave is ready to accept data
  modport slave  (input p, n);
  modport master (output p, n);
endinterface


module adc_get_data(
    input wire adc_clk_valid,
    //input wire fpga_clk,
    //input wire adc_dco,
    //input wire adc_da,
    //input wire adc_db,    
    //output wire adc_clk,
    output wire [17:0] adc_data,
    output wire data_ready,
    output wire ready_clk,

    diff.slave adc_dax,
    diff.slave adc_dbx,
    //diff.slave adc_dcox,
    diff.slave fpga_clkx,
    diff.master adc_clkx
    );
    
    reg[7:0] adc_current_strobe_num_reg = 1;
    assign ready_clk = (adc_current_strobe_num_reg==5) ? 1 : 0;
    
    wire adc_clk;
    //wire adc_dco;
    wire adc_da;
    wire adc_db;
    wire fpga_clk;
        
    OBUFDS #( 
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard 
        .SLEW("SLOW") // Specify the output slew rate 
    ) OBUFDS_adc_clk ( 
        .O(adc_clkx.p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_clkx.n), // Diff_n output (connect directly to top-level port) 
        .I(adc_clk) // Buffer input 
    );

    //IBUFDS #(
    //    .DIFF_TERM("TRUE"), // Differential Termination
    //    .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
    //    .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    //) IBUFDS_adc_dco (
    //    .O(adc_dco), // Buffer output
    //    .I(adc_dcox.p), // Diff_p buffer input (connect directly to top-level port)
    //    .IB(adc_dcox.n) // Diff_n buffer input (connect directly to top-level port)
    //);
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da (
        .O(adc_da), // Buffer output
        .I(adc_dax.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dax.n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_db (
        .O(adc_db), // Buffer output
        .I(adc_dbx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dbx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_fpga_clk (
        .O(fpga_clk), // Buffer output
        .I(fpga_clkx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(fpga_clkx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    reg is_data_ready = 0;
    
    assign data_ready = is_data_ready;
    
    wire clk_adc, clk_adc_delay;
    
    wire clk_locked;
    
      clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_adc),
      .clk_out2(clk_adc_delay),                 
      .clk_in1(fpga_clk & adc_clk_valid),
      .locked(clk_locked)                      
    );
    
    reg[17:0] output_data_reg = 0;
    reg send_uart_byte_reg = 0;
    reg is_counting = 0;
    reg [18:0] cnt_uart = 0;
    
    //assign adc_data = output_data_reg;
    //assign send_uart_byte = send_uart_byte_reg;
    
    reg adc_da_prev_reg=0;
    reg adc_db_prev_reg=0;
    
    
   
    
     //��� ��� ���� ��������� �������� ����� 10 �� (�� clk_adc). � ������ ����� ������� ��� ����.
    
    reg [7:0] adc_tfirstclk_reg = 1;
    reg is_tfirstclk_counting = 0; 
    reg adc_reading_allowed_reg = 0;
    reg adc_reading_allowed_neg_reg = 1;
    reg adc_bytes_read_num_reg = 0;
    reg[7:0] adc_current_clk_num_reg = 1;
    
    reg [17:0] adc_data_pos = 0;
    reg [17:0] adc_data_neg = 0;
    
    assign adc_data = adc_data_pos|adc_data_neg;
    
    assign adc_clk = clk_adc&(adc_reading_allowed_reg>0)&(adc_reading_allowed_neg_reg>0);
    
    
    reg is_sending_prev = 0;
    
    reg fpgaclk_prev = 0;
    
    
    
    reg adc_data_saving_needed = 0;
    
    reg[18:0] adc_data_last_saved_number = 0;
    reg [18:0] adc_current_transmit_data_number = 0;
    reg [1:0] adc_current_transmit_part_number = 0; 
    
    reg is_it_first_strobe = 0; 
    
    reg is_strobe_clk_active=0;
    reg is_strobe_clk_active_neg=1;
    
    wire clk_adc_strobe;
    
    assign clk_adc_strobe=clk_adc_delay&(is_strobe_clk_active>0)&(is_strobe_clk_active_neg>0);
    
    always @(posedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_reg==1) is_strobe_clk_active<=1;
        else is_strobe_clk_active<=0;        
    end
    
    always @(negedge clk_adc_delay)
    begin        
        if (adc_reading_allowed_neg_reg==1) is_strobe_clk_active_neg<=1;
        else is_strobe_clk_active_neg<=0;        
    end
    
    reg[3:0] fpga_clk_counter=0;
    reg tfirstclk_passed = 0;
    reg tfirstclk_passed_prev = 0;
    
    always @(posedge fpga_clk)
    begin
        if (fpga_clk_counter<9) fpga_clk_counter<=fpga_clk_counter+1;
            else fpga_clk_counter <= 0;
        if (fpga_clk_counter==7) tfirstclk_passed <= 1;
        else if (fpga_clk_counter==9) tfirstclk_passed <= 0;
    end
    
    always @(posedge (clk_adc&clk_locked))
    begin
       if (tfirstclk_passed==0) tfirstclk_passed_prev<=0;
       else
       if (tfirstclk_passed_prev == 0)
       begin
            tfirstclk_passed_prev<=1;
            adc_reading_allowed_reg <= 1;
            is_it_first_strobe <= 1;
       end
                     
       if (adc_reading_allowed_reg == 1)
       begin         
         if (adc_current_clk_num_reg < 5) 
         begin            
            adc_current_clk_num_reg <= adc_current_clk_num_reg+1;
            is_it_first_strobe <= 0;            
         end
         else if (adc_current_clk_num_reg == 5)
         begin
            adc_reading_allowed_reg <= 0;
            adc_current_clk_num_reg <= 1;            
            end
       end
    end
    
    always @(negedge (clk_adc&clk_locked))
    begin
        if (adc_current_clk_num_reg == 5) adc_reading_allowed_neg_reg <= 0;
        if (adc_current_clk_num_reg == 1) adc_reading_allowed_neg_reg <= 1;  
    end 
    
    always @(posedge (clk_adc_strobe&clk_locked))
    begin 
         if (is_it_first_strobe==1)
         begin          
            adc_data_pos[17] <= adc_da;
            adc_data_pos[16] <= adc_db;
            adc_current_strobe_num_reg<=1;
         end
         else if (adc_current_strobe_num_reg<5)         
         begin 
            adc_data_pos[17-((adc_current_strobe_num_reg)<<2)] <= adc_da;
            adc_data_pos[16-((adc_current_strobe_num_reg)<<2)] <= adc_db;
            
            adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;            
         end 
         else adc_current_strobe_num_reg<=adc_current_strobe_num_reg+1;      
    end
   
   always @(negedge (clk_adc_strobe&clk_locked))    
    begin
       
       if (adc_current_strobe_num_reg==5)   is_data_ready <= 1;
       else if (adc_current_strobe_num_reg<5)
       begin
           adc_data_neg[15-((adc_current_strobe_num_reg-1)<<2)] <= adc_da;
           adc_data_neg[14-((adc_current_strobe_num_reg-1)<<2)] <= adc_db;
           if (adc_current_strobe_num_reg==4) is_data_ready <= 1;
           if (adc_current_strobe_num_reg==1) is_data_ready <= 0;
       end
    end
endmodule
