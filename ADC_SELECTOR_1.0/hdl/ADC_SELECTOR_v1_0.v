
`timescale 1 ns / 1 ps

module ADC_SELECTOR_v1_0 
	(
		input wire  ADC_CLK_IF1,
		input wire  ADC_CLK_IF2,
		input wire [15:0] ADC_DATA_IF1,
		input wire [15:0] ADC_DATA_IF2,

		input wire CH_SEL,

		output reg [15:0] ADC_DATA,
		output reg  ADC_CLK
	);

  	always @ (ADC_CLK_IF1, ADC_CLK_IF2, CH_SEL) begin
    case(CH_SEL)
      1'b00    : begin ADC_DATA = ADC_DATA_IF1; ADC_CLK	= ADC_CLK_IF1;   end		
      1'b01    : begin ADC_DATA = ADC_DATA_IF2; ADC_CLK	= ADC_CLK_IF2;   end
      default  : begin ADC_DATA = 0; 			ADC_CLK	= 0;  		     end 		
    endcase
  	end
 
endmodule 
