module uart(
    input wire clk,
    input wire uart_rx,
    input wire [7:0] uart_data_tx,
    
    
    output wire byte_received,
    output wire [7:0] uart_data_rx,
    
    
    
    output wire uart_tx,
    output wire is_transmission_going,
    input wire send_byte      
    );
    
  
    
  uart_send   uart_send_inst(
    .uart_data(uart_data_tx),//input wire [7:0] uart_data,
    .clk(clk),//input wire clk,    
    .uart_tx(uart_tx),//output wire uart_tx,
    .is_transmission_going(is_transmission_going),//output wire is_transmission_going,
    .send_byte(send_byte)//input wire send_byte      
    );
    
 uart_receive uart_receive_inst(
    .clk(clk),//input wire clk,
    .uart_rx(uart_rx),//input wire uart_rx,
    .byte_received(byte_received),//output wire byte_received,
    .uart_data(uart_data_rx)//output wire [7:0] uart_data
    );
 endmodule