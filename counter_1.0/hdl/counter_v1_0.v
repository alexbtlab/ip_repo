module counter(
		input wire clk,
		input wire reset,
		input wire [15:0] FrameSize,
		output wire ready,
		output wire [15:0] count,
		output wire div_clk
);

assign div_clk = count[13];
reg [15:0] cnt = 0;

assign count = cnt;
assign ready = clk;

reg direct = 0 ;
always @(negedge reset) begin
    if(direct)
        direct <= 0;
    else
        direct <= 1;           
end
always @(negedge clk) begin

    if(!reset) begin
        if(direct)     cnt <= 16'h0000;
        else           cnt <= FrameSize;
    end
    else begin
        if(direct) begin
           if(cnt == FrameSize)
                cnt <= FrameSize;
           else
                cnt <= cnt + 1;    
        end
        else begin
           if(cnt == 0)
                cnt <= 0;
           else
                cnt <= cnt - 1;   
        end
    end 
           
end
endmodule