
`timescale 1 ns / 1 ps

	module myip_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S05_AXI
		parameter integer C_S05_AXI_DATA_WIDTH	= 32,
		parameter integer C_S05_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S05_AXI
		input wire  s05_axi_aclk,
		input wire  s05_axi_aresetn,
		input wire [C_S05_AXI_ADDR_WIDTH-1 : 0] s05_axi_awaddr,
		input wire [2 : 0] s05_axi_awprot,
		input wire  s05_axi_awvalid,
		output wire  s05_axi_awready,
		input wire [C_S05_AXI_DATA_WIDTH-1 : 0] s05_axi_wdata,
		input wire [(C_S05_AXI_DATA_WIDTH/8)-1 : 0] s05_axi_wstrb,
		input wire  s05_axi_wvalid,
		output wire  s05_axi_wready,
		output wire [1 : 0] s05_axi_bresp,
		output wire  s05_axi_bvalid,
		input wire  s05_axi_bready,
		input wire [C_S05_AXI_ADDR_WIDTH-1 : 0] s05_axi_araddr,
		input wire [2 : 0] s05_axi_arprot,
		input wire  s05_axi_arvalid,
		output wire  s05_axi_arready,
		output wire [C_S05_AXI_DATA_WIDTH-1 : 0] s05_axi_rdata,
		output wire [1 : 0] s05_axi_rresp,
		output wire  s05_axi_rvalid,
		input wire  s05_axi_rready
	);
// Instantiation of Axi Bus Interface S05_AXI
	myip_v1_0_S05_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S05_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S05_AXI_ADDR_WIDTH)
	) myip_v1_0_S05_AXI_inst (
		.S_AXI_ACLK(s05_axi_aclk),
		.S_AXI_ARESETN(s05_axi_aresetn),
		.S_AXI_AWADDR(s05_axi_awaddr),
		.S_AXI_AWPROT(s05_axi_awprot),
		.S_AXI_AWVALID(s05_axi_awvalid),
		.S_AXI_AWREADY(s05_axi_awready),
		.S_AXI_WDATA(s05_axi_wdata),
		.S_AXI_WSTRB(s05_axi_wstrb),
		.S_AXI_WVALID(s05_axi_wvalid),
		.S_AXI_WREADY(s05_axi_wready),
		.S_AXI_BRESP(s05_axi_bresp),
		.S_AXI_BVALID(s05_axi_bvalid),
		.S_AXI_BREADY(s05_axi_bready),
		.S_AXI_ARADDR(s05_axi_araddr),
		.S_AXI_ARPROT(s05_axi_arprot),
		.S_AXI_ARVALID(s05_axi_arvalid),
		.S_AXI_ARREADY(s05_axi_arready),
		.S_AXI_RDATA(s05_axi_rdata),
		.S_AXI_RRESP(s05_axi_rresp),
		.S_AXI_RVALID(s05_axi_rvalid),
		.S_AXI_RREADY(s05_axi_rready)
	);

	// Add user logic here

	// User logic ends

	endmodule
