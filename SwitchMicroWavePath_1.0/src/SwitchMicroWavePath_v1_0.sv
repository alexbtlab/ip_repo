interface sw_base;
    logic sw_out_if1_A0, 
          sw_out_if1_A1, 
          sw_out_if2_A0,
          sw_out_if2_A1;      
    modport master  (output sw_out_if1_A0, sw_out_if1_A1, sw_out_if2_A0, sw_out_if2_A1);
endinterface

interface sw_1;
    logic sw1_if1_ctrl, 
          sw1_if2_ctrl;      
    modport master  (output sw1_if1_ctrl, sw1_if2_ctrl);
endinterface

interface sw_2;
    logic sw2_if1_ctrl, 
          sw2_if2_ctrl;      
    modport master  (output sw2_if1_ctrl, sw2_if2_ctrl);
endinterface

interface sw_3;
    logic sw3_if1_ctrl, 
          sw3_if2_ctrl;      
    modport master  (output sw3_if1_ctrl, sw3_if2_ctrl);
endinterface

interface rx_amp;
    logic amp0,
          amp1, 
          amp2;      
    modport master  (output amp0, amp1, amp2);
endinterface

	module SwitchMicroWavePath_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		
		sw_base.master SW_BASE,
		sw_1.master SW_1,
		sw_2.master SW_2,
		sw_3.master SW_3,
		
		rx_amp.master RX_AMP
		
//		output wire  low_amp_en
	);
	
//	assign low_amp_en = 1;
	
	wire [C_S00_AXI_DATA_WIDTH-1 : 0] switch_IF1_IF2_val;
	wire [C_S00_AXI_DATA_WIDTH-1 : 0] switch_CTRL_val;
	wire [2 : 0] rx_amp;
	
// Instantiation of Axi Bus Interface S00_AXI
	SwitchMicroWavePath_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) SwitchMicroWavePath_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		.slv_reg0(switch_IF1_IF2_val),             // Switch IF1 IF2 channel Value 
		.slv_reg1(switch_CTRL_val),                 // SW CTRL IF1 IF2 channel Value
		.slv_reg2(rx_amp)
	);
    reg  sw_out_if1_A0_r;
    reg  sw_out_if1_A1_r;
    reg  sw_out_if2_A0_r;
    reg  sw_out_if2_A1_r;
    reg  sw1_if1_ctrl_r;
    reg  sw1_if2_ctrl_r;
    reg  sw2_if1_ctrl_r;
    reg  sw2_if2_ctrl_r;
    reg  sw3_if1_ctrl_r;
    reg  sw3_if2_ctrl_r;
    
    assign SW_BASE.sw_out_if1_A0 = sw_out_if1_A0_r;
    assign SW_BASE.sw_out_if1_A1 = sw_out_if1_A1_r;
    assign SW_BASE.sw_out_if2_A0 = sw_out_if2_A0_r;
    assign SW_BASE.sw_out_if2_A1 = sw_out_if2_A1_r;
    
    assign SW_1.sw1_if1_ctrl = sw1_if1_ctrl_r;
    assign SW_1.sw1_if2_ctrl = sw1_if2_ctrl_r;
    assign SW_2.sw2_if1_ctrl = sw2_if1_ctrl_r;
    assign SW_2.sw2_if2_ctrl = sw2_if2_ctrl_r;
    assign SW_3.sw3_if1_ctrl = sw3_if1_ctrl_r;
    assign SW_3.sw3_if2_ctrl = sw3_if2_ctrl_r;
    
    assign RX_AMP.amp0 = rx_amp[0];
    assign RX_AMP.amp1 = rx_amp[1];
    assign RX_AMP.amp2 = rx_amp[2];
    
    always @ (posedge s00_axi_aclk) begin
    
        if(s00_axi_aresetn & switch_CTRL_val[0]) begin
          
                sw_out_if1_A0_r <= switch_IF1_IF2_val[0];
                sw_out_if1_A1_r <= switch_IF1_IF2_val[1];
                sw_out_if2_A0_r <= switch_IF1_IF2_val[2];
                sw_out_if2_A1_r <= switch_IF1_IF2_val[3];
                
                sw1_if1_ctrl_r <= switch_IF1_IF2_val[4];
                sw1_if2_ctrl_r <= switch_IF1_IF2_val[5];
                
                sw2_if1_ctrl_r <= switch_IF1_IF2_val[6];
                sw2_if2_ctrl_r <= switch_IF1_IF2_val[7];
                
                sw3_if1_ctrl_r <= switch_IF1_IF2_val[8];
                sw3_if2_ctrl_r <= switch_IF1_IF2_val[9];
                             
        end
    end
    
	endmodule
