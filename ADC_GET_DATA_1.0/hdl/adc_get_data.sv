`timescale 1ns / 1ps


interface diff;
  logic p, n;      // Indicates if slave is ready to accept data
  modport slave  (input p, n);
  modport master (output p, n);
endinterface


module LTC2387_18(
    //input wire main_clk,
    input wire adc_clk_valid,
    //input wire fpga_clk,
    //input wire adc_dco,
    //input wire adc_da,
    //input wire adc_db,    
    //output wire adc_clk,
    //output wire [17:0] adc_data,
    //output wire data_ready,
    //output wire ready_clk,
    
    output wire adc_clk,
    output wire adc_da,
    output wire adc_db,
    output wire adc_dco,
    output wire adc_dco_del,
    output wire fpga_clk,
    output wire [7:0] cnt100_ila,
    output wire count100_allowed_ila,
    output reg [17:0] ADC_DATA,

    
    diff.slave adc_dax,
    diff.slave adc_dbx,
    diff.slave adc_dcox,
    diff.slave fpga_clkx,
    diff.master adc_clkx
    );
    
   wire clk_100;
   wire clk_200; 
   wire clk_locked;
    
    clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_100),
      .clk_out2(clk_200),                 
      .clk_in1(fpga_clk & adc_clk_valid),
      .locked(clk_locked)                      
    );  
        
    OBUFDS #( 
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard 
        .SLEW("SLOW") // Specify the output slew rate 
    ) OBUFDS_adc_clk ( 
        .O(adc_clkx.p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_clkx.n), // Diff_n output (connect directly to top-level port) 
        .I(adc_clk) // Buffer input 
    ); 
    
    //wire adc_dco;
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_dco (
        .O(adc_dco), // Buffer output
        .I(adc_dcox.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dcox.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
      IDELAYE2 #(
        .CINVCTRL_SEL("FALSE"), // Enable dynamic clock inversion (FALSE, TRUE)
        .DELAY_SRC("IDATAIN"), // Delay input (IDATAIN, DATAIN)
        .HIGH_PERFORMANCE_MODE("TRUE"),// Reduced jitter ("TRUE"), Reduced power ("FALSE")
        .IDELAY_TYPE ("FIXED"), // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
        .IDELAY_VALUE(31),//Input delay tap setting (0-31). Delay = 600ps + 78 ps*31=  ns
        .PIPE_SEL("FALSE"),// Select pipelined mode, FALSE, TRUE
        .REFCLK_FREQUENCY(200.0),// IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
        .SIGNAL_PATTERN ("DATA")// DATA, CLOCK input signal
    ) IDELAYE2_adc_dco (
        .C(clk_200),
        .DATAOUT(adc_dco_del), // Buffer output
        .IDATAIN(adc_dco) 
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da (
        .O(adc_da), // Buffer output
        .I(adc_dax.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dax.n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_db (
        .O(adc_db), // Buffer output
        .I(adc_dbx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dbx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_fpga_clk (
        .O(fpga_clk), // Buffer output
        .I(fpga_clkx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(fpga_clkx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    reg count100_allowed;
    assign count100_allowed_ila = count100_allowed;
    reg [7:0] cnt100 = 0;
    assign cnt100_ila  = cnt100;
    assign adc_clk = (cnt100 > 0) ? clk_100 : 0;
    reg cnt_pos_dco = 0;
    reg bit_17 = 0;
    reg bit_16 = 0;
    reg bit_15 = 0;
    reg bit_14 = 0;
    reg bit_13 = 0;
    reg bit_12 = 0;
    reg bit_11 = 0;
    reg bit_10 = 0;
    reg bit_9 = 0;
    reg bit_8 = 0;
    reg bit_7 = 0;
    reg bit_6 = 0;
    reg bit_5 = 0;
    reg bit_4 = 0;
    reg bit_3 = 0;
    reg bit_2 = 0;
    reg bit_1 = 0;
    reg bit_0 = 0;
    
    
 //wire [7:0] ADC_DATA1_w;
 //wire [7:0] ADC_DATA2_w;
 //wire [7:0] ADC_DATA3_w;
 //assign ADC_DATA1 = ADC_DATA1_w;
 //assign ADC_DATA2 = ADC_DATA2_w;
 //assign ADC_DATA3 = ADC_DATA3_w;
 
 assign ADC_DATA =    (bit_17  << 17)  |
                      (bit_16  << 16)  | 
                      (bit_15  << 15)  |
                      (bit_14  << 14)  | 
                      (bit_13  << 13)  |
                      (bit_12  << 12)  |
                      (bit_11  << 11)  | 
                      (bit_10  << 10)  |
                      (bit_9  << 9)  | 
                      (bit_8  << 8)  |
                      (bit_7  << 7)  |
                      (bit_6  << 6)  | 
                      (bit_5  << 5)  |
                      (bit_4  << 4)  | 
                      (bit_3  << 3)  |
                      (bit_2  << 2)  | 
                      (bit_1  << 1)  |
                       bit_0;
                                            
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    always @ (negedge fpga_clk) begin 
        if(cnt100 > 0) begin
            count100_allowed <= 1; 
        end
        else begin
            count100_allowed <= 0; 
        end         
    end
    //////////////////////////////////////////////////////////////////////////////////
     always @ (negedge clk_100) begin 
        if(~fpga_clk) begin
            cnt100 <= cnt100 + 1;
        end 
        else begin
            cnt100 <= 0;
        end       
    end
 ///////////////////////////////////////////////////////////////////////////////
                      
  always @ (posedge adc_dco_del) begin 
        if(cnt_pos_dco == 5) begin
            cnt_pos_dco <= 0; 
        end
        if(cnt_pos_dco == 4) begin
            cnt_pos_dco <=  cnt_pos_dco + 1; 
            bit_1 <= adc_da;
            bit_0 <= adc_db;
        end
        if(cnt_pos_dco == 3) begin
            cnt_pos_dco <=  cnt_pos_dco + 1; 
            bit_5 <= adc_da;
            bit_4 <= adc_db;
        end
        if(cnt_pos_dco == 2) begin
            cnt_pos_dco <=  cnt_pos_dco + 1;
            bit_9 <= adc_da;
            bit_8 <= adc_db; 
        end
        if(cnt_pos_dco == 1) begin
            cnt_pos_dco <=  cnt_pos_dco + 1;
            bit_13 <= adc_da;
            bit_12 <= adc_db; 
        end
        if(cnt_pos_dco == 0) begin
            cnt_pos_dco <=  cnt_pos_dco + 1;
            bit_17 <= adc_da;
            bit_16 <= adc_db;
        end           
   end    
 ////////////////////////////////////////////////////////////////
 reg cnt_neg_dco = 0;
 
    always @ (negedge adc_dco_del) begin 
        if(cnt_neg_dco == 5) begin
            cnt_neg_dco <= 0; 
        end
        if(cnt_neg_dco == 3) begin
            cnt_neg_dco <=  cnt_neg_dco + 1; 
            bit_3 <= adc_da;
            bit_2 <= adc_db;
        end
        if(cnt_neg_dco == 2) begin
            cnt_neg_dco <=  cnt_neg_dco + 1;
            bit_7 <= adc_da;
            bit_6 <= adc_db; 
        end
        if(cnt_neg_dco == 1) begin
            cnt_neg_dco <=  cnt_neg_dco + 1;
            bit_11 <= adc_da;
            bit_10 <= adc_db; 
        end
        if(cnt_neg_dco == 0) begin
            cnt_neg_dco <=  cnt_neg_dco + 1;
            bit_15 <= adc_da;
            bit_14 <= adc_db;
        end           
   end    
endmodule
