
`timescale 1 ns / 1 ps

	module FFT2DMA_v1_0 # 
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 64,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
	    input  wire  ch,
		output wire  s00_axis_tready,
		input  wire  [15:0] VAL_SET,
		//input  wire  [15:0] frame_size,
		//input wire  sel,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		input  wire  m00_axis_aresetn,
		input  wire  m00_axis_aclk,
		
		output reg interrupt_frame,
		//input  wire [15:0] frame_offset,
		//input  wire [15:0] averag_val,
		//input  wire [15:0] divide_val,
		output reg interrupt_frame_1ms
	);
	
	assign m00_axis_tlast  = s00_axis_tlast;
	assign m00_axis_tvalid = s00_axis_tvalid;
	
	
	
	
	assign m00_axis_tdata[30:0] =  (s00_axis_tdata[30] == 1) ? -s00_axis_tdata[30:0]  : s00_axis_tdata[30:0];
	assign m00_axis_tdata[31] =  1'b0;
	
	assign s00_axis_tready = m00_axis_tready;
    assign m00_axis_tstrb = 4'b1111;
    
    
endmodule
