
`timescale 1 ns / 1 ps

	module constrict_AXIS_v1_0 #
	(
		// Parameters of Axi Slave Bus Interface S00_AXIS
		parameter integer C_S00_AXIS_TDATA_WIDTH	= 64,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	(
		output wire  s00_axis_tready,
		input  wire  [15:0] VAL_SET,
		input  wire  [15:0] frame_size,
		//input wire  sel,
		input wire [C_S00_AXIS_TDATA_WIDTH-1 : 0] s00_axis_tdata,
		input wire [(C_S00_AXIS_TDATA_WIDTH/8)-1 : 0] s00_axis_tstrb,
		input wire  s00_axis_tlast,
		input wire  s00_axis_tvalid,

		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		input  wire  m00_axis_aresetn,
		input  wire  m00_axis_aclk,
		
		input  wire [15:0] divide_val,
		
		output reg interrupt_frame
	);
	localparam averag_val = 8;     // ���������� ����������� ��������
	//localparam divide_val = 16;     // �� ������� ��� �������� ������ �� ������ ��� ������� 1-����� �� 2, 2-����� �� 4,3-8 � ��
	//localparam frame_size = 4096;     // ���������� ������ �� ������
	
    wire [31:0] data_abs_1;     //wire [15:0] data_abs_1;
	wire [31:0] data_abs_2;     //wire [15:0] data_abs_2;
	reg [16:0]  data_sum;
	reg [7:0] frame = 0;
	reg [63:0] RAM [0:8191];
    reg [15:0] adr;
    reg [31:0] m00_axis_tdata_r;
	reg [15:0] cnt100;
	assign m00_axis_tdata = m00_axis_tdata_r;
	//assign frame_ILA = frame;
	assign m00_axis_tstrb = 4'hF;
	assign m00_axis_tlast  = (frame == averag_val) ? s00_axis_tlast  : 0;
	assign m00_axis_tvalid = (frame == averag_val) ? s00_axis_tvalid : 0;
	assign s00_axis_tready = m00_axis_tready;
	assign data_abs_1[29:0] = (s00_axis_tdata[29] == 1) ? -s00_axis_tdata[29:0]  : s00_axis_tdata[29:0];
	assign data_abs_2[29:0] = (s00_axis_tdata[61] == 1) ? -s00_axis_tdata[61:32] : s00_axis_tdata[61:32];
    //assign interrupt_frame = (frame == averag_val) ? 1 : 0;
	always @ (negedge m00_axis_aclk) begin 
        if(cnt100 == VAL_SET & frame == averag_val)
            interrupt_frame <= 1;
        else 
            interrupt_frame <= 0;
        
        if(m00_axis_aresetn)
            cnt100 <= cnt100 + 1;
        else
            cnt100 <= 0;
	end
	
	always @ (posedge m00_axis_aclk) begin 
	   if(m00_axis_tready & s00_axis_tvalid) begin
	       if(adr == frame_size - 1)
	           adr <= 0;
	       else
	           adr <= adr + 1;     // ����� ������ ������ ��������� ��� ������ ������������ ���������� ������ 
           if(frame == 0) begin    // ���� ������� ����� �� ��������� ������ � ������ 
               RAM[adr] <= data_abs_1 + data_abs_2;
           end
           else begin      
               if(frame == averag_val)
                   m00_axis_tdata_r <= (RAM[adr] >> divide_val);   // ���� �������������� averag_val �������� �� ���������� ������ �� ������ � ��������(�������) �� 2,4,8 � ��
               else
                   RAM[adr] <= RAM[adr] + data_abs_1 + data_abs_2; // ���� �� ������� ����� �� ��������� �������� ������ � ���������� (������������ �� ���������� ������) �������
	   end
	end
	else begin
	   adr <= 0;
	end
	end
    always @ (negedge m00_axis_aresetn) begin 
	   if(frame == averag_val) 
	       frame <= 0;
	   else 
	       frame <= frame + 1;
	end
endmodule
