
`timescale 1 ns / 1 ps

	module inout2_v1_0 	(
	
		output wire  out,
		input wire  in
	);
// Instantiation of Axi Bus Interface S00_AXI
	

	// Add user logic here
assign out = in;
	// User logic ends

	endmodule
