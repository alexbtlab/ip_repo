`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 30.04.2020 09:54:36
// Design Name: 
// Module Name: tb_sampleGenerator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module tb_Config_HMC769();
    reg         clk;
//    reg         start;
 

    wire [5:0] atten;
   
    reg        wPLL_LD_SDO;   
    reg        wSTART;
    wire       wATTEN;
    wire       wPLL_CEN;
    wire       wPLL_MOSI;
    wire       wPLL_SCK;
    wire       wPLL_SEN;
    wire       wPLL_TRIG;
    //reg        ws00_axi_aclk;
    reg        aresetn;
      
//initial begin
  //  AXI_En = 0;
  //  FrameSise =  16;
  //  S00_AXIS_tdata = 0;
  //  S00_AXIS_tlast =  0;
  //  S00_AXIS_tstrb =  0;
  //  S00_AXIS_tvalid = 0;
//end
initial begin
    clk = 0;
    aresetn = 1;
    forever #5 clk = ~clk;
end
//initial begin
  //  start = 0;

//initial begin
   // ResetN = 0;
   // #100 ResetN = 1;
//end
initial begin

    wPLL_LD_SDO = 0;
    wSTART = 0;
    #100 wSTART  = 0;
    #1000 wSTART  = 1;
    #1000 wSTART  = 0;
    #100 wSTART  = 0;
    #100 wSTART  = 0;
    #100 wSTART  = 0;
end

//initial begin
  //  M00_AXIS_tready = 0;
  //  #200 M00_AXIS_tready = 1;
  //  #2000 M00_AXIS_tready = 0;
  //  #200 M00_AXIS_tready = 1;
//end

design_1_wrapper DUT
 ( 
   .ATTEN(wATTEN),
   .PLL_CEN(wPLL_CEN),
   .PLL_LD_SDO(wPLL_LD_SDO),
   .PLL_MOSI(wPLL_MOSI),
   .PLL_SCK(wPLL_SCK),
   .PLL_SEN(wPLL_SEN),
   .PLL_TRIG(wPLL_TRIG),
   .START(wSTART),
   .s00_axi_aclk(clk),
   .s00_axi_aresetn(aresetn)
 );
endmodule
