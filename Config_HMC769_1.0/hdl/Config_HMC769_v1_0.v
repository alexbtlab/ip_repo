
`define UART_COMMAND_INIT_UART 3
`define UART_COMMAND_SW_TO_CONNECTOR 4
`define UART_COMMAND_SW_TO_ATTENUATOR 5
`define UART_COMMAND_ATTENUATOR_VALUE 6
`define UART_COMMAND_PAMP_ENABLE 7
`define UART_COMMAND_PAMP_DISABLE 8
`define UART_COMMAND_SW_IF1_AROUND 9
`define UART_COMMAND_SW_IF1_FORWARD 10
`define UART_COMMAND_SW_IF2_AROUND 11
`define UART_COMMAND_SW_IF2_FORWARD 12
`define UART_COMMAND_SW_IF3_AROUND 13
`define UART_COMMAND_SW_IF3_FORWARD 14
`define UART_COMMAND_IF_AMP1_ENABLE 15
`define UART_COMMAND_IF_AMP1_DISABLE 16
`define UART_COMMAND_IF_AMP2_ENABLE 17
`define UART_COMMAND_IF_AMP2_DISABLE 18
`define UART_COMMAND_READ_PLL 19
`define UART_COMMAND_GIVE_PLL_FIRST_BYTE 20
`define UART_COMMAND_GIVE_PLL_SECOND_BYTE 21
`define UART_COMMAND_GIVE_PLL_THIRD_BYTE 22
`define UART_COMMAND_WRITE_PLL 23
`define UART_COMMAND_WRITE_CLK 24
`define UART_COMMAND_GIVE_ADC_DATA 25
`define UART_COMMAND_STOP_SENDING_ADC_DATA 26

`define UART_RESPONCE_ACK 1
`define UART_RESPONCE_SW_TO_CONNECTOR 4
`define UART_RESPONCE_SW_TO_ATTENUATOR 5
`define UART_RESPONCE_ATTEN_VALUE_SET 6
`define UART_RESPONCE_PAMP_ENABLED 7
`define UART_RESPONCE_PAMP_DISABLED 8
`define UART_RESPONCE_SW_IF1_AROUND 9
`define UART_RESPONCE_SW_IF1_FORWARD 10
`define UART_RESPONCE_SW_IF2_AROUND 11
`define UART_RESPONCE_SW_IF2_FORWARD 12
`define UART_RESPONCE_SW_IF3_AROUND 13
`define UART_RESPONCE_SW_IF3_FORWARD 14
`define UART_RESPONCE_IF_AMP1_ENABLED 15
`define UART_RESPONCE_IF_AMP1_DISABLED 16
`define UART_RESPONCE_IF_AMP2_ENABLED 17
`define UART_RESPONCE_IF_AMP2_DISABLED 18
`define UART_RESPONCE_READ_PLL_READY 19

`timescale 1ns / 1ps

	module Config_HMC769_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 9
	)
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		
    output wire USER_LED,
    output wire PLL_SEN,
    output wire PLL_SCK,
    output wire PLL_MOSI,
    input wire PLL_LD_SDO,
    output wire PLL_CEN,
    output wire PLL_TRIG,
    output wire [5:0] ATTEN,
    input wire START	
	);
	
	
	
	
// Instantiation of Axi Bus Interface S00_AXI
	Config_HMC769_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) Config_HMC769_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready)
		
		
		///////////////////////
	);

	// Add user logic here


top_PLL_control top_PLL_control_i(

.clk_100MHz(s00_axi_aclk),
.user_led(USER_LED),
.pll_sen(PLL_SEN),
.pll_sck(PLL_SCK),
.pll_mosi(PLL_MOSI),
.pll_ld_sdo(PLL_LD_SDO),
.pll_cen(PLL_CEN),
.pll_trig(PLL_TRIG),
.atten(ATTEN),
.START(START)

);



	// User logic ends

	endmodule


//////////////////////////////////////////////////////////////////////////////////
// Company: BTLabs
// Engineer: Nechaev
// 
// Create Date: 23.07.2019 12:42:55
// Design Name: MRLS
// Module Name: mrls_top
// Project Name: 
// Target Devices: XC7A100
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////




