
`define UART_COMMAND_INIT_UART 3
`define UART_COMMAND_SW_TO_CONNECTOR 4
`define UART_COMMAND_SW_TO_ATTENUATOR 5
`define UART_COMMAND_ATTENUATOR_VALUE 6
`define UART_COMMAND_PAMP_ENABLE 7
`define UART_COMMAND_PAMP_DISABLE 8
`define UART_COMMAND_SW_IF1_AROUND 9
`define UART_COMMAND_SW_IF1_FORWARD 10
`define UART_COMMAND_SW_IF2_AROUND 11
`define UART_COMMAND_SW_IF2_FORWARD 12
`define UART_COMMAND_SW_IF3_AROUND 13
`define UART_COMMAND_SW_IF3_FORWARD 14
`define UART_COMMAND_IF_AMP1_ENABLE 15
`define UART_COMMAND_IF_AMP1_DISABLE 16
`define UART_COMMAND_IF_AMP2_ENABLE 17
`define UART_COMMAND_IF_AMP2_DISABLE 18
`define UART_COMMAND_READ_PLL 19
`define UART_COMMAND_GIVE_PLL_FIRST_BYTE 20
`define UART_COMMAND_GIVE_PLL_SECOND_BYTE 21
`define UART_COMMAND_GIVE_PLL_THIRD_BYTE 22
`define UART_COMMAND_WRITE_PLL 23
`define UART_COMMAND_WRITE_CLK 24
`define UART_COMMAND_GIVE_ADC_DATA 25
`define UART_COMMAND_STOP_SENDING_ADC_DATA 26

`define UART_RESPONCE_ACK 1
`define UART_RESPONCE_SW_TO_CONNECTOR 4
`define UART_RESPONCE_SW_TO_ATTENUATOR 5
`define UART_RESPONCE_ATTEN_VALUE_SET 6
`define UART_RESPONCE_PAMP_ENABLED 7
`define UART_RESPONCE_PAMP_DISABLED 8
`define UART_RESPONCE_SW_IF1_AROUND 9
`define UART_RESPONCE_SW_IF1_FORWARD 10
`define UART_RESPONCE_SW_IF2_AROUND 11
`define UART_RESPONCE_SW_IF2_FORWARD 12
`define UART_RESPONCE_SW_IF3_AROUND 13
`define UART_RESPONCE_SW_IF3_FORWARD 14
`define UART_RESPONCE_IF_AMP1_ENABLED 15
`define UART_RESPONCE_IF_AMP1_DISABLED 16
`define UART_RESPONCE_IF_AMP2_ENABLED 17
`define UART_RESPONCE_IF_AMP2_DISABLED 18
`define UART_RESPONCE_READ_PLL_READY 19

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: BTLabs
// Engineer: Nechaev
// 
// Create Date: 23.07.2019 12:42:55
// Design Name: MRLS
// Module Name: mrls_top
// Project Name: 
// Target Devices: XC7A100
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module top_PLL_control(
    input wire clk_100MHz,
    output wire user_led,
    output wire pll_sen,
    output wire pll_sck,
    output wire pll_mosi,
    input wire pll_ld_sdo,
    output wire pll_cen,
    output wire pll_trig,
    output wire [5:0] atten,
    input wire START
);

  /*
    wire uart_rx;
    wire uart_tx;
    //assign uart_rx = user_io_9;
    //assign user_io_10 = uart_tx; 
    reg is_clk_initialized_reg = 0;
    reg [31:0] clk_sync_counter_reg = 0;
    reg pll_cen_reg = 0;
    assign pll_cen = pll_cen_reg;
    assign sw_ls=1'b1;
    wire adc_clk, adc_dco, adc_da, adc_db, fpga_clk; 
    wire sys_clk_locked;
    wire uart_tx_busy;         
    reg pll_trig_reg = 0;
    assign pll_trig = pll_trig_reg;
    reg clk_sync_reg =0;
    assign clk_sync=clk_sync_reg;
    wire clk_spi;
    wire clk_adc;
    wire clk_adc_delay;       
    wire clk_100MHz_main;
    wire clk_100MHz_adc;
    
    */
        wire clk_uart;
        wire clk_pll_spi;
        wire clk_8MHz;    
        reg [7:0] pll_address_reg = 0;
        reg pll_read = 0;
        wire pll_sck_read;
        wire pll_mosi_read;
        wire pll_sen_read;
        wire [23:0] pll_read_data;
        wire pll_data_ready;
        reg [23:0] pll_write_data = 0;
        reg pll_write = 0;
        wire pll_sck_write;
        wire pll_mosi_write;
        wire pll_sen_write;
        reg [4:0] pll_write_address_reg = 0;
        wire sys_clk_locked;
        reg uart_byte_received_prev = 0;
        wire uart_byte_received;
        reg pll_data_ready_prev;
        reg is_counting = 0; 
        reg[7:0] uart_tx_data;
        reg uart_start_sending = 0;
        reg [3:0] received_byte_num = 0; 
        reg is_uart_receiving_atten_value = 0;
        reg [5:0] atten_reg = 6'h3F;
        reg is_uart_receiving_pll_data = 0;
        wire[7:0] uart_rx_data;
        reg is_uart_receiving_pll_read_address = 0;
        reg is_uart_receiving_pll_write_address = 0;
        reg is_uart_receiving_clk_write_address = 0;
        reg [7:0] clk_address_reg = 0;
        reg is_uart_receiving_clk_data = 0; 
        reg [7:0] clk_write_data_reg = 0;
        reg sw_ctrl_reg = 1'b0;
        reg sw_if1_reg = 1'b1;    
        reg sw_if2_reg = 1'b0;
        reg sw_if3_reg = 1'b0; 
        reg pamp_en_reg = 1'b0;
        reg if_amp1_en_reg=1'b0;
        reg if_amp2_en_reg=1'b0;
        reg is_adc_data_sending = 0;
        reg[3:0] cnt_uart = 0; 
        wire wSTART; 
        
//    assign if_amp2_stu_en = if_amp2_en_reg;            
//    assign if_amp1_stu_en = if_amp1_en_reg;      
 //   assign pamp_en = pamp_en_reg;
  //      assign sw_if1 = sw_if1_reg;
  //      assign sw_if2 = sw_if2_reg;
 //       assign sw_if3 = sw_if3_reg;
 //       assign sw_ctrl = sw_ctrl_reg;
        assign atten = atten_reg; 
        assign sys_clk_locked = 1;
        assign pll_sen = pll_sen_write | pll_sen_read;
        assign pll_sck = pll_sck_write | pll_sck_read;
        assign pll_mosi = pll_mosi_write | pll_mosi_read;
        
     clk_wiz_0 clk_wizard_main(      
      .clk_out1(clk_uart),      
      .clk_out2(clk_8MHz),
      .clk_out3(clk_pll_spi),
      .clk_in1(clk_100MHz)
                   
    );    
    pll_receive pll_receive(
        .clk(clk_pll_spi),
        .reg_address(pll_address_reg),
        .start(pll_read),
        .pll_clk(pll_sck_read),
        .pll_mosi(pll_mosi_read),
        .pll_miso(pll_ld_sdo),
        .pll_cs(pll_sen_read),
        .read_data(pll_read_data),
        .data_ready(pll_data_ready)        
    );
    pll_send pll_send(
        .clk(clk_pll_spi),
        .pll_data(pll_write_data),
        .start(wSTART),
        .pll_clk(pll_sck_write),
        .pll_mosi(pll_mosi_write),
        .pll_cs(pll_sen_write),
        .reg_address(pll_write_address_reg)          
    );
        
    
   // wire [7:0] uart_tx_adc_data;
   // 
   // wire uart_adc_start_sending;
   // reg adc_clk_valid=0;
   // reg [16:0] trig_tsweep_counter=0; 
   // reg [16:0] trig_tguard_counter=0;
   // reg clk_start_init = 0;
 
  
  //assign wSTART = START;
  //  always @(posedge clk_8MHz )
  //  begin
       
   //     pll_write_data <= 23'b1010101010101010101010;  
        //pll_write <= start;
   //     pll_write_address_reg <= 5'b10101;
   // end
   
   
  
  
  /*
     always @(posedge clk_8MHz )
     begin
        uart_byte_received_prev <= uart_byte_received;
        pll_data_ready_prev <= pll_data_ready;
        if ((pll_data_ready == 1)&(pll_data_ready_prev == 0))
        begin
            is_counting <=1;            
            uart_tx_data <= `UART_RESPONCE_READ_PLL_READY;
            if (uart_tx_data != `UART_RESPONCE_READ_PLL_READY) uart_start_sending <= 1;
            pll_read<=0;
        end
        if ((uart_byte_received == 1)&(uart_byte_received_prev == 0))
        begin
            is_counting <=1;
            if (received_byte_num>0)
            begin 
                if (is_uart_receiving_atten_value==1) // ���� �������� ��� �����������
                begin                    
                    atten_reg <= uart_rx_data;
                    uart_tx_data <= `UART_RESPONCE_ATTEN_VALUE_SET;
                    uart_start_sending <= 1;                    
                    received_byte_num <= 0;                      
                end
                else               
                if (is_uart_receiving_pll_read_address==1) // ���� ������ �������� ���� ��� ������
                begin
                    pll_address_reg <= uart_rx_data;                                      
                    is_uart_receiving_pll_read_address <= 0; 
                    pll_read <= 1;                                           
                    received_byte_num <= 0; 
                end
                else if (is_uart_receiving_pll_write_address==1) // ���� ������ �������� ���� ��� ������ � ����
                begin
                    pll_write_address_reg <= uart_rx_data;                                      
                    is_uart_receiving_pll_write_address <= 0;                                                          
                    received_byte_num <= 1;
                    is_uart_receiving_pll_data <= 1;
                    pll_write_data <= 0;
                    uart_tx_data <= `UART_RESPONCE_ACK;
                    uart_start_sending <= 1; 
                end
                else if (is_uart_receiving_clk_write_address==1) // ���� ������ �������� �������������� ��������� �������
                begin
                    clk_address_reg <= uart_rx_data;                                      
                    is_uart_receiving_clk_write_address <= 0;                                                          
                    received_byte_num <= 1;
                    is_uart_receiving_clk_data <= 1;
                    clk_write_data_reg <= 0;
                    uart_tx_data <= `UART_RESPONCE_ACK;
                    uart_start_sending <= 1; 
                end
                else if (is_uart_receiving_pll_data==1) // ���� �������� �������� ����
                begin
                    pll_write_data <= pll_write_data | (uart_rx_data << (8*(received_byte_num-1)));
                    uart_tx_data <= `UART_RESPONCE_ACK;
                    uart_start_sending <= 1;                    
                    if (received_byte_num < 3) received_byte_num <= received_byte_num + 1;                
                    else 
                    begin
                        received_byte_num <= 0;
                        is_uart_receiving_pll_data <= 0;
                        pll_write <= 1;                                                                       
                    end
                end
            end            
            else if (uart_rx_data == `UART_COMMAND_INIT_UART) 
            begin
                uart_tx_data=`UART_COMMAND_INIT_UART;
                uart_start_sending <= 1;                
            end            
            else if (uart_rx_data == `UART_COMMAND_SW_TO_CONNECTOR) 
            begin
                sw_ctrl_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_SW_TO_CONNECTOR;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_TO_ATTENUATOR) 
            begin
                sw_ctrl_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_SW_TO_ATTENUATOR;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_PAMP_ENABLE) 
            begin
                pamp_en_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_PAMP_ENABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_PAMP_DISABLE) 
            begin
                pamp_en_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_PAMP_DISABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF1_AROUND) 
            begin
                sw_if1_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_SW_IF1_AROUND;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF1_FORWARD) 
            begin
                sw_if1_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_SW_IF1_FORWARD;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF2_AROUND) 
            begin
                sw_if2_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_SW_IF2_AROUND;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF2_FORWARD) 
            begin
                sw_if2_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_SW_IF2_FORWARD;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF3_AROUND) 
            begin
                sw_if3_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_SW_IF3_AROUND;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_IF3_FORWARD) 
            begin
                sw_if3_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_SW_IF3_FORWARD;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_IF_AMP1_ENABLE) 
            begin
                if_amp1_en_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_IF_AMP1_ENABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_IF_AMP1_DISABLE) 
            begin
                if_amp1_en_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_IF_AMP1_DISABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_IF_AMP2_ENABLE) 
            begin
                if_amp2_en_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_IF_AMP2_ENABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_IF_AMP2_DISABLE) 
            begin
                if_amp2_en_reg <= 1'b0;
                uart_tx_data=`UART_RESPONCE_IF_AMP2_DISABLED;
                uart_start_sending <= 1;                
            end
            else if (uart_rx_data == `UART_COMMAND_SW_TO_ATTENUATOR) 
            begin
                sw_ctrl_reg <= 1'b1;
                uart_tx_data=`UART_RESPONCE_SW_TO_ATTENUATOR;
                uart_start_sending <= 1;                
            end
            else  if (uart_rx_data == `UART_COMMAND_ATTENUATOR_VALUE)
            begin
                uart_tx_data=`UART_RESPONCE_ACK;
                uart_start_sending <= 1;                
                is_uart_receiving_atten_value <= 1;
                received_byte_num <= 1;                                
            end
            else  if (uart_rx_data == `UART_COMMAND_READ_PLL)
            begin                
                uart_tx_data<=`UART_RESPONCE_ACK;
                uart_start_sending <= 1;
                received_byte_num <= 1;
                is_uart_receiving_pll_read_address <= 1;                                                
            end
            else if (uart_rx_data == `UART_COMMAND_GIVE_PLL_FIRST_BYTE)
            begin
                uart_tx_data<=pll_read_data[7:0];
                uart_start_sending <= 1;                
            end 
            else if (uart_rx_data == `UART_COMMAND_GIVE_PLL_SECOND_BYTE)
            begin
                uart_tx_data<=pll_read_data[15:8];
                uart_start_sending <= 1;                
            end 
            else if (uart_rx_data == `UART_COMMAND_GIVE_PLL_THIRD_BYTE)
            begin
                uart_tx_data<=pll_read_data[23:16];
                uart_start_sending <= 1;                
            end 
            else  if (uart_rx_data == `UART_COMMAND_WRITE_PLL)
            begin                
                uart_tx_data<=`UART_RESPONCE_ACK;
                uart_start_sending <= 1;
                received_byte_num <= 1;
                is_uart_receiving_pll_write_address <= 1; 
                pll_write <= 0;                                               
            end                      
           
             
            //else  if (uart_rx_data == `UART_COMMAND_WRITE_CLK)
            //begin                
            //    uart_tx_data<=`UART_RESPONCE_ACK;
            //    uart_start_sending <= 1;
            //    received_byte_num <= 1;
            //    is_uart_receiving_clk_write_address <= 1; 
            //   clk_write <= 0;                                                             
            //end                   
        end             
        if (is_counting) cnt_uart <= cnt_uart+1;
        if (cnt_uart == 5) 
        begin
             cnt_uart <= 0;
             is_counting <= 0;             
             uart_start_sending <= 0;
             pll_read <= 0;                         
        end
     end 
    */ 
     
     //triggering
   /*  
     always @(posedge clk_8MHz & sys_clk_locked)
     begin
        if ((trig_tsweep_counter<8000)&(trig_tguard_counter==0))
        begin 
            trig_tsweep_counter <= trig_tsweep_counter + 1;
            pll_trig_reg <= 0;
        end
        else if ((trig_tsweep_counter==8000)&(trig_tguard_counter==0))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter<100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 0;
            trig_tguard_counter<=trig_tguard_counter+1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter==100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=0;                        
        end
     end */
     
     
        
/*      always @(posedge clk_8MHz & sys_clk_locked)
     begin
        
        if (is_clk_initialized_reg==0)
        begin
            if (clk_sync_counter_reg<100000) //��� 12,5 ��
            begin
            clk_sync_counter_reg <= clk_sync_counter_reg + 1;                
            end
            else if (clk_sync_counter_reg==100000)
            begin
                clk_start_init <= 1;
                clk_sync_reg <= 1;
                clk_sync_counter_reg <= clk_sync_counter_reg + 1; 
            end
            else if (clk_sync_counter_reg==160000)
            begin
                adc_clk_valid <= 1;
                clk_sync_counter_reg <= clk_sync_counter_reg + 1;
            end
            else if (clk_sync_counter_reg<180000) //������ clk_sync = 1 � ������� 5 ��
            begin
                clk_sync_counter_reg <= clk_sync_counter_reg + 1;                             
            end            
            else
            begin
                clk_start_init <= 0;
                clk_sync_reg <= 0;
                is_clk_initialized_reg<=1;
            end             
        end
     end
     
    reg user_led_reg = 1;
    assign user_led = user_led_reg; */
    

    
endmodule

