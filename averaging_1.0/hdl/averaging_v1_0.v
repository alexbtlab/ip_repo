
`timescale 1 ns / 1 ps

	module averaging_v1_0 
	(
		input wire  clk_1,
		input wire [15:0] data_1,
		
		input wire  clk_2,
		input wire [15:0] data_2,

		input wire [15:0] frame_size,		

		input wire  reset,
 
		output wire  clk_out_1,
		output wire [15:0] data_out_1,

		output wire  clk_out_2,
		output wire [15:0] data_out_2,

		output wire [3:0] frame_ila,
		output wire [15:0] cnt_adr_ila,
        output wire [15:0] cnt_adr_read_ila,

		output wire  clk_1_inv_ila,
		
		output wire  we_ila
		);
		
    wire we;
    assign we_ila = we;

	reg [3:0] frame = 0;
	assign frame_ila = frame;

	wire clk_1_inv;
	assign clk_1_inv = ~clk_1;
	assign clk_1_inv_ila = clk_1_inv;

	reg [15:0] data_out_1_reg = 0;
	assign data_out_1 = data_out_1_reg;

    reg [31:0] memory_array [0:4095]; 

	reg [15:0] cnt_adr = 0;
	assign cnt_adr_ila = cnt_adr;
	reg [15:0] cnt_adr_read = 0;
	assign cnt_adr_read_ila = cnt_adr_read;

	wire [15:0] data_1_reg;
	
	assign data_1_reg = data_1;
	
//////////////////////////////////////////////////////////////////////////////////////
always @ (posedge clk_1_inv) begin 
	if(reset) begin	

		if(we == 1) begin 
		  data_out_1_reg <= 0;
		  if (frame == 0)
		     memory_array[cnt_adr] <= data_1_reg;
		  else    
			 memory_array[cnt_adr] <= memory_array[cnt_adr] + data_1_reg;
		end 
		else begin
		    data_out_1_reg <= (memory_array[cnt_adr] >> 3);
        end
    end
end
//////////////////////////////////////////////////////////////////////////////////////
always @ (posedge clk_1) begin 
	if(reset) begin	
	
		if(cnt_adr == frame_size)
		      cnt_adr <= 0;
		else
		      cnt_adr <= cnt_adr + 1;
    
    end
    else begin
        cnt_adr <= 0;
    end
end
//////////////////////////////////////////////////////////////////////////////////////
always @ (posedge reset) begin
	if(frame == 8) begin 
		frame <= 0;
	end 
	else begin
		frame <= frame + 1;
    end 
end

    assign we = (frame == 8) ? 0 : 1;
	assign data_out_2 = 16'b1111111111111111;
	assign clk_out_2  = 1'b1;
	assign clk_out_1  = clk_1;


endmodule
