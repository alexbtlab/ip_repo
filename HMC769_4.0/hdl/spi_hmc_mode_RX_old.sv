
module spi_hmc_mode_RX(
     input [15:0] val_set_ready,
        input start,
        input [5:0] adr,
        output [23:0]data,
        input clk,
        output mosi,
        output sck,
        input miso,
        output cs,
        output ready 
    );
    
    reg ready_r;
    assign ready = ready_r;
    reg [23:0] data_r;
    assign data = data_r;
    reg start_prev = 0;
    reg enable_cnt = 0;
    reg [15:0] cnt;
    reg mosi_r;
    reg cs_r = 0;
    reg enable_sck;
    assign sck =  clk &  enable_sck;
    assign mosi = mosi_r;
    assign cs = cs_r;
    
     always @ (negedge clk) begin

            if(start == 1 & start_prev == 0) begin
                start_prev <= 1;
                enable_cnt <= 1;
                cs_r <= 1;
                mosi_r <= 0;
            end
            
            if(enable_cnt & cnt != 8'h21)     cnt <= cnt + 1;
            else                              cnt <= 0;
                
            if(cnt > 0 & cnt < 32)            enable_sck <= 1;
            else                              enable_sck <= 0;
    
            if(cnt == 1 )                     mosi_r <= 1;          //read
            if(cnt > 1 & cnt < 8)             mosi_r <= adr[7-cnt];
            if(cnt > 7 & cnt < 35)            mosi_r <= 0;
            
            if(cnt == 8'h21) begin
                enable_cnt  <= 0;
                cs_r        <= 0;
                start_prev  <= 0;
                mosi_r      <= 0;
            end
            
            if(cnt > 6 & cnt < 34) begin
                data_r[32-cnt] <= miso;
            end
            if(cnt == val_set_ready)   ready_r <= 1;
            else                       ready_r <= 0;
            
    end
    
endmodule
