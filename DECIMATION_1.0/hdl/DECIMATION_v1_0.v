`timescale 1 ns / 1 ps

	module DECIMATION_v1_0 #
	(
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	)
	( 
        input [15:0] FrameSize,
		input  wire  data_clk,
		input wire [15:0]   data_in_IF1,
		input wire [15:0]   data_in_IF2,
		input  wire  m00_axis_aresetn,
		output wire m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0]     m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire m00_axis_tlast,
		input  wire  m00_axis_tready,
		input  wire  m00_axis_aclk
		
	);
	reg cnt_10 = 0;
	always @ (negedge data_clk) begin
	   if(cnt_10)
	       cnt_10 <= 0;
	   else
	       cnt_10 <= 1;
    end
	
	
	
//	assign m00_axis_tstrb = 4'b1111;
//	reg i_write;
//	wire [31:0] data_in_ram;
//	assign data_in_ram = (i_write) ? {data_in_IF2, data_in_IF1} : 0;
//	reg m00_axis_tvalid_w = 0;
//	assign m00_axis_tvalid = m00_axis_tvalid_w;
//	reg m00_axis_tlast_w = 0;
//	assign m00_axis_tlast = m00_axis_tlast_w;
//	reg [31:0] m00_axis_tdata_w;
//	assign  m00_axis_tdata = m00_axis_tdata_w;
//    reg [12:0] i_addr;
//    wire i_write_w = 1;
//    assign i_write_w = i_write;
//    reg [31:0] memory_array [0:8191];
//    wire [12:0] cnt_clk_10;
//    reg [15:0] cnt_clk_10_w = 0;
//    assign cnt_clk_10 = cnt_clk_10_w;
//    wire clk_W10_R100;
//    assign clk_W10_R100 = (i_write) ? data_clk : m00_axis_aclk;
    
//    always @ (posedge clk_W10_R100) begin
//    if(m00_axis_aresetn & m00_axis_tready) begin
//        if(i_write) begin
//             m00_axis_tvalid_w <= 0;
//             m00_axis_tlast_w <= 0;
                    
//                    if(cnt_clk_10 != FrameSize) begin
//                        cnt_clk_10_w <= cnt_clk_10_w + 1;
//                        memory_array[i_addr] <= data_in_ram;
//                    end 
//                    else begin
//                        cnt_clk_10_w <= 0;
//                        i_write <= 0;
//                    end
//        end
//        else begin
//            if(cnt_clk_10 <= FrameSize) begin
//                cnt_clk_10_w <= cnt_clk_10_w + 1;
//                m00_axis_tdata_w <= memory_array[i_addr];                
//            end
//            else begin
//                m00_axis_tdata_w <= 0;               
//            end 
//            if((cnt_clk_10_w > 0) & (cnt_clk_10_w <= FrameSize-1))  
//                m00_axis_tvalid_w <= 1; 
//            else 
//                m00_axis_tvalid_w <= 0;
            
//            if((cnt_clk_10_w == FrameSize-1))  
//                m00_axis_tlast_w <= 1;            
//            else 
//                m00_axis_tlast_w <= 0;  
//        end
//    end
//    else begin
//        i_write <= 1;
//        cnt_clk_10_w <= 0;
//        m00_axis_tvalid_w <= 0;
//        m00_axis_tlast_w <= 0;
//    end       
//    end
    
//    always @ (negedge clk_W10_R100) begin
//        if(m00_axis_aresetn & m00_axis_tready) 
//            i_addr <= cnt_clk_10;
//        else
//            i_addr <= 0;
//    end
	endmodule
