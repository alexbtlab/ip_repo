
`timescale 1 ns / 1 ps

	module sampleGenerator_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXIS
		//parameter integer C_S00_AXIS_TDATA_WIDTH	= 32,

		// Parameters of Axi Master Bus Interface M00_AXIS
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32
	) 
	(
		// Users to add ports here
		input [15:0] FrameSize,
		//input AXI_En,
		//input En,
		// User ports ends
		// Do not modify the ports beyond this line



		// Ports of Axi Master Bus Interface M00_AXIS
		input wire  m00_axis_aclk,
		input wire  m00_axis_aclk_200,
		input wire  m00_axis_aresetn,
		output wire  m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire  m00_axis_tlast,
		input wire  m00_axis_tready,
		
		output wire [15:0]  Word_Counter,
		input wire [31:0]   data_from_ram,
		input wire          en_AXIclk,
		input wire          main_clk_100MHz,
		input wire [5:0]    inc_adr,
		output wire         clk_read_ram,
		output wire [31:0]  adr_read_ram	
	);
		wire  data_to_transmit;
		wire  m00_axis_aclk_inv;
		wire  m00_axis_tvalidW;
		wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdataW;
		wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrbW;
		wire  m00_axis_tlastW;
		//wire  s00_axis_treadyW;
		wire  m00_axis_treadyW;
// Instantiation of Axi Bus Interface S00_AXIS

   reg  m00_axis_aclk_reg;
   wire  off_clk = 0;   

assign m00_axis_aclk_inv = ~m00_axis_aclk;     
//assign m00_axis_aclk_reg = (en_AXIclk == 1) ? off_clk : m00_axis_aclk_inv;
//assign m00_axis_aclk_reg =  m00_axis_aclk ;   

// Instantiation of Axi Bus Interface M00_AXIS
	sampleGeneraror_v1_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH(C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT(C_M00_AXIS_START_COUNT)
	) samplGeneraror_v1_0_M00_AXIS_inst (
		.FrameSize(FrameSize),
		.En(1'b1),
		.inc_adr(inc_adr),
		.en_AXIclk(en_AXIclk),
		.main_clk_100MHz(main_clk_100MHz),
		.Word_Counter(Word_Counter),
		.M_AXIS_ACLK(m00_axis_aclk),
		.M_AXIS_ACLK_200(m00_axis_aclk_200),
		.M_AXIS_ARESETN(m00_axis_aresetn),
		.M_AXIS_TVALID(m00_axis_tvalidW),
		.data_to_transmit(data_from_ram),
		.M_AXIS_TDATA(m00_axis_tdata),
		.M_AXIS_TSTRB(m00_axis_tstrbW),
		.M_AXIS_TLAST(m00_axis_tlastW),
		.M_AXIS_TREADY(!en_AXIclk)
	);





	// Add user logic here
	assign m00_axis_tdata =  m00_axis_tdataW;
	assign m00_axis_tstrb =  m00_axis_tstrbW;
	assign m00_axis_tlast =  m00_axis_tlastW;
	assign m00_axis_tvalid =  m00_axis_tvalidW;
	//assign s00_axis_tready = m00_axis_tready;
	// User logic ends

	endmodule
