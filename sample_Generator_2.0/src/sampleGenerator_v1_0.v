
`timescale 1 ns / 1 ps

	module sample_Generator_v2_0 #
	( 
		parameter integer C_M00_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M00_AXIS_START_COUNT	= 32,
		parameter  ADDR_WIDTH = 16, 
		parameter  DATA_WIDTH = 32, 
		parameter  DEPTH = 8192
	)  
	(
		input [15:0] FrameSize,
		input  wire  data_ready,
		input  wire  m00_axis_aresetn,
		output wire m00_axis_tvalid,
		output wire [C_M00_AXIS_TDATA_WIDTH-1 : 0]     m00_axis_tdata,
		output wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrb,
		output wire m00_axis_tlast,
		input  wire  m00_axis_tready,
		input  wire  m00_axis_aclk,
		
		input wire [31:0]   data_in,
		
		output wire [15 : 0]wordCounter_ila,
		output wire RW_ila,
		output wire clk_100MHz_AXI_ila,
		output wire [15:0]adr_ram_w_ila,
        output wire [31:0] data_from_ram_ila,
		output wire clk_write_read_w_ila
		
	);
	    wire clk_100MHz_AXI;
		wire en_AXIclk;
	    wire [31:0] data_from_ram;
		wire  data_to_transmit;
		wire  m00_axis_aclk_inv;
		wire  m00_axis_tvalidW;
		wire [C_M00_AXIS_TDATA_WIDTH-1 : 0] m00_axis_tdataW;
		wire [(C_M00_AXIS_TDATA_WIDTH/8)-1 : 0] m00_axis_tstrbW;
		wire  m00_axis_tlastW;
		wire  m00_axis_treadyW; 
		
		assign RW_ila = en_AXIclk;
		assign clk_100MHz_AXI_ila = clk_100MHz_AXI;
		assign data_from_ram_ila = data_from_ram;
		
	sampleGeneraror_v2_0_M00_AXIS # ( 
		.C_M_AXIS_TDATA_WIDTH (C_M00_AXIS_TDATA_WIDTH),
		.C_M_START_COUNT      (C_M00_AXIS_START_COUNT)
	) samplGeneraror_v2_0_M00_AXIS_inst (
		.FrameSize        (FrameSize),
		.En               (1'b1),
		.inc_adr          (1'b1),
		.en_AXIclk        (en_AXIclk),
		.main_clk_100MHz  (m00_axis_aclk),
		.M_AXIS_ACLK      (clk_100MHz_AXI),
		.M_AXIS_ARESETN   (m00_axis_aresetn),
		.M_AXIS_TVALID    (m00_axis_tvalidW),
		.data_to_transmit (data_from_ram),
		.M_AXIS_TDATA     (m00_axis_tdata),
		.M_AXIS_TSTRB     (m00_axis_tstrbW),
		.M_AXIS_TLAST     (m00_axis_tlastW),
		.M_AXIS_TREADY    (!en_AXIclk),
		
		.wordCounter_ila(wordCounter_ila)
	);
	
	wire clk_write_read_w;
	
	wire [15:0] adr_ram_w;
	
	assign clk_write_read_w_ila = clk_write_read_w;
	assign adr_ram_w_ila = adr_ram_w;
	ram  ram_inst(
    .i_clk      (clk_write_read_w),
    .i_addr     (adr_ram_w), 
    .i_write    (en_AXIclk),
    .i_data     (data_in),
    .o_data     (data_from_ram) 
    );

    wire main_clk_200_w;
    
    ram_control ram_control_inst(
		.main_clk                 (m00_axis_aclk),
        .main_clk_200             (main_clk_200_w),
		.select_clk_read_100_200  (1'b0),
        .reset                    (m00_axis_aresetn),
        .inc_adr                  (6'b000001),
		.data_ready               (data_ready),
		.FrameSize                (FrameSize),
		.clk_write_read           (clk_write_read_w),
		.adr_ram                  (adr_ram_w),
		.WR                       (en_AXIclk),
		.clk_100MHz_AXI           (clk_100MHz_AXI)
	);
	assign m00_axis_tdata =  m00_axis_tdataW;
	assign m00_axis_tstrb =  m00_axis_tstrbW;
	assign m00_axis_tlast =  m00_axis_tlastW;
	assign m00_axis_tvalid = m00_axis_tvalidW;

endmodule
