module ram 
    (
        input wire i_clk,
        input wire [15:0] i_addr, 
        input wire i_write,
        input wire [31:0] i_data,
        output reg [31:0] o_data 
    );

    reg [31:0] memory_array [0:8191]; 

    always @ (posedge i_clk)
    begin
        if(i_write) begin
            memory_array[i_addr] <= i_data;
        end
        else begin
            o_data <= memory_array[i_addr];
        end     
    end
endmodule