
`timescale 1 ns / 1 ps

module ram_control(
		input wire   main_clk,
        input wire   main_clk_200,
		input wire   select_clk_read_100_200,
        input wire   reset,
        input wire   [5:0] inc_adr,
		input wire   data_ready,
		input wire   [15:0] FrameSize,
		output wire  clk_write_read,
		output reg   [15:0]  adr_ram,
		output wire  WR,
		output wire  clk_100MHz_AXI
	);
	reg  [15:0] count_clk_write_read = 0;
    reg  state_WR   = 1;
    reg  state_STOP = 0;
    wire clk_write_read_w;
    wire main_clk_100_200;
    assign main_clk_100_200 = (select_clk_read_100_200) ? main_clk_200 : main_clk;
    assign clk_write_read_w = (state_WR)                ? data_ready : ~main_clk_100_200;
    assign clk_write_read   = (~state_STOP & reset)     ? clk_write_read_w : 0;
    assign WR               = (state_WR)                ? 1 : 0;
    assign clk_100MHz_AXI   = (state_WR |  (count_clk_write_read == 0) | (count_clk_write_read ==  1) | (state_STOP == 1)) ? 0 : main_clk;

/*-----------------------------------------------------------------------------------------------------*/
always @ (negedge clk_write_read_w) begin
    if(!reset) begin
        adr_ram  =  0;
    end
    else begin
        adr_ram <= count_clk_write_read; 
    end             
end
/*-----------------------------------------------------------------------------------------------------*/
always @ (posedge clk_write_read_w) begin
    if(!reset) begin
        count_clk_write_read <= 0;
        state_WR = 1;
        state_STOP <= 0;
    end
    else begin
        if(state_WR) begin 
            if(count_clk_write_read == (FrameSize)) begin
                count_clk_write_read <= 0;
                state_WR = 0;  
            end
            else begin
                count_clk_write_read <= count_clk_write_read + 1;
            end 
        end
        else if(!state_WR) begin 
            if(count_clk_write_read >= (FrameSize + 1)) begin
                count_clk_write_read <= 0;
                state_STOP <= 1;
            end
            else begin
                count_clk_write_read <= count_clk_write_read + 1;
            end 
        end                      
    end    
end
endmodule
