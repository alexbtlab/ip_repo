
`timescale 1 ns / 1 ps

module sampleGeneraror_v2_0_M00_AXIS #
	(
		// Width of S_AXIS address bus. The slave accepts the read and write addresses of width C_M_AXIS_TDATA_WIDTH.
		parameter integer C_M_AXIS_TDATA_WIDTH	= 32,
		// Start count is the number of clock cycles the master will wait before initiating/issuing any transaction.
		parameter integer C_M_START_COUNT	= 32
	)
	(
		// Users to add ports here
		input wire [15:0] FrameSize,
		input wire  En,
		input wire  [5:0] inc_adr,
		// User ports ends
		input wire  en_AXIclk,
		// Do not modify the ports beyond this line
        input wire  main_clk_100MHz,
		// Global ports
		input wire  M_AXIS_ACLK,
		// 
		input wire  M_AXIS_ARESETN,
		// Master Stream Ports. TVALID indicates that the master is driving a valid transfer, A transfer takes place when both TVALID and TREADY are asserted. 
		output wire  M_AXIS_TVALID,
		input wire [C_M_AXIS_TDATA_WIDTH-1 : 0] data_to_transmit,
		// TDATA is the primary payload that is used to provide the data that is passing across the interface from the master.
		output reg [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA,
		// TSTRB is the byte qualifier that indicates whether the content of the associated byte of TDATA is processed as a data byte or a position byte.
		output wire [(C_M_AXIS_TDATA_WIDTH/8)-1 : 0] M_AXIS_TSTRB,
		// TLAST indicates the boundary of a packet.
		output wire  M_AXIS_TLAST,
		// TREADY indicates that the slave can accept a transfer in the current cycle.
		input wire  M_AXIS_TREADY,
		
		output wire [15 : 0] wordCounter_ila
	);
	

reg [C_M_AXIS_TDATA_WIDTH-1 : 0] counterR;
wire en_AXIclk_w;
assign en_AXIclk_w = en_AXIclk;
reg M_AXIS_TVALID_reg;
assign M_AXIS_TVALID = M_AXIS_TVALID_reg;
reg M_AXIS_TLAST_reg;
assign M_AXIS_TLAST = M_AXIS_TLAST_reg;
assign M_AXIS_TSTRB = 4'b1111;
            
reg [15:0] wordCounter = 0;
assign wordCounter_ila = wordCounter;
/*--------------------------------------------------------------------------------*/
reg flag_stop = 0;

always @(posedge main_clk_100MHz) begin

		if( ! M_AXIS_ARESETN ) begin
			counterR <= 0;
		end	
		else begin
			if(!en_AXIclk & flag_stop == 0) begin 
			     
			     if((wordCounter == (FrameSize))) begin
			         M_AXIS_TLAST_reg <= 1;
			     end
			     else begin 
			         M_AXIS_TLAST_reg <= 0;
			     end
			     
			     
			     if((wordCounter > 1)&(wordCounter <= (FrameSize))) begin
			         M_AXIS_TVALID_reg <= 1;
			     end
			     else begin 
			         M_AXIS_TVALID_reg <= 0;
			     end
			end 
			else begin
			     M_AXIS_TVALID_reg <= 0; 
			     M_AXIS_TLAST_reg <= 0;
			end	
		end
end
/*--------------------------------------------------------------------------------*/
always @(posedge M_AXIS_ACLK) begin
		if(M_AXIS_TREADY) begin
		    if ((wordCounter != 0) || (wordCounter != 1) || (wordCounter != 2)) begin
			    M_AXIS_TDATA <= data_to_transmit;   
			end
	    end
	    else begin
	       M_AXIS_TDATA <= 0;
	    end
end

always @(negedge main_clk_100MHz) begin
		if(!M_AXIS_ARESETN) begin
			flag_stop <= 0;
		end
		if(M_AXIS_TREADY) begin			
			if (wordCounter >= (FrameSize)) begin
				wordCounter <= 16'h0000; 
				flag_stop <= 1;   
			end
			else begin	
				if(flag_stop != 1)
					wordCounter <= wordCounter + inc_adr;
		    end
	    end
	    else begin
	       wordCounter <= 16'h0000;
	    end
end		

endmodule
