`timescale 1ns / 1ps


interface diff;
  logic p, n;      // Indicates if slave is ready to accept data
  modport slave  (input p, n);
  modport master (output p, n);
endinterface


module LTC2387_18(
    //input wire main_clk,
    input wire adc_clk_valid,
    //input wire fpga_clk,
    //input wire adc_dco,
    //input wire adc_da,
    //input wire adc_db,    
    //output wire adc_clk,
    //output wire [17:0] adc_data,
    //output wire data_ready,
    
    //output wire adc_clk,
    //output wire adc_da,
    //output wire adc_db,
    //output wire adc_dco,
    //output wire adc_dco_del,
    //output wire fpga_clk,
    output wire sync,
    output wire clk_data,
    output wire clk_data_inv, 
    output wire [7:0] cnt100_ila,
    output wire [17:0] ADC_DATA,
     output wire [17:0] ADC_DATA2,
       
    diff.slave adc_dax,
    diff.slave adc_dbx,
    diff.slave adc_dcox,
    diff.slave fpga_clkx,
    diff.master adc_clkx
    );
   wire sync_w = 0; 
   assign sync = sync_w;
   assign clk_data = (cnt100 == 0) ? 1:0; 
   assign clk_data_inv = ~clk_data; 

   reg [17:0] ADC_DATA2_reg;
   assign ADC_DATA2 = ADC_DATA2_reg;

   wire fpga_clk; 
   wire adc_da;
   wire adc_db;
   wire adc_clk;
   wire clk_100;
   wire clk_200; 
   wire clk_locked;
    
    wire adc_clk_delayed;   
    wire adc_dco_delayed;    


    clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_100),
      .clk_out2(clk_200),                 
      .clk_in1(fpga_clk & adc_clk_valid),
      .locked(clk_locked)                      
    );

    OBUFDS #( 
        .IOSTANDARD("DEFAULT"), // Specify the output I/O standard 
        .SLEW("SLOW") // Specify the output slew rate 
    ) OBUFDS_adc_clk ( 
        .O(adc_clkx.p), // Diff_p output (connect directly to top-level port) 
        .OB(adc_clkx.n), // Diff_n output (connect directly to top-level port) 
        .I(adc_clk) // Buffer input 
    ); 
    
    wire adc_dco;
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_dco (
        .O(adc_dco), // Buffer output
        .I(adc_dcox.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dcox.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_da (
        .O(adc_da), // Buffer output
        .I(adc_dax.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dax.n) // Diff_n buffer input (connect directly to top-level port)
    );

     IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_adc_db (
        .O(adc_db), // Buffer output
        .I(adc_dbx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(adc_dbx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    IBUFDS #(
        .DIFF_TERM("TRUE"), // Differential Termination
        .IBUF_LOW_PWR("TRUE"), // Low power="TRUE", Highest performance="FALSE"
        .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_fpga_clk (
        .O(fpga_clk), // Buffer output
        .I(fpga_clkx.p), // Diff_p buffer input (connect directly to top-level port)
        .IB(fpga_clkx.n) // Diff_n buffer input (connect directly to top-level port)
    );
    
    reg [7:0] cnt100 = 0;
    assign cnt100_ila  = cnt100;

    wire adc_clk;
    wire adc_clk_inv;

    assign adc_clk = (cnt100 > 0) ? clk_100 : 0;
    assign adc_clk_inv = ~adc_clk;

    reg [7:0] cnt_pos_dco = 0;
    
    reg bit_17 = 0;
    reg bit_16 = 0;
    reg bit_15 = 0;
    reg bit_14 = 0;
    reg bit_13 = 0;
    reg bit_12 = 0;
    reg bit_11 = 0;
    reg bit_10 = 0;
    reg bit_9 = 0;
    reg bit_8 = 0;
    reg bit_7 = 0;
    reg bit_6 = 0;
    reg bit_5 = 0;
    reg bit_4 = 0;
    reg bit_3 = 0;
    reg bit_2 = 0;
    reg bit_1 = 0;
    reg bit_0 = 0;

 	reg [17:0] ADC_DATA_w;
 	assign ADC_DATA[17:0] = ADC_DATA_w[17:0];

 	assign     ADC_DATA_w[0] = bit_0;
 	assign     ADC_DATA_w[1] = bit_1;
 	assign     ADC_DATA_w[2] = bit_2;
 	assign     ADC_DATA_w[3] = bit_3;
 	assign     ADC_DATA_w[4] = bit_4;
 	assign     ADC_DATA_w[5] = bit_5;
 	assign     ADC_DATA_w[6] = bit_6;
 	assign     ADC_DATA_w[7] = bit_7;
 	assign     ADC_DATA_w[8] = bit_8;
 	assign     ADC_DATA_w[9] = bit_9;
 	assign     ADC_DATA_w[10] = bit_10;
 	assign     ADC_DATA_w[11] = bit_11;
 	assign     ADC_DATA_w[12] = bit_12;
 	assign     ADC_DATA_w[13] = bit_13;
 	assign     ADC_DATA_w[14] = bit_14;
 	assign     ADC_DATA_w[15] = bit_15;
 	assign     ADC_DATA_w[16] = bit_16;
 	assign     ADC_DATA_w[17] = bit_17;

 reg reset_cnt = 0;      
 /*----------------------------------------------------------------------------------------------*/
    always @ (negedge clk_data) begin 
         ADC_DATA2_reg <= ADC_DATA;
    end
/*----------------------------------------------------------------------------------------------*/
    always @ (negedge clk_100) begin 
        if(~fpga_clk) begin
            cnt100 <= cnt100 + 1;
            if(cnt100 == 0) begin 
            	reset_cnt <= 1;
           	end 
           	else begin
           		reset_cnt <= 0;	
           	end
        end 
        else begin
        	reset_cnt <= 1;
            cnt100 <= 0;
        end       
    end
/*----------------------------------------------------------------------------------------------*/                 
  always @ (posedge adc_clk_inv) begin 
       
        if((cnt_pos_dco == 0) & (reset_cnt == 1) & (cnt_neg_dco == 0)) begin
        	cnt_pos_dco <= 1;
            bit_17 <= adc_da;
            bit_16 <= adc_db;
        end
        else if((cnt_pos_dco == 1) & (cnt_neg_dco == 1)) begin
            cnt_pos_dco <= 2;
            bit_13 <= adc_da;
            bit_12 <= adc_db; 
        end
        else if((cnt_pos_dco == 2) & (cnt_neg_dco == 2)) begin
            cnt_pos_dco <= 3;
            bit_9 <= adc_da;
            bit_8 <= adc_db; 
        end
        else if((cnt_pos_dco == 3)  & (cnt_neg_dco == 3)) begin
            cnt_pos_dco <= 4;
            bit_5 <= adc_da;
            bit_4 <= adc_db;
        end 
        else if((cnt_pos_dco == 4)  & (cnt_neg_dco == 4)) begin
            bit_1 <= adc_da;
            bit_0 <= adc_db;
            cnt_pos_dco <= 0;
        end
end    
 /*----------------------------------------------------------------------------------------------*/
 reg [7:0] cnt_neg_dco = 0;

always @ (negedge adc_clk_inv) begin 
   
        if((cnt_neg_dco == 0) & (cnt_pos_dco == 1)) begin
        	cnt_neg_dco <=  1; 
            bit_15 <= adc_da;
            bit_14 <= adc_db;
        end
        else if((cnt_neg_dco == 1) & (cnt_pos_dco == 2)) begin
            cnt_neg_dco <= 2;
            bit_11 <= adc_da;
            bit_10 <= adc_db; 
        end
        else if((cnt_neg_dco == 2) & (cnt_pos_dco == 3))  begin   
            cnt_neg_dco <=  3;
            bit_7 <= adc_da;
            bit_6 <= adc_db; 
        end
        else if((cnt_neg_dco == 3) & (cnt_pos_dco == 4))  begin 
            cnt_neg_dco <=  4;
            bit_3 <= adc_da;
            bit_2 <= adc_db;
        end
		else if((cnt_neg_dco == 4)  & (cnt_pos_dco == 0))  begin 
            cnt_neg_dco <=  0;
        end
end    
endmodule   
