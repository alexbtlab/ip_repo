

`define WRITE_STATE         0
`define READ_STATE          1
`define W0                  0
`define W1                  0
`define SIZE_TRANSACTION    24
`define ADDRESS_SECTION     cnt >= 4  & cnt <= 16
`define DATA_WRITE_SECTION  cnt >= 21 & cnt <= 28
`define DATA_READ_SECTION   cnt_re >= 21 & cnt_re <= 28
`define ALLOW_SCK_RANGE     (cnt > 0 & cnt <= 16) | (cnt > 20 & cnt <= 28)

`define POS_STATE           cnt == 1
`define POS_W0              cnt == 2
`define POS_W1              cnt == 3
`define POS_CS_SET          cnt == 34
`define POS_MAX_CNT         cnt == 80
`define POS_MAX_CNT_RE      cnt_re == 80
`define POS_READY_RX        cnt_re == 30
`define POS_SWICH_IO        cnt_re == 19

`define ENABLE_COUNT        enable_cnt & cnt != 80   
`define ENABLE_COUNT_RE     enable_cnt_re & cnt_re != 80 

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/31/2021 09:07:21 PM
// Design Name: 
// Module Name: io_2_0
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



`timescale 1 ns / 1 ps

module io_2_0 (
    input wire  data_in,
    output wire  data_out,
    inout wire DQ,
    input wire tristate_n,
     input wire  tristate_n2,
    
    input      wire START,
    input wire RW,
    input wire [12:0] ADR,
    input wire [7:0] DATA_TX,
    output wire [7:0] DATA_RX,
    input wire CLK,
    output wire data_rx_ready,
    inout wire SDIO,
    input wire CS,
    output wire SCK,
    
    output wire DATA_TX_serial,
    output reg DATA_RX_serial
);

 reg tristate_n_r;
assign tristate_n = tristate_n_r;
reg data_out_r;
assign data_out = data_out_r;

assign DQ=(tristate_n_r==1'b0)?'z:data_in;

 always_comb data_out_r=DQ;

 
 
 
 
 
 
 ///////////////////////////

 
   reg [7:0] DATA_RX_r;
    reg [7:0] cnt = 0;
//    reg tristate_n2;
//    reg DATA_TX_serial = 0;
//    reg DATA_RX_serial;
    reg start_prev = 0;
    reg enable_cnt = 0; 
    reg cs_r = 1;
    reg enable_sck;
    reg start_inner = 0;
 
 
    
    assign DATA_RX = DATA_RX_r; 
    assign SCK =  CLK &  enable_sck;
    assign CS = cs_r;
    assign SDIO = (tristate_n2 == 0) ? 'z : DATA_TX_serial;
    always_comb DATA_RX_serial = SDIO;

    
endmodule

