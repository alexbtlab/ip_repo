
`timescale 1 ns / 1 ps

module io_v1_0 (
    input wire  data_in,
    output wire  data_out,
    inout wire DQ,
    input wire tristate_n
);

    reg tristate_n_r;
    assign tristate_n = tristate_n_r;
    reg data_out_r;
    assign data_out = data_out_r;

    assign DQ = ( tristate_n_r == 1'b0 ) ? 'bz : data_in;
    always_comb data_out_r = DQ;

 

endmodule

