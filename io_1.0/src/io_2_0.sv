module io(data,read_write);

input read_write;
inout  data;

reg  data_reg, state; // data_reg - регистр изображения, state - регистр

    always@(read_write or data)
    begin
    if (read_write == 0) // 0 выводится, 1 вводится, когда 0, значение регистра состояния прикрепляется к регистру изображения, а регистр изображения выводится в порт данных
    // 1 - это вход, значение данных напрямую прикрепляется к регистру состояний, а порт с тремя состояниями должен быть отключен.
         data_reg<=state;
      else
         state<=data;
    end
    
    
    assign data = (read_write == 0)? data_reg: 2'bz; // 1 Когда данные оператора assign являются выходными,
endmodule 