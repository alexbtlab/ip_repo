
`timescale 1 ns / 1 ps

interface dco_or;
    logic dcoa, dcob, ora, orb;      // Indicates if slave is ready to accept data
    modport slave  (input dcoa, dcob, ora, orb);
endinterface
 
interface spi_adc;
    logic sck, cs, sdio;      // Indicates if slave is ready to accept data
    modport master  (output sck, output cs, inout sdio);
endinterface

	module AD9650_v2_0
	#
	(
		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(	   
		// Ports of Axi Slave Bus Interface S00_AXI	
		input wire clk_10MHz,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		
		
		output wire  m00_fft_axis_tvalid,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] m00_fft_axis_tdata,
		output wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] m00_fft_axis_tstrb,
		output wire  m00_fft_axis_tlast,
		input wire  m00_fft_axis_tready,
		
		output wire  m01_fft_axis_tvalid,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] m01_fft_axis_tdata,
		output wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] m01_fft_axis_tstrb,
		output wire  m01_fft_axis_tlast,
		input wire  m01_fft_axis_tready,
		        
        dco_or.slave DCO_OR,
                     
		spi_adc.master SPI_ADC,

        output wire ADC_PDwN,  // Power-Down Input in External Pin Mode.
        output wire SYNC,      // Digital Synchronization Pin
                
        input wire [15:0] DATA_INA,
        input wire [15:0] DATA_INB,
        
//        input wire [15:0] FrameSize,
        // output wire [15:0] DATA_INB_ila,
        
        output wire [15:0] cnt_in_DCO_ila,
        output wire [15:0] cnt_DCO_ila,
        
        input wire  allowed_clk,
        input wire  mux_fft,
        
        input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		output wire DCO
    );
    wire [15:0] FrameSize;
//    localparam  FrameSize = 8192;
    assign DCO = DCO_OR.dcoa;  
    assign m00_fft_axis_tstrb = 4'hF;  
    assign m01_fft_axis_tstrb = 4'hF;  
    wire [32-1:0]	ip2mb_reg0;
    wire [32-1:0]	ip2mb_reg1;
    wire [32-1:0]	ip2mb_reg2;
    wire [32-1:0]	ip2mb_reg3;
    wire [32-1:0]	ip2mb_reg4;
    wire [32-1:0]	ip2mb_reg5;
    wire [32-1:0]	ip2mb_reg6;
    wire [32-1:0]	ip2mb_reg7;
         
    wire [7:0] reg_address;
    wire [7:0] write_data;
    wire START;
    
    wire [12:0] ADR;
    wire [7:0] DATA_TX;
    wire [7:0] DATA_RX;
    wire [7:0] DATA_RX2;
    wire RW;
    reg [31:0] slv_reg3;
    
    reg [31:0] slv_reg4;
    reg [31:0] slv_reg5;
    reg [31:0] slv_reg6;
    reg [31:0] slv_reg7;
    //reg tristate_n;
 
    wire SYNC_signal;
    
//    AD9650_v1_0_S00_AXI # ( 
//		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
//		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
//	) AD9650_v1_0_S00_AXI_inst (
//		.S_AXI_ACLK(s00_axi_aclk),
//		.S_AXI_ARESETN(s00_axi_aresetn),
//		.S_AXI_AWADDR(s00_axi_awaddr),
//		.S_AXI_AWPROT(s00_axi_awprot),
//		.S_AXI_AWVALID(s00_axi_awvalid),
//		.S_AXI_AWREADY(s00_axi_awready),
//		.S_AXI_WDATA(s00_axi_wdata),
//		.S_AXI_WSTRB(s00_axi_wstrb),
//		.S_AXI_WVALID(s00_axi_wvalid),
//		.S_AXI_WREADY(s00_axi_wready),
//		.S_AXI_BRESP(s00_axi_bresp),
//		.S_AXI_BVALID(s00_axi_bvalid),
//		.S_AXI_BREADY(s00_axi_bready),
//		.S_AXI_ARADDR(s00_axi_araddr),
//		.S_AXI_ARPROT(s00_axi_arprot),
//		.S_AXI_ARVALID(s00_axi_arvalid),
//		.S_AXI_ARREADY(s00_axi_arready),
//		.S_AXI_RDATA(s00_axi_rdata),
//		.S_AXI_RRESP(s00_axi_rresp),
//		.S_AXI_RVALID(s00_axi_rvalid),
//		.S_AXI_RREADY(s00_axi_rready),
		
//	    .slv_reg0({ DATA_TX, ADR}),        // 0x0
//		.slv_reg1({ START }),       // 0x4
//		.slv_reg2({ ADC_PDwN, RW }),      // 0x8
//		.slv_reg3({ SYNC_signal, FrameSize  }),          // 12

//		.ip2mb_reg0(ip2mb_reg0),         //16
//		.ip2mb_reg1(ip2mb_reg1),         //20
//		.ip2mb_reg2(ip2mb_reg2),         //16
//		.ip2mb_reg3(ip2mb_reg3)          //20

//	);
    
//   reg SYNC_reg;
//   assign SYNC = SYNC_reg;
   
//   always @ (posedge clk_10MHz) begin
//        SYNC_reg <= SYNC_signal;
//   end
   
//   wire data_rx_ready;
//   wire clk_100MHz;
   
//   assign clk_100MHz = s00_axi_aclk;
   
//spi_AD9650 spi_AD9650_inst(
//    . START(START),
//    . RW(RW),
//    . ADR(ADR),
//    . DATA_TX(DATA_TX),
//    . DATA_RX(DATA_RX),
//    . DATA_RX2(DATA_RX2),
//    . CLK_10(clk_10MHz),
//    . CLK_100(clk_100MHz),
//    . SDIO(SPI_ADC.sdio),
//    . SCK(SPI_ADC.sck),
//    . CS(SPI_ADC.cs),
////    .tristate_n(tristate_n),
//    . data_ready(data_rx_ready)
////    .tristate_ila(tristate_ila)
//    ); 
   
   
//   assign  ip2mb_reg0[0]=  data_rx_ready;
//   assign  ip2mb_reg1 = DATA_RX;
////   assign  ip2mb_reg2 = DATA_RX2;  
  
//  reg [15:0] cnt_in_DCO;
//  reg [15:0] cnt_DCO;
//  assign cnt_in_DCO_ila = cnt_in_DCO;
//  assign cnt_DCO_ila = cnt_DCO;
  
//  reg m00_fft_axis_tvalid_r;
//  reg m01_fft_axis_tvalid_r;
//  assign m00_fft_axis_tvalid = m00_fft_axis_tvalid_r;
//  assign m01_fft_axis_tvalid = m01_fft_axis_tvalid_r;
  
//  reg m00_fft_axis_tlast_r;
//  reg m01_fft_axis_tlast_r;
//  assign m00_fft_axis_tlast = m00_fft_axis_tlast_r;
//  assign m01_fft_axis_tlast = m01_fft_axis_tlast_r;
  
//  wire [15:0] data_fft0_RE;
//  wire [15:0] data_fft0_IM;
//  wire [15:0] data_fft1_RE;
//  wire [15:0] data_fft1_IM;
  
//  assign m00_fft_axis_tdata = (mux_fft)  ?  {DATA_INB, DATA_INA} : 0;
//  assign m01_fft_axis_tdata = (~mux_fft) ?  {DATA_INB, DATA_INA} : 0;
  
//  always @ (posedge s00_axi_aclk) begin
  
//    if( DCO_OR.dcoa & allowed_clk & s00_axi_aresetn ) begin
//        cnt_in_DCO <= cnt_in_DCO + 1;
//    end
//    else begin
//        cnt_in_DCO <= 0;
//    end
    
//    if(mux_fft) begin
//            if(cnt_in_DCO == 1 & cnt_DCO > 0 & cnt_DCO <= FrameSize)        m00_fft_axis_tvalid_r <= 1;
//            else                                                            m00_fft_axis_tvalid_r <= 0;
//            if(cnt_in_DCO == 1 & cnt_DCO == FrameSize)                      m00_fft_axis_tlast_r <= 1;
//            else                                                            m00_fft_axis_tlast_r <= 0;    
//    end
//    else begin
//            if(cnt_in_DCO == 1 & cnt_DCO > 0 & cnt_DCO <= FrameSize)        m01_fft_axis_tvalid_r <= 1;
//            else                                                            m01_fft_axis_tvalid_r <= 0;
//            if(cnt_in_DCO == 1 & cnt_DCO == FrameSize)                      m00_fft_axis_tlast_r <= 1;
//            else                                                            m00_fft_axis_tlast_r <= 0;    
//    end
    
//  end
//  always @ (negedge DCO_OR.dcoa) begin
//     if(allowed_clk) begin  
//        if(cnt_DCO != FrameSize + 1)
//            cnt_DCO <= cnt_DCO + 1;
//        else
//            cnt_DCO <= FrameSize + 1;
//     end
//     else begin
//        cnt_DCO <= 0;
//     end
//  end

endmodule
