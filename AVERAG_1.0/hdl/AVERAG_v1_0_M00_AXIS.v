
`timescale 1 ns / 1 ps

	module AVERAG_ADC_DATA_v1_0_M00_AXIS #
	(
		parameter integer C_M_AXIS_TDATA_WIDTH	= 32,
		parameter integer C_M_START_COUNT	    = 32
	)   
	(
		input wire  M_AXIS_ACLK,
		input wire  M_AXIS_ARESETN,
		output wire  M_AXIS_TVALID,
		input wire  EnData,
		input wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA_in,
		output wire [C_M_AXIS_TDATA_WIDTH-1 : 0] M_AXIS_TDATA,
		output wire  M_AXIS_TLAST,
		input wire  M_AXIS_TREADY
		
		//input wire  [15:0] frame_size
	);
	
	
	
	assign M_AXIS_TDATA = (EnData) ? M_AXIS_TDATA_in : 0;
	



	endmodule
