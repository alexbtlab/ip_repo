module top_PLL_control(
    input wire clk_100MHz,
    input wire [15:0] shift_front,
    output wire pll_sen,
    output wire pll_sck,
    output wire pll_mosi,
    input wire pll_ld_sdo,    
    output wire pll_cen,
    output wire pll_trig,
    output wire [5:0] ATTEN, 
    output wire start_adc_count,
    //input wire [15:0] sweep_val,
    
    input wire [31:0]	slv_reg0,
    input wire [31:0]	slv_reg1,
    input wire [31:0]	slv_reg2,
    input wire [31:0]	slv_reg3,
    input wire [31:0]	slv_reg4,
    input wire [31:0]	slv_reg5,
    input wire [31:0]	slv_reg6,
    input wire [31:0]	slv_reg7,
    
    output wire [31:0]	ip2mb_reg0,
    output wire [31:0]	ip2mb_reg1,
    output reg  [31:0]	ip2mb_reg2,
    output reg  [31:0]	ip2mb_reg3,
    output reg  [31:0]	ip2mb_reg4,
    output reg  [31:0]	ip2mb_reg5,
    output reg  [31:0]	ip2mb_reg6,
    output reg  [31:0]	ip2mb_reg7,
    
    input wire FPGA_clk
);
        wire clk_uart;
        wire clk_pll_spi;
        wire clk_8MHz;    
        reg [7:0] pll_address_reg = 0;
        reg pll_read = 0;
        wire pll_sck_read;
        wire pll_mosi_read;
        wire pll_sen_read;
        wire [23:0] pll_read_data;
        wire pll_data_ready;
        reg [23:0] pll_write_data = 0;
        reg pll_write = 0;
        wire pll_sck_write;
        wire pll_mosi_write;
        wire pll_sen_write;
        reg [4:0] pll_write_address_reg = 0;
        wire sys_clk_locked;
        reg uart_byte_received_prev = 0;
        wire uart_byte_received;
        reg pll_data_ready_prev;
        reg is_counting = 0; 
        reg[7:0] uart_tx_data;
        reg uart_start_sending = 0;
        reg [3:0] received_byte_num = 0; 
        reg is_uart_receiving_atten_value = 0;
        reg [5:0] atten_reg = 6'h3F;
        reg is_uart_receiving_pll_data = 0;
        wire[7:0] uart_rx_data;
        reg is_uart_receiving_pll_read_address = 0;
        reg is_uart_receiving_pll_write_address = 0;
        reg is_uart_receiving_clk_write_address = 0;
        reg [7:0] clk_address_reg = 0;
        reg is_uart_receiving_clk_data = 0; 
        reg [7:0] clk_write_data_reg = 0;
        reg sw_ctrl_reg = 1'b0;
        reg sw_if1_reg = 1'b1;    
        reg sw_if2_reg = 1'b0;
        reg sw_if3_reg = 1'b0; 
        reg pamp_en_reg = 1'b0;
        reg if_amp1_en_reg=1'b0;
        reg if_amp2_en_reg=1'b0;
        reg is_adc_data_sending = 0;
        reg[3:0] cnt_uart = 0; 
        wire wSTART; 
        reg pll_trig_reg = 0;
        reg pll_cen_reg = 0;
        reg START_send;        
        reg START_receive; 
        reg START_trig; 
        reg EN_clk_8MHz; 
        wire [31:0] DATA_receive;
        wire [31:0] DATA_send;
        wire [7:0] reg_address_receive;
        wire [4:0] reg_address_send;
        
        reg start_adc_count_r;
        assign start_adc_count = start_adc_count_r;//assign start_adc_count = (START_trig) ? start_adc_count_r : 0;
        assign ATTEN = atten_reg; 
        assign sys_clk_locked = 1;
        assign pll_sen = pll_sen_write | pll_sen_read;
        assign pll_sck = pll_sck_write | pll_sck_read;
        assign pll_mosi = pll_mosi_write | pll_mosi_read;   
        assign pll_cen = pll_cen_reg;
        assign pll_trig = pll_trig_reg;  
        reg [15:0] trig_tguard_counter=0;   
        reg [15:0] trig_tsweep_counter=0;
   /*------------------------------------------------------------------------------------------------*/       
     clk_wiz_0 clk_wizard_main(      
      .clk_out1(clk_uart),      
      .clk_out2(clk_8MHz),
      .clk_out3(clk_pll_spi),
      .clk_in1(clk_100MHz)              
    );    
 /*------------------------------------------------------------------------------------------------*/          
    pll_receive pll_receive(
        .clk(clk_pll_spi),
        .reg_address(reg_address_receive),
        .start(START_receive),
        .pll_clk(pll_sck_read),
        .pll_mosi(pll_mosi_read),
        .pll_miso(pll_ld_sdo),
        .pll_cs(pll_sen_read),
        .read_data(DATA_receive),
        .data_ready(pll_data_ready)        
    );
/*------------------------------------------------------------------------------------------------*/       
    pll_send pll_send_i(
        .clk(clk_pll_spi),
        .pll_data(DATA_send),
        .reg_address(reg_address_send),
        .start(START_send),
        .pll_clk(pll_sck_write),
        .pll_mosi(pll_mosi_write),
        .pll_cs(pll_sen_write)                  
    );            
 ///////////////////////////////////////////////////////////////////////    
always @(posedge clk_100MHz) begin 
     //ip2mb_reg0 <= 32'hACDC0000;
     //ip2mb_reg1 <= 32'hACDC0001;
     ip2mb_reg2 <= 32'hACDC0002;
     ip2mb_reg3 <= 32'hACDC0003;
     ip2mb_reg4 <= 32'hACDC0004;
     ip2mb_reg5 <= 32'hACDC0005;
     ip2mb_reg6 <= 32'hACDC0006;
     ip2mb_reg7 <= 32'hACDC0007;    
end   
//////////////////////////////////////////////////////////////////////////   
reg  netSTART_send;    
//reg  [15:0] sweep_val; 

    assign  ip2mb_reg0[0] = pll_data_ready;  // ������ ���� ���������� � 0-� ��� �������� ������� IP2MB 
    assign  ip2mb_reg1 = DATA_receive;       // ��������� ����� ������ ��������� � ��������� ��� �������� ������ � ��
    assign  reg_address_send = slv_reg1;     // ��������� �������� �������� �� �� ������� ������� ������� ���������\���������� SPI.��� ��������� ������ �������� �� HMC769 
    assign  reg_address_receive = slv_reg2;
    assign  DATA_send = slv_reg3; 
//////////////////////////////////////////////////////////////////////////   
always @(posedge clk_100MHz) begin    // ���� ������� �� ����������� � ������ ��������
        atten_reg <= slv_reg4[5:0];
        //sweep_val <= slv_reg5[15:0];
             
        if(slv_reg0[0] == 1'b1)     START_send <= 1;
        else                        START_send <= 0;
        if(slv_reg0[1] == 1'b1)     START_receive <= 1;
        else                        START_receive <= 0;
        if(slv_reg0[2] == 1'b1)     START_trig <= 1;
        else                        START_trig <= 0;
//        if(slv_reg1[0] == 1'b1)     EN_clk_8MHz <= 1;
//        else                        EN_clk_8MHz <= 0;
end
        
localparam sweep_val = 9000;  
         
always @(posedge FPGA_clk) begin
    //if(EN_clk_8MHz) begin
        if ((trig_tsweep_counter < sweep_val)&(trig_tguard_counter==0))
        begin 
            trig_tsweep_counter <= trig_tsweep_counter + 1;
            pll_trig_reg <= 0;
        end
        else if ((trig_tsweep_counter == sweep_val)&(trig_tguard_counter==0))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter>0)&(trig_tguard_counter<100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 0;
            trig_tguard_counter<=trig_tguard_counter+1;                        
        end
        else if ((trig_tsweep_counter==0)&(trig_tguard_counter==100))
        begin
            trig_tsweep_counter <= 0;
            pll_trig_reg <= 1;
            trig_tguard_counter<=0;                        
        end
    //end
end
always @(negedge FPGA_clk)  begin
    //if(EN_clk_8MHz) begin 
        if((trig_tguard_counter == 0) & (trig_tsweep_counter > shift_front))
            start_adc_count_r <= 1;
        else
            start_adc_count_r <= 0;
   //end         
end
endmodule