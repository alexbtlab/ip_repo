`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 18.09.2017 17:00:50
// Design Name: 
// Module Name: adc_get_data
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ADC_VETAL(
    input wire fpga_clk,
    input wire clk_100MHz,    
    input wire adc_dco,
    input wire adc_da,
    input wire adc_db,
    output wire adc_clk,   
    output wire [15:0] adc_data
    );
    
    reg [15:0] adc_data_reg = 0;
    
    assign adc_data = adc_data_reg;
    
    wire clk_adc;
    assign clk_adc = clk_100MHz;
    
    reg data_15 = 0;
    reg data_14 = 0;
    reg data_13 = 0;
    reg data_12 = 0;
    reg data_11 = 0;
    reg data_10 = 0;
    reg data_9 = 0;
    reg data_8 = 0;
    reg data_7 = 0;
    reg data_6 = 0;
    reg data_5 = 0;
    reg data_4 = 0;
    reg data_3 = 0;
    reg data_2 = 0;
    reg data_1 = 0;
    reg data_0 = 0;
    
    wire [15:0] adc_data_temp;
   
    assign adc_data_temp[15] = data_15; 
    assign adc_data_temp[14] = data_14;
    assign adc_data_temp[13] = data_13;
    assign adc_data_temp[12] = data_12;
    assign adc_data_temp[11] = data_11;
    assign adc_data_temp[10] = data_10;
    assign adc_data_temp[9] = data_9;
    assign adc_data_temp[8] = data_8;
    assign adc_data_temp[7] = data_7;
    assign adc_data_temp[6] = data_6;
    assign adc_data_temp[5] = data_5;
    assign adc_data_temp[4] = data_4;
    assign adc_data_temp[3] = data_3;
    assign adc_data_temp[2] = data_2;
    assign adc_data_temp[1] = data_1;
    assign adc_data_temp[0] = data_0;
    
    
    
    reg adc_reading_allowed_reg = 0;
      
    assign adc_clk = clk_adc&(adc_reading_allowed_reg>0);
    
    reg [7:0] tconv_cnt_reg = 0;
    
    reg fpga_clk_prev = 0;
    
    reg even_odd_frame = 0;
    
   
    
    always @(negedge clk_adc)
    begin  
        
          
        if (fpga_clk == 1)
        begin
            fpga_clk_prev <= 1;
            if (fpga_clk_prev==0) tconv_cnt_reg <= 0;
            else tconv_cnt_reg <= tconv_cnt_reg+1;
         end
         else
         begin
            fpga_clk_prev <= 0;
            tconv_cnt_reg <= tconv_cnt_reg+1;
         end
        
        if (tconv_cnt_reg==4) even_odd_frame<=even_odd_frame+1; 
             
        if (tconv_cnt_reg==7) 
        begin
            adc_reading_allowed_reg <= 1;
            data_15 = adc_da;
            data_14 = adc_db;
        end    
        else if (tconv_cnt_reg==1) adc_reading_allowed_reg <=0;
    end
    
    reg [1:0] dco_pos_num = 0; //????? ????????? ???????? ?? 0 ?? 3, ?.? ???? ??? 3 ????????? 1, ????? ????? 0
    reg [1:0] dco_neg_num = 0; //????? ????????? ???????? ?? 0 ?? 3, ?.? ???? ??? 3 ????????? 1, ????? ????? 0
    
    reg even_odd_frame_prev_pos = 0;
    
    always @(posedge adc_dco)
    begin
       even_odd_frame_prev_pos <= even_odd_frame;
       if (even_odd_frame_prev_pos != even_odd_frame) dco_pos_num <= 1;
       else if ((dco_pos_num == 0)&(even_odd_frame_prev_pos == even_odd_frame)) dco_pos_num <= 0; //???? ????????? 5-? ??????? dco, ?? ?? ?? ?????????
       else dco_pos_num <= dco_pos_num + 1;
       
        
        if ((dco_pos_num == 0)&(even_odd_frame_prev_pos != even_odd_frame)) 
        begin
            data_13 = adc_da;
            data_12 = adc_db;
        end 
        if (dco_pos_num == 1) 
        begin
            data_9 = adc_da;
            data_8 = adc_db;
        end
        if (dco_pos_num == 2) 
        begin
            data_5 = adc_da;
            data_4 = adc_db;
        end 
        if (dco_pos_num == 3) 
        begin
            data_1 = adc_da;
            data_0 = adc_db;
        end  
        
    end
    
   reg even_odd_frame_prev_neg = 0;
   
   
    
    always @(negedge adc_dco)
    begin
    even_odd_frame_prev_neg <= even_odd_frame;
    if (even_odd_frame_prev_neg != even_odd_frame) dco_neg_num <= 1;
    else if ((dco_neg_num == 0)&(even_odd_frame_prev_neg == even_odd_frame)) dco_neg_num <= 0; //???? ????????? 5-? ??????? dco, ?? ?? ?? ?????????
    else dco_neg_num<=dco_neg_num+1;
    
        if ((dco_neg_num == 0)&(even_odd_frame_prev_neg != even_odd_frame))
        begin        
            data_11 = adc_da;
            data_10 = adc_db;
             
           
        end 
        else if (dco_neg_num == 1) 
        begin
            data_7 = adc_da;
            data_6 = adc_db;
        end
        else if (dco_neg_num == 2) 
        begin
            data_3 = adc_da;
            data_2 = adc_db;            
        end
        //??? dco_neg_num == 3 ?? ?????? ??? ??????? ????????, ??????? ?? ??????????         
         else if (dco_neg_num == 3) 
        begin            
            
            adc_data_reg = adc_data_temp;
        end  
          
    end
    
    
endmodule
