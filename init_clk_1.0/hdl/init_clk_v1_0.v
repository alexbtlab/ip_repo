module init_clk(
    input wire clk,    
    //input wire start,
    output wire adc_clk_valid,
    output wire clk_sclk,
    output wire clk_mosi,
    output wire clk_cs,
    output wire clk_sync    
    );
    
    wire start;
    wire clk_uart;
    wire clk_pll_spi;
    wire clk_spi;
    //wire clk_adc;
    wire clk_adc_delay;   
    wire sys_clk_locked;
    wire clk_8MHz;
    
    clk_wiz_0 clk_wizard_main(      
      .clk_out1(clk_8MHz),      
      .clk_in1(clk),
      .locked(sys_clk_locked)                 
    );
    
    reg clk_start_init = 0;
    assign start = clk_start_init;
        INITCLK clk_send_all_regs(
        .clk(clk_8MHz & sys_clk_locked),
        .start(clk_start_init),
        .clk_sclk(clk_sclk),
        .clk_mosi(clk_mosi),
        .clk_cs(clk_cs)               
    );
    
    /*-----------------------------------------------------------------------------*/
    reg is_clk_initialized_reg = 0;
    reg [31:0] clk_sync_counter_reg = 0;
    
    reg clk_sync_reg =0;
    assign clk_sync=clk_sync_reg;
    reg adc_clk_valid_reg=0;
    assign adc_clk_valid = adc_clk_valid_reg;
    
    always @(posedge clk_8MHz & sys_clk_locked)
     begin 
        if (is_clk_initialized_reg==0)  begin
            if (clk_sync_counter_reg<100000) //��� 12,5 ��
            begin
            clk_sync_counter_reg <= clk_sync_counter_reg + 1;                
            end
            else if (clk_sync_counter_reg==100000)
            begin
                clk_start_init <= 1;
                clk_sync_reg <= 1;
                clk_sync_counter_reg <= clk_sync_counter_reg + 1; 
            end
            else if (clk_sync_counter_reg==160000)
            begin
                adc_clk_valid_reg <= 1;
                clk_sync_counter_reg <= clk_sync_counter_reg + 1;
            end
            else if (clk_sync_counter_reg<180000) //������ clk_sync = 1 � ������� 5 ��
            begin
                clk_sync_counter_reg <= clk_sync_counter_reg + 1;                             
            end            
            else
            begin
                clk_start_init <= 0;
                clk_sync_reg <= 0;
                is_clk_initialized_reg<=1;
            end             
        end
     end
    
endmodule