module INITCLK(
    input wire clk, 
    //input wire start_ext,   
    input wire start,
    output wire clk_sclk,
    output wire clk_mosi,
    output wire clk_cs,
    output wire clk_sync    
    );
  

    //wire start;
    //wire clk_uart;
    //wire clk_pll_spi;
    //wire clk_spi;
    //wire clk_adc;
    //wire clk_adc_delay;   
    //wire sys_clk_locked;
    //wire clk_8MHz;
       
    reg [6:0] bits_left = 0;
    reg start_prev = 0;
    reg clk_mosi_reg = 0, clk_cs_reg=1;    
    reg [23:0] read_data_reg = 0;   
    reg [7:0] address = 0; //�������� ���������� � �������� ������
    reg [55:0] write_data = (8'h03<<48)+ //Reg h00
                            (8'hC0<<40)+ //reg h01 
                            (8'd10<<32)+ //reg h02
                            (8'hC0<<24)+ //reg h03
                            (8'd10<<16)+ //reg h04   //(8'd01<<16)+ //reg h04   1 - 
                            (8'hC0<<08)+ //reg h05
                            (8'd10<<00); //reg h06
    
    assign clk_sclk = (clk & (bits_left>0));    
    assign clk_cs = clk_cs_reg;
    assign clk_mosi = clk_mosi_reg;
        
    always @(negedge clk)
    begin
        
        if (start==1)
        begin
            start_prev <= 1;
            if (start_prev == 0)
            begin            
                bits_left <= 64;
                clk_cs_reg <= 0;
                clk_mosi_reg <= address[6];             
            end
            else if (bits_left > 58)
            begin            
                clk_mosi_reg <= (address>>(bits_left-59));
                bits_left <= bits_left - 1;
            end
            else if (bits_left == 58)
            begin            
                clk_mosi_reg <= 0; //������
                bits_left <= bits_left - 1;
            end
            else if (bits_left > 1)
            begin            
                clk_mosi_reg = write_data[bits_left-2];
                bits_left <= bits_left - 1;
            end
            else if (bits_left == 1)
            begin            
                clk_mosi_reg <= 0;
                bits_left <= bits_left - 1;
            end
            else 
            begin 
                clk_cs_reg <= 1;
            end
        end
        else start_prev<=0;                  
    end
    /*---------------------------------------------------------------------------*/
   
  
    
endmodule