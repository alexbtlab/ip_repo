//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Nov  5 18:13:38 2021
//Host        : alex-HP-Compaq-8200-Elite-CMT-PC running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target design_2.bd
//Design      : design_2
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_2,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_2,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=2,numReposBlks=2,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_2.hwdef" *) 
module design_2
   (clk_10MHz_0,
    data_to_and_from_pins_0,
    s00_axi_aresetn_0);
  input clk_10MHz_0;
  inout [0:0]data_to_and_from_pins_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 RST.S00_AXI_ARESETN_0 RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME RST.S00_AXI_ARESETN_0, INSERT_VIP 0, POLARITY ACTIVE_LOW" *) input s00_axi_aresetn_0;

  wire AD9650_0_io_reset;
  wire AD9650_0_tristate_n_ila;
  wire [0:0]Net;
  wire clk_10MHz_0_1;
  wire s00_axi_aresetn_0_1;

  assign clk_10MHz_0_1 = clk_10MHz_0;
  assign s00_axi_aresetn_0_1 = s00_axi_aresetn_0;
  design_2_AD9650_0_0 AD9650_0
       (.DATA_INA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .DATA_INB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .clk_10MHz(clk_10MHz_0_1),
        .dco_or_dcoa(1'b0),
        .dco_or_dcob(1'b0),
        .dco_or_ora(1'b0),
        .dco_or_orb(1'b0),
        .io_reset(AD9650_0_io_reset),
        .s00_axi_aclk(clk_10MHz_0_1),
        .s00_axi_araddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_aresetn(s00_axi_aresetn_0_1),
        .s00_axi_arprot({1'b0,1'b0,1'b0}),
        .s00_axi_arvalid(1'b0),
        .s00_axi_awaddr({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_awprot({1'b0,1'b0,1'b0}),
        .s00_axi_awvalid(1'b0),
        .s00_axi_bready(1'b0),
        .s00_axi_rready(1'b0),
        .s00_axi_wdata({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .s00_axi_wstrb({1'b1,1'b1,1'b1,1'b1}),
        .s00_axi_wvalid(1'b0),
        .tristate_n_ila(AD9650_0_tristate_n_ila));
  design_2_selectio_wiz_0_0 selectio_wiz_0
       (.clk_in(clk_10MHz_0_1),
        .data_out_from_device(1'b0),
        .data_to_and_from_pins(data_to_and_from_pins_0[0]),
        .io_reset(AD9650_0_io_reset),
        .tristate_output(AD9650_0_tristate_n_ila));
endmodule
