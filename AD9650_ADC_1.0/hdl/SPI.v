module SPI(
    input wire clk,
    input wire [7:0] write_data,
    
    input wire [7:0] reg_address,
    input wire start,
    output wire clk_sclk,
    output wire clk_mosi,
    output wire clk_cs
    
    );
    
    reg [5:0] bits_left = 0;
    reg start_prev = 0;
    reg clk_mosi_reg = 0, clk_cs_reg=1;
    reg [23:0] read_data_reg = 0;    
    
    assign clk_sclk = clk & (bits_left>0);    
    assign clk_cs = clk_cs_reg;
    assign clk_mosi = clk_mosi_reg;
        
    always @(negedge clk)
    begin
        start_prev <= start;
        if (start_prev < start)
        begin
            bits_left <= 24;
            clk_cs_reg <= 0;
            clk_mosi_reg <= 0; //0-write command             
        end 
        else if (bits_left > 16)
        begin
            bits_left <= bits_left - 1;
        end 
        else if (bits_left == 16)
        begin            
            clk_mosi_reg <= reg_address[7];
            bits_left <= bits_left - 1;
        end      
        else if (bits_left > 9)
        begin            
            clk_mosi_reg <= (reg_address>>(bits_left-10));
            bits_left <= bits_left - 1;
        end
        else if (bits_left == 9)
        begin            
            clk_mosi_reg <= write_data[7];
            bits_left <= bits_left - 1;
        end
        else if (bits_left > 1)
        begin            
            clk_mosi_reg = (write_data>>(bits_left-2));
            bits_left <= bits_left - 1;
        end
        else if (bits_left == 1)
        begin            
            clk_mosi_reg <= 0;
            bits_left <= bits_left - 1;
        end
        else 
        begin 
            clk_cs_reg <= 1;
        end
    end
    
endmodule
