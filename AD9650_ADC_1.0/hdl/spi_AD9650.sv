`define WRITE_STATE         0
`define READ_STATE          1
`define W0                  1
`define W1                  1
`define MAX_CNT             40
`define SIZE_TRANSACTION    24
`define ADDRESS_SECTION     cnt >= 4  & cnt <= 16
`define DATA_SECTION        cnt >= 17 & cnt <= 24
                    
module spi_AD9650(
    
    input START,
    input RW,
    input [12:0] ADR,
    input [7:0] DATA_TX,
    output [5:0] DATA_RX,
    input CLK,
    inout SDIO,
    output SCK,
    output CS,
    output data_rx_ready
    );
    
    reg [7:0] DATA_RX_r;
    assign DATA_RX = DATA_RX_r; 
    reg tristate_n;
    reg DATA_TX_serial = 0;
    reg DATA_RX_serial;
    
    assign SDIO = (tristate_n == 0) ? 'z : DATA_TX_serial;
    always_comb DATA_RX_serial = SDIO;

    reg start_prev = 0;
    reg enable_cnt = 0;
    reg [15:0] cnt = 0;
    reg mosi_r;
    reg cs_r = 1;
    reg enable_sck;
    
    reg [15:0] cnt_re = 0 ;
    reg enable_cnt_re = 0;
    reg start_prev_fe = 0;

    assign SCK =  CLK &  enable_sck;
    assign CS = cs_r;
    
    reg data_rx_ready_r = 0;
    assign data_rx_ready = data_rx_ready_r;

    always @ (negedge CLK) begin

//    /*----WRITE-------------------------------------------------------------------------------------*/
        if(RW == 0) begin    // write
                        
//                        tristate_n <= 1;
                        if(START == 1 & start_prev == 0) begin                        
                            start_prev <= 1;
                            enable_cnt <= 1;
                            cs_r <= 0;
                        end
                        
                        if(enable_cnt & cnt != `MAX_CNT)            cnt <= cnt + 1;
                        else                                        cnt <= 0;
                        if(cnt > 0 & cnt <= `SIZE_TRANSACTION)      enable_sck <= 1;
                        else                                        enable_sck <= 0;
                        
                        if(cnt == 1 )                   DATA_TX_serial <= `WRITE_STATE; //write
                        if(cnt == 2 )                   DATA_TX_serial <= `W0; //write
                        if(cnt == 3 )                   DATA_TX_serial <= `W1; //write
    
                        if(`ADDRESS_SECTION)            DATA_TX_serial <= ADR[16 - cnt]; //write
                        if(`DATA_SECTION)               DATA_TX_serial <= DATA_TX[24 - cnt];
                                
                        if(cnt == 24) begin 
                            cs_r <= 1;
                        end
                        if(cnt == `MAX_CNT) begin
                            enable_cnt <= 0;
                            start_prev <= 0;
                        end
        end   
    
//    /*----READ-------------------------------------------------------------------------------------*/      
    else begin

                    if(START == 1 & start_prev == 0) begin     
                        tristate_n <= 1;                   
                        start_prev <= 1;
                        enable_cnt <= 1;
                        cs_r <= 0;
                    end
                    
                    if(enable_cnt & cnt != `MAX_CNT)            cnt <= cnt + 1;
                    else                                        cnt <= 0;
                    if(cnt > 0 & cnt <= `SIZE_TRANSACTION)      enable_sck <= 1;
                    else                                        enable_sck <= 0;
                    
                    if(cnt == 1 )                   DATA_TX_serial <= `WRITE_STATE; //write
                    if(cnt == 2 )                   DATA_TX_serial <= `W0; //write
                    if(cnt == 3 )                   DATA_TX_serial <= `W1; //write

                    if(`ADDRESS_SECTION)            DATA_TX_serial <= ADR[16 - cnt]; //write
                    if(`DATA_SECTION)               DATA_TX_serial <= DATA_TX[24 - cnt];
                            
                    if(cnt == 24) begin 
                        cs_r <= 1;
                    end
                    if(cnt == `MAX_CNT) begin
                        enable_cnt <= 0;
                        start_prev <= 0;
                    end
                    
    end
end
    


    always @ (posedge CLK) begin

    /*----WRITE-------------------------------------------------------------------------------------*/
                if(RW == 1) begin    // write
                                
                                if(START == 1 & start_prev_fe == 0) begin  
                                    tristate_n <= 1;                       
                                    start_prev_fe <= 1;
                                    enable_cnt_re <= 1;
                                    data_rx_ready_r <= 0; 
                                end
                                if(cnt_re == 16)    tristate_n <= 0;
                                if(cnt_re >= 17 & cnt_re <= 24)                 DATA_RX_r[24 - cnt_re] <= DATA_RX_serial;
                                if(cnt_re == 25)                                data_rx_ready_r <= 1; 
                                
                                if(enable_cnt_re & cnt_re != `MAX_CNT)          cnt_re <= cnt_re + 1;
                                else                                            cnt_re <= 0;

                                if(cnt_re == `MAX_CNT) begin
                                    enable_cnt_re <= 0;
                                    start_prev_fe <= 0;
                                end
                end  
                else begin
                    data_rx_ready_r <= 0; 
                    tristate_n <= 1;
                    start_prev_fe <= 0;
                    enable_cnt_re <= 0;
                end 

                          
    end
    
endmodule
