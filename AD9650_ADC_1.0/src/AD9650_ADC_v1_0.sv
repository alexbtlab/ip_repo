
`timescale 1 ns / 1 ps

interface dco_or;
    logic dcoa, dcob, ora, orb;      // Indicates if slave is ready to accept data
    modport slave  (input dcoa, dcob, ora, orb);
endinterface

	module AD9650_ADC_v1_0
	#
	(
		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
	   output wire dcoa_ila,
	   output wire dcob_ila,
	   output wire ora_ila,
	   output wire orb_ila,
	   
		// Ports of Axi Slave Bus Interface S00_AXI	
		input wire clk_10MHz,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready,
		
        output wire IO_1,
        
        dco_or.slave DCO_OR,
             
        output wire SPI_SCLK,
        output wire SPI_SDIO,
        output wire SPI_CS,
        
        output wire ADC_PDwN,  // Power-Down Input in External Pin Mode.
        output wire SYNC,      // Digital Synchronization Pin
        
//        input wire DCOA,       // Channel A Data Clock Output.
//        input wire DCOB,       // Channel B Data Clock Output. 
        
//        input wire ORA,        // Channel A Overrange Output 
//        input wire ORB,        // Channel B Overrange Output 
        
        input wire [15:0] DATA_INA,
        input wire [15:0] DATA_INB,
        
        output wire [15:0] DATA_INA_ila,
        output wire [15:0] DATA_INB_ila,
        
        input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn
    );
    
    
    assign dcoa_ila = DCO_OR.dcoa;
    assign dcob_ila = DCO_OR.dcob;
    assign ora_ila  = DCO_OR.ora;
    assign orb_ila  = DCO_OR.orb;
    
    wire [32-1:0]	ip2mb_reg0;
    wire [32-1:0]	ip2mb_reg1;
    wire [32-1:0]	ip2mb_reg2;
    wire [32-1:0]	ip2mb_reg3;
    wire [32-1:0]	ip2mb_reg4;
    wire [32-1:0]	ip2mb_reg5;
    wire [32-1:0]	ip2mb_reg6;
    wire [32-1:0]	ip2mb_reg7;
         
    wire [7:0] reg_address;
    wire [7:0] write_data;
    wire start;
    
    wire START;
    wire [12:0] ADR;
    wire [5:0] DATA_TX;
    wire [5:0] DATA_RX;
    wire CLK;
    wire SDIO;
    wire SCK;
    wire CS;   
    wire data_rx_ready;
    wire RW;
    reg [31:0] slv_reg3;
    
    reg [31:0] slv_reg4;
    reg [31:0] slv_reg5;
    reg [31:0] slv_reg6;
    reg [31:0] slv_reg7;
    
    AD9650_ADC_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) AD9650_ADC_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
	    .slv_reg0({DATA_TX, ADR}),     // 0x0
		.slv_reg1({ START }),             // 0x4
		.slv_reg2({ RW }),                // 0x8
		.slv_reg3({ slv_reg3 }),          // 12
		.slv_reg4(slv_reg4),     // 0x0
		.slv_reg5(slv_reg5),             // 0x4
		.slv_reg6(slv_reg6),                // 0x8
		.slv_reg7(slv_reg7),          // 12
		
		.ip2mb_reg0(ip2mb_reg0),          //16
		.ip2mb_reg1(ip2mb_reg1),           //20
		.ip2mb_reg2(ip2mb_reg2),          //16
		.ip2mb_reg3(ip2mb_reg3),           //20
		.ip2mb_reg4(ip2mb_reg4),          //16
		.ip2mb_reg5(ip2mb_reg5),           //20
		.ip2mb_reg6(ip2mb_reg6),          //16
		.ip2mb_reg7(ip2mb_reg7)           //20
	);
    
    
    
spi_AD9650 spi_AD9650_inst(
    . START(START),
    . RW(RW),
    . ADR(ADR),
    . DATA_TX(DATA_TX),
    . DATA_RX(DATA_RX),
    . CLK(clk_10MHz),
    . SDIO(SPI_SDIO),
    . SCK(SPI_SCLK),
    . CS(SPI_CS),
    . data_rx_ready(data_rx_ready)
    ); 
   
   assign  ip2mb_reg0[0]=  data_rx_ready;
   assign  ip2mb_reg1 = DATA_RX;  
//   assign SPI_SCLK  = DCOA;
//   assign SPI_SDO   = DCOA;
//   assign SPI_CS    = DCOA;
//   assign ADC_PDwN  = DCOA;
//   assign SYNC      = DCOA;
//   //assign SPI_SCLK  = DCOA;
   
   
 
   
    

endmodule
