

/***************************** Include Files *******************************/
#include "AD9650_ADC.h"

void AD9650_ADC_SetSwitch_AMP(uint32_t numAmpEnable){

                            AD9650_ADC_mWriteReg(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 4, 0x1); 
    if(numAmpEnable == 1)   AD9650_ADC_mWriteReg(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0, 0xA);
    if(numAmpEnable == 2)   AD9650_ADC_mWriteReg(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0, 0x30);
    if(numAmpEnable == 3)   AD9650_ADC_mWriteReg(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0, 0xF5);
    if(numAmpEnable == 4)   AD9650_ADC_mWriteReg(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0, 0x3FF);
}
/************************** Function Definitions ***************************/
