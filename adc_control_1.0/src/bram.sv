`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.09.2017 10:20:54
// Design Name: 
// Module Name: bram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram (
input clk,
input we,
input [18:0] addr_write,
input [18:0] addr_read,
input [17:0] din,
output [17:0] dout
);
reg [17:0] RAM [8191:0];
reg [17:0] dout_reg;
reg we_prev;

assign dout = dout_reg;

always @(posedge clk)
begin
    if (we)
    begin
        we_prev<=1;
        if (we_prev == 0) RAM[addr_write] <= din;        
    end
    else
    begin
        dout_reg <= RAM[addr_read];
        we_prev<=0;
    end    
end

endmodule
