`timescale 1ns / 1ps

interface sb;
    logic [7:0] address;
    logic [7:0] data;
    logic dataValid;
    logic start, read;   
    modport master (output start, output read, output address, inout dataValid, inout data ); 
    modport slave  (input  start, input  read, input  address, inout dataValid, inout data );
endinterface

module ProcessorInterfaceThread(
    input logic resetN, clock,
    input logic access,
    input logic doRead,
    input logic wDataRdy,
    input logic [7:0] DataReg,
    input logic [15:0] AddrReg,
    sb.master procBus
);

    logic en_AddrUp, en_AddrLo,
    ld_Data, en_Data, 
//    access = 0,
//    doRead, wDataRdy, dv;
    dv;
//    logic [7:0] DataReg;
    logic [15:0] DataFromMemReg;

    enum {MA, MB, MC, MD}
        State, NextState;

        assign procBus.data      = (en_Data) ? DataReg : 'bz;
        assign procBus.dataValid = (State == MD) ? dv : 1'bz;
        assign procBus.dataValid = dv;


always_comb
    if      (en_AddrLo) procBus.address = AddrReg[7:0];
    else if (en_AddrUp) procBus.address = AddrReg[15:8];
    else                procBus.address = 'bz;

always @( posedge clock )
    if (ld_Data) DataFromMemReg <= procBus.data;

//always_ff @(posedge clock, negedge resetN)
//    if (~resetN) State <= MA;
//    else State <= NextState;

always_ff @(posedge clock, negedge resetN) begin
    if (~resetN) begin
        procBus.start <= 0;
        en_AddrUp <= 0;
        en_AddrLo <= 0;
        procBus.read <= 0;
        ld_Data <= 0;
        en_Data <= 0;
        dv = 0;
    end 
    else begin
        case (State)
            MA: begin
                State <= (access) ? MB : MA;
                procBus.start <= (access) ? 1: 0;
                en_AddrUp <= (access) ? 1: 0;
                dv <= 0;
            end
            MB: begin
                State <= (doRead) ? MC : MD;
                procBus.start <= 0;
                en_AddrUp <= 0;
                en_AddrLo <= 1;
                procBus.read <= (doRead) ? 1: 0;
            end
            MC: begin
                State <= (procBus.dataValid) ? MA : MC;
                en_AddrLo <= 0;
                ld_Data <= (procBus.dataValid) ? 1: 0;
            end
            MD: begin // write
                State <= (wDataRdy) ? MA: MD;
                en_AddrLo <= 0;
                en_Data <= (wDataRdy) ? 1: 0;
                dv <= (wDataRdy) ? 1:0;
            end
        endcase
    end
end
//always_comb begin
//    procBus.start = 0;
//    en_AddrUp = 0;
//    en_AddrLo = 0;
//    procBus.read = 0;
//    ld_Data = 0;
//    en_Data = 0;
//    dv = 0;

//    case (State)
//        MA: begin
//            NextState = (access) ? MB : MA;
//            procBus.start = (access) ? 1: 0;
//            en_AddrUp = (access) ? 1: 0;
//        end
//        MB: begin
//            NextState = (doRead) ? MC : MD;
//            en_AddrLo = 1;
//            procBus.read = (doRead) ? 1: 0;
//        end
//        MC: begin
//            NextState = (procBus.dataValid) ? MA : MC;
//            ld_Data = (procBus.dataValid) ? 1: 0;
//        end
//        MD: begin
//            NextState = (wDataRdy) ? MA: MD;
//            en_Data = (wDataRdy) ? 1: 0;
//            dv = (wDataRdy) ? 1:0;
//        end
//    endcase
//end
endmodule
