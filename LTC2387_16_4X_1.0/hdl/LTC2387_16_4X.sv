`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.08.2020 10:48:07
// Design Name: 
// Module Name: LTC2387_16_4X
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//interface diff;
//  logic p, n;      // Indicates if slave is ready to accept data
//  modport slave  (input p, n);
//  modport master (output p, n);
//endinterface

module LTC2387_16_4X(

	input fpga_clk,
    input adc_clk_valid,

	output wire clk_data_IF1,
	output wire clk_data_IF2,

	output wire [15:0] ADC_DATA_IF1,
	output wire [15:0] ADC_DATA_IF2,

	diff.slave adc_da_IF1,
    diff.slave adc_db_IF1,
    diff.slave adc_dco_IF1,
    diff.master adc_clk_IF1,

	diff.slave adc_da_IF2,
    diff.slave adc_db_IF2,
    diff.slave adc_dco_IF2,
    diff.master adc_clk_IF2
    );


	wire clk_100;
	wire adc_clk_valid;

    clk_wiz_1 clk_wizard_adc(
      .clk_out1(clk_100),                 
      .clk_in1(fpga_clk & adc_clk_valid),
      .locked(clk_locked)                      
    );

LTC2387_16 LTC2387_16_IF1(
    .fpga_clk(fpga_clk),
    .clk_100(clk_100),
    .clk_data(clk_data_IF1),
    .ADC_DATA(ADC_DATA_IF1),  
    .adc_dax(adc_da_IF1),
    .adc_dbx(adc_db_IF1),
    .adc_dcox(adc_dco_IF1),
    .adc_clkx(adc_clk_IF1)
    );

LTC2387_16 LTC2387_16_IF2(
    .fpga_clk(fpga_clk),
    .clk_100(clk_100),
    .clk_data(clk_data_IF2),
    .ADC_DATA(ADC_DATA_IF2),  
    .adc_dax(adc_da_IF2),
    .adc_dbx(adc_db_IF2),
    .adc_dcox(adc_dco_IF2),
    .adc_clkx(adc_clk_IF2)
    );



endmodule
