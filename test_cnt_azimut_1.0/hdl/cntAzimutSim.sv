`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.03.2021 12:01:49
// Design Name: 
// Module Name: cntAzimutSim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module cntAzimutSim;

logic clk, DMA_8ms, TIM_8ms;

test_cnt_azimut( clk, DMA_8ms, TIM_8ms);

initial begin
    $monitor($time, "clk=%b DMA_8ms=%b TIM_8ms=%b", clk, TIM_8ms, DMA_8ms);
    
    clk = 0;
    
end

endmodule
