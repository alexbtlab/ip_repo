//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.03.2021 11:51:29
// Design Name: 
// Module Name: test_cnt_azimut_mod
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
	module test_cnt_azimut 
	(
		input wire  clk,
		output wire  DMA_8ms,
		output wire  TIM_8ms	
	);

    wire CLKOUT0;
    
    design_1_clk_wiz_0_2 clk_wizard_adc(
      .clk_out1(CLKOUT0),
      .clk_in1(clk)        
    );
    
    localparam cntMAX = 65535; 
    reg [15:0] cnt_100;
    
    always @ (posedge CLKOUT0) begin
        if(cnt_100 != cntMAX)
            cnt_100 <= cnt_100 +1;
        else
            cnt_100 <= 0;
    end

   
   
   
	endmodule